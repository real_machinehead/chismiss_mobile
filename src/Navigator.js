import React,{ Component } from 'react';
import { createStackNavigator,createAppContainer,createSwitchNavigator } from 'react-navigation';
import Login from './containers/security/login';
import DashBoard from './containers/dashboard';
import Splash from './containers/splash';
import Registration from './containers/setups/registration';
import BottomTab from './BottomNavigator';
import Call from './containers/calls';
import Chatlist from './containers/chatlist';
import Contacts from './containers/contacts';
import Search from './containers/search';
import Profile from './containers/profile';
import Notifications from './containers/notification';
import Chats from './containers/chats';
import Settings from './containers/settings';
import Privacy from './containers/privacy';
import Terms from './containers/terms';
import AboutUs from './containers/about_us';
//import Chats from './ChatsNavigator';
//import Webrtc from './containers/webrtc';

//import Channels from './containers/setups/channels';
//import Workspace from './containers/setups/workspace';
//import Invites from './containers/setups/invites';



const Navigator=createSwitchNavigator({
    Home:{
        screen:Splash
    },
    DashBoard:{
        screen:DashBoard
    },
    Registration:{
        screen:Registration
    },
    Login:{
        screen:Login
    },
    BottomTab:{
        screen:BottomTab
    },
    Chatlist: {
        screen:Chatlist
    },
    Calls: {
        screen:Call,
    },
    Contacts:{
        screen:Contacts,
    },
    Search:{
        screen:Search,
    },
    Profile:{
        screen:Profile,
    },
    Notification:{
       screen:Notifications, 
    },
    Chats:{
        screen:Chats,
    },
    Settings:{
        screen:Settings,
    },
    Privacy:{
        screen:Privacy,
    },
    Terms:{
        screen:Terms,
    },
    AboutUs:{
        screen:AboutUs
    }
    /*Webrtc:{
        screen:Webrtc,
    },*/
},
{backBehavior: 'initialRoute',resetOnBlur:true}
);


export default createAppContainer(Navigator);