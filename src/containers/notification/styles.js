export default{
    flatlist:{
        flex:1,
        flexDirection:'row',
        width:'100%',
        backgroundColor:'white',             
    },
    notificationIcon:{
        width:'33%',
        justifyContent:'space-between',
    },
    profileIcon:{
        width:'33%',
        alignItems:'center',
        flexDirection:'row',
    },
    searchIcon:{
        width:'33%',
        alignItems:'flex-end'       
    },
    smallroundavatar:{
        width:12,
        height:12,
        backgroundColor:'green',
        borderRadius:100/2,
        alignItems:'center',
        justifyContent:'center',
    },
    profileAvatar:{
        width:50,
        height:50,
        alignItems:'center',
        backgroundColor:'white',
        borderRadius:100/2,
        alignItems:'flex-end',       
        justifyContent:'center',
        flexDirection:'row',
        marginLeft:30,
    },
}