import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform } from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import {GeneralContainers,GeneralHeaders,GeneralContents} from '../../components';
import { bindActionCreators } from 'redux';
import * as chatActions from '../datas/chatlist/actions';
import styles from '../styles';
import chatstyles from './styles';
import {IconButton} from '../../components/common';
import Icon from 'react-native-vector-icons/Ionicons';


export default class Notifications extends Component{

    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:{
                flagno:1,
                message:'success',
                data:[
                    {
                        firstName:'Kpop',
                        lastName:'Fans',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    },
                    {
                        firstName:'Girls',
                        lastName:'Generation',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    },
                    {
                        firstName:'Black',
                        lastName:'Pink',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    }
                ]
            },
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
        };
    }
    
    /*static navigationOptions ={
        title: 'CALLS',
        headerStyle:{
            backgroundColor: 'white'
        },
        headerTitleStyle:{
            alignSelf:'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign:'center'
        }
    }*/


    componentDidMount(){
        this._fetchData();
    }


    _fetchData=()=>{

    }

    _keyExtractor = (item, index) => item.firstName;

    _getNotification=()=>{
  
    }

    _getProfile=()=>{
        this.props.navigation.navigate('Profile')
    }
    _getOnSearch=()=>{
        this.props.navigation.navigate('Search')
    }
    render(){
        const data=this.state.data.data;
        return(
            <GeneralContainers>

                <GeneralHeaders>
                    <View style={styles.headerTitle}>
                        <Text>NOTIFICATION</Text>
                    </View>
                </GeneralHeaders>

                <GeneralContents>
                    <View style={chatstyles.flatlist}>
                        <FlatList
                            data={data}
                            keyExtractor={this._keyExtractor}
                            renderItem={({ item }) => (
                                <ListItem
                                    title={`${item.firstName} ${item.lastName}`}
                                    subtitle={
                                        <View style={{flex:1,backgroundColor:'gray',marginLeft:10,opacity:1}}>
                                            <View>
                                                <Text>{item.message}</Text>
                                            </View>
                                        </View>

                                    }
                                    avatar={<Avatar
                                        small
                                        rounded
                                        title={`${item.lastName.charAt(0)}${item.firstName.charAt(0)}`}
                                        onPress={() => console.log("Works!")}
                                        activeOpacity={0.7}
                                    />}
                                    rightIcon={{fontSize:0,color:'white'}}
                                />
                            )}
                        />  
                      
                    </View>
                </GeneralContents>

            </GeneralContainers>
        );
    }
}
/*function mapStateToProps(state){

    return{
        chats:state
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            chats:bindActionCreators(chatActions,dispatch),
        }
    }
}

export default connect(
    mapDispatchToProps,
    mapStateToProps
)(Chat)*/