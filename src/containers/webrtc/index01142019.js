import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    ListView,
    Platform,

  } from 'react-native';
// import { Platform } from 'react-native';
import {
    RTCPeerConnection,
    RTCMediaStream,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStreamTrack,
    getUserMedia,
    mediaDevices
} from 'react-native-webrtc';
import io from 'socket.io-client';

const socket = io.connect('https://react-native-webrtc.herokuapp.com', {transports: ['websocket']});
const pcPeers = {};
let localStream;
class Main extends Component {
    // Initial state
    constructor(props){
        super(props);

        this.state = {
            videoURL: null,
            remoteUrl:null,
            isFront: true,
            info: 'Initializing',
            status: 'init',
            roomID: '',
            selfViewSrc: null,
            remoteList: {},
            textRoomConnected: false,
            textRoomData: [],
            textRoomValue: '',
            localDesc:'',
            remotDesc:'',
            iceCandidate:'',
            isAnswer:false,
            clientId:null,
            connectDisabled:false,
            clientEnable:true,
            remoteiceCandidate:[],
            mediaStream:null,
        }
    }; 


    componentDidMount() {
        var socket = this._onSocketConnection();
        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        const { isFront } = this.state;
        const remote=this;
        
        //if recieved remote ice candidate

        socket.on('remotecredentials',function(data){
            //console.log('remoteonicecandidate',data);
            
            remote._addIceCandidate(data)
        });


        /*socket.on('remotecallconnection',function(data){
            console.log('remote',data);
            if(remote.state.clientId !=data){
                remote._onRecievedremoteSignal();
            }

            socket.on('remotestreamurl',function(data){
                console.log('remote streamsssss',data);
                remote.setState({mediaStream:data})
            });
        })*/


        /*socket.on('remoteonicecandidate',function(data){
            remote._addIceCandidate(data);
        });*/


        MediaStreamTrack.getSources(sourceInfos => {
            console.log('MediaStreamTrack.getSources', sourceInfos);
            let videoSourceId;
            for (let i = 0; i < sourceInfos.length; i++) {
                const sourceInfo = sourceInfos[i];
                // I like ' than "
                if (sourceInfo.kind === 'video' && sourceInfo.facing === (isFront ? 'front' : 'back')) {
                    videoSourceId = sourceInfo.id;
                }
            }
            getUserMedia({
                audio: true,
                // THIS IS FOR SIMULATOR ONLY
                // In fact, you better test on real iOS/Android device
                // We just can test audio on simulator, so i set video = false
                // video: Platform.OS === 'ios' ? false : {
                video: {
                    mandatory: {
                        minWidth: 500, // Provide your own width, height and frame rate here
                        minHeight: 300,
                        minFrameRate: 30
                    },
                    facingMode: (isFront ? 'user' : 'environment'),
                    optional: (videoSourceId ? [{ sourceId: videoSourceId }] : [])
                }
            }, (stream) => {
                // use arrow function :)
                // (stream) or stream are fine
                //console.log('Streaming OK', stream.toURL());
                this.setState({
                    videoURL: stream.toURL(),
                });
                pc.addStream(stream);
            }, error => {
                console.log('Oops, we getting error', error.message);
                throw error;
            });
        });


          

    }

    componentDidUpdate(prevProps,prevState){

    }

    _onLocalStreamConnection=()=>{
        
    }

    _onCreateOffer=(data)=>{

    }    



    _onRemoteConnection=(data)=>{

      
    }

    _onSocketConnection=()=>{

        var socket = io('http://192.168.22.162:8888',{transports: ['websocket']});
        return socket;
    }

    _onIceConnection=()=>{
        const configuration = { "iceServers": [{ "url": "stun:stun.l.google.com:19302" }] };

        return configuration;
    }


    _onSendRemoteinfo=()=>{

    }

    _onSendCandidate=()=>{
        var socket = this._onSocketConnection();
        
        //socket.emit('exchange',{iceCandidate:iceCandidate});
        //socket.emit('icecandidate',this.state.iceCandidate);
        //console.log('icecandidate',this.state.iceCandidate);
    }

    _onclientIdChanged=(text)=>{

    }


    _onsendPeerCreditials=async()=>{
        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        var socket = this._onSocketConnection();
        const arraycandidate=[]
        pc.createOffer((desc) => {
            pc.setLocalDescription(desc, () => {
                // Send pc.localDescription to peer
                console.log('pc.setLocalDescription',desc);
                /*this.setState({
                  localDesc:desc,
                })*/
                //cket.emit('localdescription',desc);
                socket.emit('peercredentials',{sdp:desc});
            }, (e) => { throw e; });
        }, (e) => { throw e; });

        pc.onicecandidate = function (event) {
            // send event.candidate to peer
            
            if(event.candidate){
                //console.log('ice candidate',event.candidate);
                arraycandidate.push(event.candidate);
               
                socket.emit('peercredentials',{candidate:event.candidate});     
                
                //console.log('ice candidate',event);
            }else{
                //console.log('ice candidatesss',arraycandidate);
                //socket.emit('peercredentials',arraycandidate);    
            }

        };

        pc.onaddstream = function (event) {
            console.log('onaddstream', event.stream);
        };
        pc.oniceconnectionstatechange=function(event){
            console.log('statusss',event)
        }
    }
    

    _addIceCandidate=(data)=>{
        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);

        console.log('remote credentials',data);
        //console.log('Ice Candidate',data);
        //const dataobj=Object.entries(data);
        //console.log('Ice Candidate',dataobj);
        pc.onsignalingstatechange = function(event) {
            //signalState.signalStatus=event.target.signalingState;
            console.log('onsignalingstatechangessss', event.target.signalingState);
        };


        if(data.sdp){
            pc.setRemoteDescription(new RTCSessionDescription(data.sdp), function () {
                if (pc.remoteDescription.type == "offer"){

                      pc.createAnswer(function(desc) {
                        console.log('createanswer', desc);

                        });

                    }
                  
              });
        }else{

            const icecandidates=new RTCIceCandidate(data.candidate);
            console.log('icecandidatesss',icecandidates.candidate);
            //pc.addIceCandidate(new RTCIceCandidate(icecandidates));
            pc.addIceCandidate(icecandidates).catch(e => {
                console.log("Failure during addIceCandidate(): " + e);
            });
        }

    }
    _onRecievedremoteSignal=()=>{

    }

    _onaswerConnection=async()=>{
        //this._onSendCandidate();
        await this._sendIceCandidate()
        await this._onsendlocalDescription()
    }

    _onCallConnection=async()=>{
        //send ice candidate
        await this._onsendPeerCreditials()
    }

    render() {
        console.log('mediaStream',this.state.mediaStream);
        console.log('remoteurl',this.state.remoteUrl);
        return (
            <View style={{flex:1,flexDirection:'column'}}>
                  <RTCView streamURL={this.state.videoURL} style={styles.container} />
                  <RTCView streamURL={this.state.remoteUrl} style={styles.container} />
                  <View>
                        <TextInput
                            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                            onChangeText={this._onclientIdChanged.bind(this)}
                            value={this.state.clientId}
                            enabled={this.state.clientEnable}
                            placeholder='ClientId'
                        />
                  </View>
                  <View style={{height:150,backgroundColor:'green'}}>
                    <TouchableOpacity
                        style={{borderWidth: 1, borderColor: 'black',height:50}}
                        onPress={this._onCallConnection}
                        disabled={this.state.connectDisabled}>
                        <Text>Call</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{borderWidth: 1, borderColor: 'black',height:50}}
                      onPress={this._onaswerConnection}
                    >
                      <Text>Answer</Text>
                    </TouchableOpacity>
                  </View>

            </View>
           
        );
    }
}
const styles = {
    container: {
        flex: 1,
        backgroundColor: '#ccc',
        borderWidth: 1,
        borderColor: '#000'
    },
    selfView: {
        width: 200,
        height: 150,
      },
      remoteView: {
        width: 200,
        height: 150,
      },
      container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      listViewContainer: {
        height: 150,
      },
};

export default Main;
