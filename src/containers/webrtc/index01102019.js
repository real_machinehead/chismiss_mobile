import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    ListView,
    Platform,

  } from 'react-native';
// import { Platform } from 'react-native';
import {
    RTCPeerConnection,
    RTCMediaStream,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStreamTrack,
    getUserMedia,
} from 'react-native-webrtc';
import io from 'socket.io-client';

const socket = io.connect('https://react-native-webrtc.herokuapp.com', {transports: ['websocket']});
const pcPeers = {};
let localStream;
class Main extends Component {
    // Initial state
    constructor(props){
        super(props);

        this.state = {
            videoURL: null,
            isFront: true,
            info: 'Initializing',
            status: 'init',
            roomID: '',
            selfViewSrc: null,
            remoteList: {},
            textRoomConnected: false,
            textRoomData: [],
            textRoomValue: '',
            localDesc:'',
            remotDesc:'',
            iceCandidate:'',
            isAnswer:false,
            clientId:null,
            connectDisabled:true,
            clientEnable:true,
            remoteiceCandidate:[],
        }
    }; 


    componentDidMount() {
        var socket = this._onSocketConnection();
        this._onLocalStreamConnection();
    }

    componentDidUpdate(prevProps,prevState){

        var socket = this._onSocketConnection();
        const remote=this;


        //if(this.state.remotDesc!=)
        socket.on('remotecallconnection',function(data){
            console.log('remote',data);
            if(remote.state.clientId !=data){
                remote._onRecievedremoteSignal();
            }

        })

        socket.on('remoteicecandidate',function(data){
            console.log('ice Candidatesss',data);
            //remote._onaddIceCandidate(data);
        });


        /*socket.on('exchange', function(data){
            console.log('get remote stream',data);
            remote.setState({remotDesc:data})
        });

        socket.on('remoteicecandidate',function(data){
            console.log('ice Candidate',data);
            remote._onaddIceCandidate(data);
        });*/




        //console.log('prevpropss',prevState);
        //if(this.state.remoteDesc=)
        //this._onRemoteConnection();
    }

    _onLocalStreamConnection=()=>{

        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        const { isFront } = this.state;

        MediaStreamTrack.getSources(sourceInfos => {
            console.log('MediaStreamTrack.getSources', sourceInfos);
            let videoSourceId;
            for (let i = 0; i < sourceInfos.length; i++) {
                const sourceInfo = sourceInfos[i];
                // I like ' than "
                if (sourceInfo.kind === 'video' && sourceInfo.facing === (isFront ? 'front' : 'back')) {
                    videoSourceId = sourceInfo.id;
                }
            }
            getUserMedia({
                audio: true,
                // THIS IS FOR SIMULATOR ONLY
                // In fact, you better test on real iOS/Android device
                // We just can test audio on simulator, so i set video = false
                // video: Platform.OS === 'ios' ? false : {
                video: {
                    mandatory: {
                        minWidth: 500, // Provide your own width, height and frame rate here
                        minHeight: 300,
                        minFrameRate: 30
                    },
                    facingMode: (isFront ? 'user' : 'environment'),
                    optional: (videoSourceId ? [{ sourceId: videoSourceId }] : [])
                }
            }, (stream) => {
                // use arrow function :)
                // (stream) or stream are fine
                console.log('Streaming OK', stream);
                this.setState({
                    videoURL: stream.toURL()
                });
                pc.addStream(stream);
            }, error => {
                console.log('Oops, we getting error', error.message);
                throw error;
            });
        });
        /*pc.createOffer((desc) => {
            pc.setLocalDescription(desc, () => {
                // Send pc.localDescription to peer
                console.log('pc.setLocalDescription',desc);
                this.setState({
                  localDesc:desc,
                })
                //socket.emit('exchange',desc);
            }, (e) => { throw e; });
        }, (e) => { throw e; });

        pc.onicecandidate = (event) => {
            // send event.candidate to peer
            console.log('onicecandidate', event.candidate);

            if(this.state.iceCandidate==''){
                this.setState({
                    iceCandidate:event.candidate,
                })     
            }

        };
        pc.onaddstream = function (event) {
          console.log('onaddstream', event.stream);
        };*/

    }

    _onCreateOffer=(data)=>{
        pc.createOffer((desc) => {
            pc.setLocalDescription(desc, () => {
                // Send pc.localDescription to peer
                console.log('pc.setLocalDescription',desc);
                /*this.setState({
                  localDesc:desc,
                })*/
                //socket.emit('exchange',desc);
            }, (e) => { throw e; });
        }, (e) => { throw e; });
    }

    _onaddIceCandidate=(data)=>{

        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        var candidateObj = new RTCIceCandidate(data);
        //icecadidate.push(data)
        //console.log('Icecandidatesss',candidate);

        //{candidate: "candidate:4130039701 1 udp 2122260223 192.168.22.2…eration 1 ufrag koWa network-id 3 network-cost 10", sdpMLineIndex: 1, sdpMid: "video"}

        //console.log('ice candidatessss',data);
        /*pc.addIceCandidate(JSON.stringify(data)).catch(e => {
            console.log("Failure during addIceCandidate(): " + e);
        });*/
        //console.log('candidateas',icecadidate);
        pc.addIceCandidate(new RTCIceCandidate({candidate: "candidate:4130039701 1 udp 2122260223 192.168.22.2…eration 1 ufrag koWa network-id 3 network-cost 10", sdpMLineIndex: 1, sdpMid: "video"}));
        //console.log('data index ',data);
        this.state.remoteiceCandidate.push(data);

    }

    _onRemoteConnection=()=>{

        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        const remotesdp=this.state.remotDesc;
        console.log('onRemoteConnection',remotesdp);



        if(remotesdp){
            pc.onsignalingstatechange = function(event) {
                //signalState.signalStatus=event.target.signalingState;
                console.log('onsignalingstatechangessss', event.target.signalingState);
            };
            pc.setRemoteDescription(new RTCSessionDescription(remotesdp)).then(function () {
                /*if (pc.remoteDescription.type == "offer"){
                    console.log('remotedescription',pc.remoteDescription.type);

                    pc.createAnswer().then(function(answer) {
                        console.log('createanswer', answer);
                        return pc.setLocalDescription(answer);
                    })
                      .then(function() {
                        // Send the answer to the remote peer through the signaling server.
                      })
                }*/


            })
            .then(function(stream){
                console.log('remote stream',stream);
                return pc.addStream(stream);
            })
            .then(function(){
                return pc.createAnswer();
            })
            .then(function(answer){
                return pc.setLocalDescription(answer);
            })
            .then(function(){

            })
            pc.onicecandidate = (event) => {
                // send event.candidate to peer
                console.log('onicecandidatessss', event);
            };
            pc.onaddstream=function(event){
                console.log('addreamm',event.stream);
            }
        }
    }

    _onSocketConnection=()=>{
        /*var socket = io('http://192.168.22.162:8888',{transports: ['websocket'],pingInterval: 10000,
        pingTimeout: 5000,});*/
        var socket = io('http://192.168.22.162:8888',{transports: ['websocket']});
        return socket;
    }

    _onIceConnection=()=>{
        const configuration = { "iceServers": [{ "url": "stun:stun.l.google.com:19302" }] };

        return configuration;
    }


    _onSendRemoteinfo=()=>{
        var socket = this._onSocketConnection();
        //console.log('send',this.state.localDesc);
        var localdesc=this.state.localDesc;
        
        //console.log('ice candidate',this.state.iceCandidate);
        //socket.emit('exchange',{iceCandidate:iceCandidate});

        socket.emit('exchange',localdesc);

        //socket.emit('icecandidate',this.state.iceCandidate);
    }

    _onSendCandidate=()=>{
        var socket = this._onSocketConnection();
        
        //socket.emit('exchange',{iceCandidate:iceCandidate});
        //socket.emit('icecandidate',this.state.iceCandidate);
        console.log('icecandidate',this.state.iceCandidate);
    }

    _onclientIdChanged=(text)=>{
        console.log('client Id',text);
        if(text){
            this.setState({clientId:text,connectDisabled:false});
        }else{
            this.setState({clientId:text,connectDisabled:true});
        }

    }

    _onCallConnection=async()=>{
        var socket = this._onSocketConnection();
        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);

        console.log('localdescription');
        await socket.emit('callconnection',this.state.clientId);
        this.setState({
            connectDisabled:false,
            clientEnable:false,
        });

        //send local description

         pc.createOffer((desc) => {
         pc.setLocalDescription(desc, () => {
                    // Send pc.localDescription to peer
                    //console.log('pc.setLocalDescription',desc);
                    socket.emit('localdescription',desc);
                }, (e) => { throw e; });
            }, (e) => { throw e; });

         pc.onicecandidate = (event) => {
            // send event.candidate to peer
            
            if(event.candidate){
                console.log('onicecandidate', event.candidate);
                socket.emit('icecandidate',event.candidate);
            }
        };

        pc.onaddstream = function (event) {
            console.log('onaddstream', event.stream);
        };
        
    }

    _onRecievedremoteSignal=()=>{
        var socket = this._onSocketConnection();
        var remote = this;
        console.log('receive remote signal');
        socket.on('remotedescription',function(data){
            console.log('localdescription',data);
            //remote._onRemoteConnection(data);
        });
        socket.on('remoteicecandidate',function(data){
            //console.log('remote ice candidate',data);
            remote._onaddIceCandidate(data);
        });

        this.setState({connectDisabled:true})
    }

    _onsendlocalDescription=()=>{
        var socket = this._onSocketConnection();
        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        pc.createOffer((desc) => {
            pc.setLocalDescription(desc, () => {
                // Send pc.localDescription to peer
                console.log('pc.setLocalDescription',desc);
                /*this.setState({
                  localDesc:desc,
                })*/
                socket.emit('localdescription',desc);
                //socket.emit('exchange',desc);
            }, (e) => { throw e; });
        }, (e) => { throw e; });

        pc.onicecandidate = (event) => {
            // send event.candidate to peer
            console.log('onicecandidate', event.candidate);
            if(event.candidate){
                socket.emit('icecandidate',event.candidate);
            }
            /*if(this.state.iceCandidate==''){
                this.setState({
                    iceCandidate:event.candidate,
                })     
            }*/

        };
        pc.onaddstream = function (event) {
          console.log('onaddstream', event.stream);
        };
    }

    render() {
        console.log('remotesdp',this.state.remotDesc);
        console.log('oncandidatessss',this.state.remoteiceCandidate);
        return (
            <View style={{flex:1,flexDirection:'column'}}>
                  <RTCView streamURL={this.state.videoURL} style={styles.container} />
                  <View>
                        <TextInput
                            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                            onChangeText={this._onclientIdChanged.bind(this)}
                            value={this.state.clientId}
                            enabled={this.state.clientEnable}
                            placeholder='ClientId'
                        />
                  </View>
                  <View style={{height:150,backgroundColor:'green'}}>
                    <TouchableOpacity
                        style={{borderWidth: 1, borderColor: 'black',height:50}}
                        onPress={this._onCallConnection}
                        disabled={this.state.connectDisabled}>
                        <Text>Call</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{borderWidth: 1, borderColor: 'black',height:50}}
                      onPress={this._onCallConnection}
                    >
                      <Text>Answer</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{height:150,backgroundColor:'blue'}}>
                    <TouchableOpacity
                      style={{borderWidth: 1, borderColor: 'black',height:50}}
                      onPress={this._onSendCandidate}>
                      <Text>Send ice Candidate</Text>
                    </TouchableOpacity>
                  </View>
            </View>
           
        );
    }
}
const styles = {
    container: {
        flex: 1,
        backgroundColor: '#ccc',
        borderWidth: 1,
        borderColor: '#000'
    },
    selfView: {
        width: 200,
        height: 150,
      },
      remoteView: {
        width: 200,
        height: 150,
      },
      container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      listViewContainer: {
        height: 150,
      },
};

export default Main;
