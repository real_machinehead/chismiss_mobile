import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    TextInput,
    ListView,
    Platform,
  } from 'react-native';
// import { Platform } from 'react-native';
import {
    RTCPeerConnection,
    RTCMediaStream,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStreamTrack,
    getUserMedia,
} from 'react-native-webrtc';

import io from 'socket.io-client/dist/socket.io';


window.navigator.userAgent = 'ReactNative';

const configuration = {"iceServers": [{"url": "stun:chismischannel.live:3478"}]};

//const socket = io.connect('wss://chismischannel.live:9999', {transports: ['websocket']});
//const socket = io.connect('https://react-native-webrtc.herokuapp.com', {transports: ['websocket']});
//const socket = socketIo.connect('https://chismischannel.live:8888/', {transports: ['websocket']});

const socket =  io('https://chismischannel.live',{transports: ['websocket']});
const pcPeers = {};
let localStream;

function join(roomID) {
    socket.emit('join', roomID, function(socketIds){
      console.log('join', socketIds);
      for (const i in socketIds) {
        const socketId = socketIds[i];
        createPC(socketId, true);
      }
    });

  }

/*function createPC(socketId, isOffer) {
    const pc = new RTCPeerConnection(configuration);
    pcPeers[socketId] = pc;
  
    pc.onicecandidate = function (event) {
      console.log('onicecandidate', event.candidate);
      if (event.candidate) {
        socket.emit('exchange', {'to': socketId, 'candidate': event.candidate });
      }
    };
    
    function createOffer() {
        pc.createOffer(function(desc) {
          console.log('createOffer', desc);
          pc.setLocalDescription(desc, function () {
            console.log('setLocalDescription', pc.localDescription);
            socket.emit('exchange', {'to': socketId, 'sdp': pc.localDescription });
          }, logError);
        }, logError);
      }
    

    pc.onnegotiationneeded = function () {
        console.log('onnegotiationneeded');
            if (isOffer) {
            createOffer();
        }
    }

    pc.oniceconnectionstatechange = function(event) {
        console.log('oniceconnectionstatechange', event.target.iceConnectionState);
        if (event.target.iceConnectionState === 'completed') {
        setTimeout(() => {
            getStats();
        }, 1000);
        }
        if (event.target.iceConnectionState === 'connected') {
        createDataChannel();
        }
    };

    pc.onsignalingstatechange = function(event) {
        console.log('onsignalingstatechange', event.target.signalingState);
    };

    pc.onaddstream = function (event) {
        console.log('onaddstream', event.stream);
        container.setState({info: 'One peer join!'});

        const remoteList = container.state.remoteList;
        remoteList[socketId] = event.stream.toURL();
        container.setState({ remoteList: remoteList });
    };
    pc.onremovestream = function (event) {
        console.log('onremovestream', event.stream);
    };

    pc.addStream(localStream);
    function createDataChannel() {
    if (pc.textDataChannel) {
        return;
    }

    const dataChannel = pc.createDataChannel("text");

    dataChannel.onerror = function (error) {
        console.log("dataChannel.onerror", error);
    };
  
    dataChannel.onmessage = function (event) {
        console.log("dataChannel.onmessage:", event.data);
        container.receiveTextData({user: socketId, message: event.data});
    };

    dataChannel.onopen = function () {
        console.log('dataChannel.onopen');
        container.setState({textRoomConnected: true});
    };

    dataChannel.onclose = function () {
        console.log("dataChannel.onclose");
    };

    pc.textDataChannel = dataChannel;
}
return pc;
}*/

function getLocalStream(isFront, callback) {

    let videoSourceId;
  
    // on android, you don't have to specify sourceId manually, just use facingMode
    // uncomment it if you want to specify
    if (Platform.OS === 'ios') {
      MediaStreamTrack.getSources(sourceInfos => {
        console.log("sourceInfos: ", sourceInfos);
  
        for (const i = 0; i < sourceInfos.length; i++) {
          const sourceInfo = sourceInfos[i];
          if(sourceInfo.kind == "video" && sourceInfo.facing == (isFront ? "front" : "back")) {
            videoSourceId = sourceInfo.id;
          }
        }
      });
    }
    getUserMedia({
      audio: true,
      video: {
        mandatory: {
          minWidth: 640, // Provide your own width, height and frame rate here
          minHeight: 360,
          minFrameRate: 30,
        },
        facingMode: (isFront ? "user" : "environment"),
        optional: (videoSourceId ? [{sourceId: videoSourceId}] : []),
      }
    }, (stream) =>{
      console.log('getUserMedia success', stream);
      callback(stream);
    }, logError);
  }

    /*socket.on('exchange', function(data){
        exchange(data);
      });
      socket.on('leave', function(socketId){
        leave(socketId);
      });

      socket.on('connect', function(data) {
        console.log('connect');
        getLocalStream(true, function(stream) {
          localStream = stream;
          container.setState({selfViewSrc: stream.toURL()});
          container.setState({status: 'ready', info: 'Please enter or create room ID'});
        });
      });

      socket.on('connect_error', (err) => {
        console.log('errorss',err)
      });
      function logError(error) {
        console.log("logError", error);
      }*/

    function exchange(data) {
        const fromId = data.from;
        let pc;
        if (fromId in pcPeers) {
          pc = pcPeers[fromId];
        } else {
          pc = createPC(fromId, false);
        }
      
        if (data.sdp) {
          console.log('exchange sdp', data);
          pc.setRemoteDescription(new RTCSessionDescription(data.sdp), function () {
            if (pc.remoteDescription.type == "offer")
              pc.createAnswer(function(desc) {
                console.log('createAnswer', desc);
                pc.setLocalDescription(desc, function () {
                  console.log('setLocalDescription', pc.localDescription);
                  socket.emit('exchange', {'to': fromId, 'sdp': pc.localDescription });
                }, logError);
              }, logError);
          }, logError);
        } else {
          console.log('exchange candidate', data);
          pc.addIceCandidate(new RTCIceCandidate(data.candidate));
        }
      }

    function getStats() {
        const pc = pcPeers[Object.keys(pcPeers)[0]];
        if (pc.getRemoteStreams()[0] && pc.getRemoteStreams()[0].getAudioTracks()[0]) {
          const track = pc.getRemoteStreams()[0].getAudioTracks()[0];
          console.log('track', track);
          pc.getStats(track, function(report) {
            console.log('getStats report', report);
          }, logError);
        }
      }
class Main extends Component {
    // Initial state
    constructor(props){
        super(props);

        this.state = {
            videoURL: null,
            isFront: true,
            info: 'Initializing',
            status: 'init',
            roomID: '',
            selfViewSrc: null,
            remoteList: {},
            textRoomConnected: false,
            textRoomData: [],
            textRoomValue: '',
            urlsockets:io('https://chismischannel.live:8888/',{ transports: ['websocket'] }),
        }

        /*this.state.urlsockets.on('channel2', (data) => {
          console.log('Data recieved from server', data); //this will console 'channel 2'
        });*/
    }; 


    componentDidMount() {
        /*const configuration = { "iceServers": [{ "url": "stun:stun.l.google.com:19302" }] };
        const pc = new RTCPeerConnection(configuration);
        const { isFront } = this.state;
        MediaStreamTrack.getSources(sourceInfos => {
            console.log('MediaStreamTrack.getSources', sourceInfos);
            let videoSourceId;
            for (let i = 0; i < sourceInfos.length; i++) {
                const sourceInfo = sourceInfos[i];
                // I like ' than "
                if (sourceInfo.kind === 'video' && sourceInfo.facing === (isFront ? 'front' : 'back')) {
                    videoSourceId = sourceInfo.id;
                }
            }
            getUserMedia({
                audio: true,
                // THIS IS FOR SIMULATOR ONLY
                // In fact, you better test on real iOS/Android device
                // We just can test audio on simulator, so i set video = false
                // video: Platform.OS === 'ios' ? false : {
                video: {
                    mandatory: {
                        minWidth: 500, // Provide your own width, height and frame rate here
                        minHeight: 300,
                        minFrameRate: 30
                    },
                    facingMode: (isFront ? 'user' : 'environment'),
                    optional: (videoSourceId ? [{ sourceId: videoSourceId }] : [])
                }
            }, (stream) => {
                // use arrow function :)
                // (stream) or stream are fine
                console.log('Streaming OK', stream);
                this.setState({
                    videoURL: stream.toURL()
                });
                pc.addStream(stream);
            }, error => {
                console.log('Oops, we getting error', error.message);
                throw error;
            });
        });
        pc.createOffer((desc) => {
            pc.setLocalDescription(desc, () => {
                // Send pc.localDescription to peer
                console.log('pc.setLocalDescription');
            }, (e) => { throw e; });
        }, (e) => { throw e; });

        pc.onicecandidate = (event) => {
            // send event.candidate to peer
            console.log('onicecandidate', event);
        };*/  

        container = this;

        const socket = io('http://localhost',{transports: ['websocket'],port:8888,path:'/',rejectUnauthorized: false});

        socket.on('response',(messages) => {
          Alert.alert("response." + JSON.stringify(messages));
          console.log("response" + JSON.stringify(messages));
        });
        socket.on('connection', ()=>{
          console.log("socket connected")
          socket.emit('YOUR EVENT TO SERVER', {})
          socket.on('EVENT YOU WANNA LISTEN', (r) => {
          })
        });
        socket.on('connect_error', (err) => {
          console.log('error123',err)
        });


        /*io.on('connection', function(socket){
          console.log('connection');
          socket.on('disconnect', function(){
            console.log('disconnect');
            if (socket.room) {
              var room = socket.room;
              io.to(room).emit('leave', socket.id);
              socket.leave(room);
            }
          });*/        
        //const socket = io.connect('wss://chismischannel.live:9999', {transports: ['websocket']});
        //const socket = io('https://chismischannel.live:8888/', {transports: ['websocket'],'timeout':5000, 'connect timeout': 5000});
        
        //const socket = io.connect("https://chismischannel.live:8888",{ transports: ['websocket'] });
        //const fs = require('fs');

        /*const socket = io('https://chismischannel.live:8888', {
          transports: ['websocket'],pingTimeout: 30000
           // you need to explicitly tell it to use websockets
        });

        //console.log('sockets',socket);
        /*socket.on('connect', () => {
          console.log("socket connected")
          socket.emit('YOUR EVENT TO SERVER', {})
          socket.on('EVENT YOU WANNA LISTEN', (r) => {
          })
        })*/

      /*socket.on('connect', function() {
          alert("connect");
      });
  
      socket.on('error', function (data) {
          alert(data);
      });
  
      socket.on('disconnect', function () {
          alert("disconnect");
      });
  
      socket.on("reconnect", function () {
          alert("reconnect");
      });
  
      socket.on('connect_error', (err) => {
        console.log('error',err)
      });
        /*socket.on('connect_error', (err) => {
          console.log('error',err)
        })
    
        socket.on('disconnect', () => {
          console.log("Disconnected Socket!")
        })*/    

        /*const dataObj = {
          action: 'click'
        };
        
       //this.state.urlsockets.emit('channel2', dataObj);
        this.state.urlsockets.on('connect_error', (err) => {
          console.log('error',err)
        });*/
    }

    _press(event) {
        this.refs.roomID.blur();
        this.setState({status: 'connect', info: 'Connecting'});
        //console.log('roomId',this.state.roomID)
        join(this.state.roomID);
    }
    receiveTextData(data) {
        const textRoomData = this.state.textRoomData.slice();
        textRoomData.push(data);
        this.setState({textRoomData, textRoomValue: ''});
    }
    _textRoomPress() {
        if (!this.state.textRoomValue) {
          return
        }
        const textRoomData = this.state.textRoomData.slice();
        textRoomData.push({user: 'Me', message: this.state.textRoomValue});
        for (const key in pcPeers) {
          const pc = pcPeers[key];
          pc.textDataChannel.send(this.state.textRoomValue);
        }
        this.setState({textRoomData, textRoomValue: ''});
      }    
    _switchVideoType() {
        const isFront = !this.state.isFront;
        this.setState({isFront});
        getLocalStream(isFront, function(stream) {
          if (localStream) {
            for (const id in pcPeers) {
              const pc = pcPeers[id];
              pc && pc.removeStream(localStream);
            }
            localStream.release();
          }
          localStream = stream;
          container.setState({selfViewSrc: stream.toURL()});
    
          for (const id in pcPeers) {
            const pc = pcPeers[id];
            pc && pc.addStream(localStream);
          }
        });
    }




     mapHash(hash, func) {
        const array = [];
        for (const key in hash) {
          const obj = hash[key];
          array.push(func(obj, key));
        }
        return array;
    }

    

     leave(socketId) {
        console.log('leave', socketId);
        const pc = pcPeers[socketId];
        const viewIndex = pc.viewIndex;
        pc.close();
        delete pcPeers[socketId];
      
        const remoteList = container.state.remoteList;
        delete remoteList[socketId]
        container.setState({ remoteList: remoteList });
        container.setState({info: 'One peer leave!'});
    }

    _renderTextRoom() {
        return (
          <View style={styles.listViewContainer}>
            <ListView
              dataSource={this.ds.cloneWithRows(this.state.textRoomData)}
              renderRow={rowData => <Text>{`${rowData.user}: ${rowData.message}`}</Text>}
              />
            <TextInput
              style={{width: 200, height: 30, borderColor: 'gray', borderWidth: 1}}
              onChangeText={value => this.setState({textRoomValue: value})}
              value={this.state.textRoomValue}
            />
            <TouchableHighlight
              onPress={this._textRoomPress}>
              <Text>Send</Text>
            </TouchableHighlight>
          </View>
        );
      }

    render() {
        return (
        <View style={styles.container}>
            <Text style={styles.welcome}>
              {this.state.info}
            </Text>
            {this.state.textRoomConnected && this._renderTextRoom()}
            <View style={{flexDirection: 'row'}}>
              <Text>
                {this.state.isFront ? "Use front camera" : "Use back camera"}
              </Text>
              <TouchableHighlight
                style={{borderWidth: 1, borderColor: 'black'}}
                onPress={this._switchVideoType.bind(this)}>
                <Text>Switch camera</Text>
              </TouchableHighlight>
            </View>
            { this.state.status == 'ready' ?
              (<View>
                <TextInput
                  ref='roomID'
                  autoCorrect={false}
                  style={{width: 200, height: 40, borderColor: 'gray', borderWidth: 1}}
                  onChangeText={(text) => this.setState({roomID: text})}
                  value={this.state.roomID}
                />
                <TouchableHighlight
                  onPress={this._press.bind(this)}>
                  <Text>Enter room</Text>
                </TouchableHighlight>
              </View>) : null
            }
            <RTCView streamURL={this.state.selfViewSrc} style={styles.selfView}/>
            {
              this.mapHash(this.state.remoteList, function(remote, index) {
                return <RTCView key={index} streamURL={remote} style={styles.remoteView}/>
              })
            }
          </View>
        );
    }
}
const styles = {
    container: {
        flex: 1,
        backgroundColor: '#ccc',
        borderWidth: 1,
        borderColor: '#000'
    },
    selfView: {
        width: 200,
        height: 150,
      },
      remoteView: {
        width: 200,
        height: 150,
      },
      container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      listViewContainer: {
        height: 150,
      },
};

export default Main;
