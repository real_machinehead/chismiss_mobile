import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    TextInput,
    ListView,
    Platform,
  } from 'react-native';
// import { Platform } from 'react-native';
import {
    RTCPeerConnection,
    RTCMediaStream,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStreamTrack,
    getUserMedia,
} from 'react-native-webrtc';
import io from 'socket.io-client';


window.navigator.userAgent = 'ReactNative';

const configuration = {"iceServers": [{"url": "stun:chismischannel.live:3478"}]};

/*console.ignoredYellowBox = ['Remote debugger'];
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings([
    'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
]);*/

class Main extends Component {
    // Initial state
    constructor(props){
        super(props);

        this.state = {
            videoURL: null,
            isFront: true,
            info: 'Initializing',
            status: 'init',
            roomID: '',
            selfViewSrc: null,
            remoteList: {},
            textRoomConnected: false,
            textRoomData: [],
            textRoomValue: '',
            urlsockets:'',
            name:'bob',
        } 
        //const socket = io('http://localhost:8888',{transports: [ 'websocket', 'polling' ]});

        //this.socket = io('http://localhost:8888/',{ transports: [ 'websocket' ],secure: true, reconnect: true, rejectUnauthorized: false,upgrade: false });
        //this.socket.heartbeatTimeout = 20000;
        //const fs = require('fs');


      var socket = io('http://192.168.22.164:8888',{ transports: [ 'websocket' ]});

      //var socket = io('https://192.168.22.164:8888');
        /*const socket = io({
          // option 1
          ca: fs.readFileSync(__dirname + '/ssl/cert.pem'),
        
          // option 2. WARNING: it leaves you vulnerable to MITM attacks!
          rejectUnauthorized: false
        });*/


        socket.on('news', function (data) {
          console.log(data);
          socket.emit('my other event', { my: 'data' });
        });

       socket.on('connect_error', (err) => {
        console.log('error123',err)
      });

      socket.on('update',()=>this.setState({name:'lyndell'}))
        /*socket.on('update',()=>this.setState({name:'nate'}));
        socket.on('connect_error', (err) => {
          console.log('error123',err)
        });*/
        /*this.state.urlsockets.on('channel2', (data) => {
          console.log('Data recieved from server', data); //this will console 'channel 2'
        });*/
    
    }; 


    componentDidMount() {


    }

    render() {
        return (
        <View style={styles.container}>
            <Text>{this.state.name}</Text>
        </View>
        );
    }
}
const styles = {
    container: {
        flex: 1,
        backgroundColor: '#ccc',
        borderWidth: 1,
        borderColor: '#000'
    },
    selfView: {
        width: 200,
        height: 150,
      },
      remoteView: {
        width: 200,
        height: 150,
      },
      container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      listViewContainer: {
        height: 150,
      },
};

export default Main;
