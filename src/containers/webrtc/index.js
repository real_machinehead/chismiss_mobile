import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    ListView,
    Platform,

  } from 'react-native';
// import { Platform } from 'react-native';
import {
    RTCPeerConnection,
    RTCMediaStream,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStreamTrack,
    getUserMedia,
    mediaDevices
} from 'react-native-webrtc';
import io from 'socket.io-client';
//import { createCipher } from 'crypto';

const socket = io.connect('https://react-native-webrtc.herokuapp.com', {transports: ['websocket'],  pingTimeout: 30000,
pingInterval: 30000});
const pcPeers = {};
let localStream;
class Main extends Component {
    // Initial state
    constructor(props){
        super(props);

        this.state = {
            videoURL: null,
            remoteUrl:null,
            isFront: true,
            status: null,
            selfViewSrc: null,
            remoteList: {},
            textRoomConnected: false,
            textRoomData: [],
            textRoomValue: '',
            localDesc:'',
            remotDesc:'',
            iceCandidate:'',
            isAnswer:false,
            clientId:null,
            connectDisabled:false,
            clientEnable:true,
            remoteiceCandidate:[],
            mediaStream:null,
            roomId:'test',
            onAnswer:null,
        }
    }; 

 
    componentDidMount() {


        var socket = this._onSocketConnection();
        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        const { isFront,roomId } = this.state;
        const remote =this;
        MediaStreamTrack.getSources(sourceInfos => {
            console.log('MediaStreamTrack.getSources', sourceInfos);
            let videoSourceId;
            for (let i = 0; i < sourceInfos.length; i++) {
                const sourceInfo = sourceInfos[i];
                // I like ' than "
                if (sourceInfo.kind === 'video' && sourceInfo.facing === (isFront ? 'front' : 'back')) {
                    videoSourceId = sourceInfo.id;
                }
            }
            
            getUserMedia({
                audio: true,

                video: {
                    mandatory: {
                        minWidth: 500, // Provide your own width, height and frame rate here
                        minHeight: 300,
                        minFrameRate: 30
                    },
                    facingMode: (isFront ? 'user' : 'environment'),
                    optional: (videoSourceId ? [{ sourceId: videoSourceId }] : [])
                }
            }, (stream) => {
                // use arrow function :)
                // (stream) or stream are fine
                console.log('Streaming OK', stream);
                this.setState({
                    videoURL: stream.toURL()
                });
                pc.addStream(stream);
            }, error => {
                console.log('Oops, we getting error', error.message);
                throw error;
            });
        });
        pc.onaddstream=function(e){
            console.log('onaddstream',e);
        }


        socket.on('remotesdpcandidate',function(data){

            console.log('messagelog',data);
            remote._onListeningToSignals(data);
        });

        socket.on('remoteanswer',function(data){
  
            /*if(!remote.state.onAnswer){
                console.log('remoteanswer',data);
                remote.state.onAnswer=data
            }*/
            console.log('remoteanswer',data);
                remote._onCreateAnswer(data);
        });
    }

    componentDidUpdate(prevProps,prevState){

    }

    _onListeningToSignals=(data)=>{

        var socket = this._onSocketConnection();
        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        const remote=this;
        console.log('client Id',data.clientId,this.state.clientId);
        if(data.clientId!=this.state.clientId){
            //const datasdp=data.sdp
            console.log('datasdp',data);
            if(data.sdp){
                pc.setRemoteDescription(new RTCSessionDescription(data.sdp)).then(function () {
                    if(pc.remoteDescription.type == "offer"){

                        /*pc.createAnswer().then(function(answer) {
                            console.log('createanswer', answer);
                            //socket.emit('createanswer',answer);
                            //return pc.setLocalDescription(answer);
                            pc.setLocalDescription(answer,function(){
                                console.log('pc description',pc.localDescription)
                            })
                            .catch(e=>{
                                console.log('error',e);
                            })
                        })*/
                        pc.createAnswer(function(answer){
                            console.log('localdescriptiosss',answer);
                            //socket.emit('createanswer',{clientId:remote.state.clientId,answer:answer});
                            pc.setLocalDescription(answer);
                        })
                        .catch(e=>{
                            console.log('error',e);
                        });                      
                    }
                });
                pc.onsignalingstatechange = function(event) {
                    //container.setState({signalState:event.target.signalingState})
                    console.log('onsignalingstatechange', event.target.signalingState);
                };
            }else{
                const icecandidates=new RTCIceCandidate(data.candidate);
                //console.log('icecandidatesss',icecandidates.candidate);
                //pc.addIceCandidate(new RTCIceCandidate(icecandidates));
                pc.addIceCandidate(icecandidates).catch(e => {
                    console.log("Failure during addIceCandidate(): " + e);
                });
            }

            pc.oniceconnectionstatechange=function(event){
                console.log('statusss',event)
            }
            pc.iceGatheringState=function(event){
                console.log('iceGatheringState',event)
            }
            pc.signalingState=function(event){
                console.log('onsignalingstatechangessss', event.target.signalingState);
            }
        }
        
    }
    _onLocalStreamConnection=()=>{
        
    }

    _onCreateOffer=(data)=>{

    }    



    _onRemoteConnection=(data)=>{

      
    }

    _onSocketConnection=()=>{

        var socket = io('http://192.168.22.162:8888',{transports: ['websocket'],  pingTimeout: 30000,
        pingInterval: 30000});
        return socket;
    }

    _onIceConnection=()=>{
        const configuration = { "iceServers": [{ "url": "stun:stun.l.google.com:19302" }] };

        return configuration;
    }


    _onSendRemoteinfo=()=>{

    }

    _onSendCandidate=()=>{
        var socket = this._onSocketConnection();
        
        //socket.emit('exchange',{iceCandidate:iceCandidate});
        //socket.emit('icecandidate',this.state.iceCandidate);
        //console.log('icecandidate',this.state.iceCandidate);
    }

    _onclientIdChanged=(text)=>{
        //this.state.clientId
        this.setState({clientId:text});
    }


    _onsendPeerCreditials=async()=>{
        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        var socket = this._onSocketConnection();
        const arraycandidate=[]
        pc.createOffer((desc) => {
            pc.setLocalDescription(desc, () => {
                // Send pc.localDescription to peer
                console.log('pc.setLocalDescription',desc);
                /*this.setState({
                  localDesc:desc,
                })*/
                //cket.emit('localdescription',desc);
                socket.emit('peercredentials',{sdp:desc});
            }, (e) => { throw e; });
        }, (e) => { throw e; });

        pc.onicecandidate = function (event) {
            // send event.candidate to peer
            
            if(event.candidate){
                //console.log('ice candidate',event.candidate);
                arraycandidate.push(event.candidate);
               
                socket.emit('peercredentials',{candidate:event.candidate});     
                
                //console.log('ice candidate',event);
            }else{
                //console.log('ice candidatesss',arraycandidate);
                //socket.emit('peercredentials',arraycandidate);    
            }

        };

        pc.onaddstream = function (event) {
            console.log('onaddstream', event.stream);
        };
        pc.oniceconnectionstatechange=function(event){
            console.log('statusss',event)
        }
    }
    

    _onRecievedremoteSignal=()=>{

    }

    _onaswerConnection=async()=>{
        //this._onSendCandidate();
        await this._sendIceCandidate()
        await this._onsendlocalDescription()
    }

    _onCallConnection=(clientId)=>{
        //send ice candidate
        //await this._onsendPeerCreditials()
        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        var socket = this._onSocketConnection();
        this.state.status='caller';
        remote=this;
        console.log('client Id',clientId);

        pc.createOffer(function(desc) {

            pc.setLocalDescription(desc, function () {
              // Send pc.localDescription to peer
              //console.log('localDescription',desc);
              socket.emit('messagesdpcandidate',{clientId:clientId,sdp:desc});

            }, function(e) {});
            
        }, function(e) {});
          
        pc.onicecandidate = function (event) {
            // send event.candidate to peer
            //console.log('icecandidate',event.candidate)
            if(event.candidate){
                socket.emit('messagesdpcandidate',{clientId:clientId,candidate:event.candidate});
            }

        };
        pc.onsignalingstatechange = function(event) {
            //container.setState({signalState:event.target.signalingState})
            console.log('onsignalingstatechange', event.target.signalingState);
        };

        pc.onaddstream = function (event) {
            console.log('onaddstream', event.stream);
        };

    }

    _onCreateAnswer=(data)=>{

        const configuration = this._onIceConnection();
        const pc = new RTCPeerConnection(configuration);
        const remoteanswer= new RTCSessionDescription(data.answer);
        console.log('remote ansower',data);

        
        /*const icecandidates=new RTCIceCandidate(data);
        //console.log('icecandidatesss',icecandidates.candidate);
        //pc.addIceCandidate(new RTCIceCandidate(icecandidates));
        pc.addIceCandidate(icecandidates).catch(e => {
            console.log("Failure during addIceCandidate(): " + e);
        });*/

        
        /*if(data.clientId!=this.state.clientId){
            pc.setRemoteDescription(remoteanswer).catch(e=>{
                console.log("error): " + e);
            });
            pc.onaddstream=function(e){
                console.log('onaddstream',e);
            }
        }*/


    }

    _onAnswerConnection=async()=>{

        var socket = this._onSocketConnection();

        console.log('remoteanswer',this.state.onAnswer);
        this._onCreateAnswer(this.state.onAnswer);
        /*socket.on('remoteanswer',function(data){
            console.log('remoteanswer',data);
            remote._onCreateAnswer(data);
        });

        socket.on('connect_error', (err) => {
            console.log('error123',err)
          });*/
        /*socket.on('remotesdpcandidate',function(data){
            console.log('messagelog234556',data);
            //remote._onListeningToSignals(data);
        });*/


    }
    render() {
        console.log('mediaStream',this.state.mediaStream);
        console.log('remoteurl',this.state.remoteUrl);
        console.log('clientId',this.state.clientId)
        const {clientId}=this.state;
        return (
            <View style={{flex:1,flexDirection:'column'}}>
                  <RTCView streamURL={this.state.videoURL} style={styles.container} />
                  <RTCView streamURL={this.state.remoteUrl} style={styles.container} />
                  <View>
                        <TextInput
                            style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                            onChangeText={this._onclientIdChanged.bind(this)}
                            value={this.state.clientId}
                            enabled={this.state.clientEnable}
                            placeholder='ClientId'
                        />
                  </View>
                  <View style={{height:150,backgroundColor:'green'}}>
                    <TouchableOpacity
                        style={{borderWidth: 1, borderColor: 'black',height:50}}
                        onPress={()=>this._onCallConnection(this.state.clientId)}
                        disabled={this.state.connectDisabled}>
                        <Text>Call</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{borderWidth: 1, borderColor: 'black',height:50}}
                      onPress={this._onAnswerConnection}
                    >
                      <Text>Answer</Text>
                    </TouchableOpacity>
                  </View>

            </View>
           
        );
    }
}
const styles = {
    container: {
        flex: 1,
        backgroundColor: '#ccc',
        borderWidth: 1,
        borderColor: '#000'
    },
    selfView: {
        width: 200,
        height: 150,
      },
      remoteView: {
        width: 200,
        height: 150,
      },
      container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#F5FCFF',
      },
      welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      },
      listViewContainer: {
        height: 150,
      },
};

export default Main;
