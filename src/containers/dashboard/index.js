import React,{ Component,PureComponent } from 'react';
import { Text,View,TouchableOpacity,Platform } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as dasboardActions from '../datas/dashboard/actions'
import styles from './styles';

export class Dashboard extends Component{

        static navigationOptions = {
            title: 'Dashboard',
            headerStyle: {
              backgroundColor: 'white'
            },
            headerTitleStyle :{
                alignSelf: 'center',
                width: Platform.OS==='android'? '90%':'100%',
                textAlign: 'center',
            },
            //headerLeft: null,
    
        }



    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:[],
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm: false,
        }
    }

    componentDidMount(){
        //this._fetchData();
    }

    _fetchData=()=>{

    }

    _onTab1(){
        this.props.navigation.navigate('Chats')
    }
    _onTab2(){

    }
    _onTab3(){

    }
    render(){
        return(
            <View style={styles.container}>

                <View style={styles.content}>
                     <Text> Content </Text>
                </View>

                <View style={styles.footer}>
                    <View style={styles.footerTab1}>
                        <TouchableOpacity onPress={()=>this._onTab1()} style={styles.ButtonTab}>
                            <Text> Chat </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.footerTab2}>
                        <TouchableOpacity onPress={()=>this._onTab2()} style={styles.ButtonTab}>
                            <Text> Calls </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.footerTab3}>
                        <TouchableOpacity onPress={()=>this._onTab3()} style={styles.ButtonTab}>
                            <Text> Contacts </Text>
                        </TouchableOpacity>

                    </View>

                </View>
                
            </View>
        );
    }
}

function mapStateToProps(state){
    
    return{
        dashboard:state,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            dashboard:bindActionCreators(dasboardActions,dispatch)
        }
    }
}

export default connect(
    mapDispatchToProps,
    mapStateToProps
)(Dashboard)
