import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform,Modal,TouchableHighlight,TouchableOpacity,AsyncStorage,SectionList } from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import {
    GeneralContainers,
    GeneralHeaders,
    GeneralContents,
    Messagebox,
    Spinner,
    
} from '../../components';
import Usercontact from '../../components/usercontact';

import Statuspicker from '../../components/picker/status';
import { bindActionCreators } from 'redux';
//import * as chatActions from '../datas/chatlist/actions';
import styles from '../styles';
import chatstyles from './styles';
import {IconButton} from '../../components/common';
import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';
import ContactForm from '../contacts/forms';
import ChannelForm from '../channels/forms';
import Chatsidebar from './chatsidebar';
import * as contactActions from '../datas/contacts/actions';
import * as profileActions from '../datas/profile/actions';
import * as chatActions  from '../datas/chats/actions';
import * as chatlistActions  from '../datas/chatlist/actions'; 
import {
    RTCPeerConnection,
    RTCMediaStream,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStreamTrack,
    getUserMedia,
    mediaDevices
} from 'react-native-webrtc';

import io from 'socket.io-client';
import websockets from '../../globals/websockets';
//import {Spinner} from '../../components';


class Chatllist extends Component{

    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:{
                flagno:1,
                message:'success',
                data:[
                    {
                        firstName:'Kpop',
                        lastName:'Fans',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    },
                    {
                        firstName:'Girls',
                        lastName:'Generation',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    },
                    {
                        firstName:'Black',
                        lastName:'Pink',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    }
                ]
            },
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            channelForm:false,
            contactForm:false,
            onlineStatusForm:false,
            messageBox:true,
            onlineStatus:null,
            chatsider:false,
        };

        var stunurl='stun:chismischannel.live:3478';
        var turnurl='turn:chismischannel.live:3478';
        var username='chismis';
        var credentials='sup3rpassw0rd';
        
        /*this.socket=io.connect('https://chismischannel.live:8888',{transports: ['websocket']});
        
        const configuration = { 
            iceServers: 
            [
                { url:stunurl },
                { urls:stunurl},
                { url:turnurl,username:username,credential:credentials},
                { urls:turnurl,username:username,credential:credentials },
                { urls:[stunurl,turnurl],username:username,credentials:credentials}
            ] 
        };
        this.pc= new RTCPeerConnection(configuration);    
        console.log('webrtc',this.pc);*/


    }
    
    /*static navigationOptions ={
        title: 'CALLS',
        headerStyle:{
            backgroundColor: 'white'
        },
        headerTitleStyle:{
            alignSelf:'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign:'center'
        }
    }*/

    _onHandleOpenWs=(events)=>{

        let websocket=new websockets();
        const ws=websocket.ws;

        const userId=this.props.userid
        console.log('Open ws',events);
          ws.send(JSON.stringify({
            'type': 'login',
            'content': {
                'userId': userId,
            }
        }));

        setTimeout(() => {
            ws.send(JSON.stringify({
                'type': 'view_clients'
            }));
        }, 1000);

    }

    _onfetchMessages=()=>{

    }
    _onHandleMessageWs=(event)=>{

        console.log('eventtts',event.data);
        /*console.log('events',event.data);*/
        //console.log('routeName',this.props.navigation.state.routeName);
            if(event.data instanceof ArrayBuffer){

                const datas=String.fromCharCode.apply(null, new Uint8Array(event.data));
                console.log('data',JSON.parse(datas))
                /*reader.onload = () => {
                    console.log("Result:234 " + reader.result);
                }*/
                var data=JSON.parse(datas);
                console.log('data type',data.type);
                let routes=this.props.routes.routes;
                switch (data.type) {
                    case 'clients_list':
                        var payload={onlineusers:data.content,contactlist:this.props.contacts.usercontactlist}
                        
                        console.log('routessss',this.props.routes.routes);
                        if(routes=='contact'){
                            console.log('contact here')
                            this.props.actions.contacts.onlineusersupdate(payload)
                            this.props.actions.chats.onupdateuserlist({onlineUser:data.content})
                        }
                       console.log('clients_list',payload)

                        //this.props.actions.chats.
                        break;
                    case 'send_message':
                        if(routes=='contact'){
                            var payload={content:data.content,usrcontactlist:this.props.contacts.usercontactlist}
                            console.log('data.content',data.content)
                            this.props.actions.contacts.onnewmessage(payload)
                        }
                        break;
                    default:
                        break;
                }
            
            }

    }

    _onHandleError=(data)=>{
        console.log('error',data);
    }

    _onHandleClose=(event)=>{


            console.log('Socket is closed. Reconnect will be attempted in 1 second.', event);
            setTimeout(() => {
                let websocket=new websockets();
                const ws=websocket.ws;
                //var url='wss://chismischannel.live:9999/';
                //const ws= new WebSocket(url);
                //console.log('websocket',ws);
                ws.onopen = (e) => { this._onHandleOpenWs(e); };
                ws.onmessage = (data) => { this._onHandleMessageWs(data); };
                ws.onerror = (data) => { this._onHandleError(data); };
                ws.onclose = (data) => { this._onHandleClose(data); };

            }, 1000)
    }

    componentDidMount(){

        let websocket=new websockets();
                //this.ws= new WebSocket('wss://chismischannel.live:9999/');
        /*ws= websocket.ws;
        ws.onopen = (e) => { this._onHandleOpenWs(e); };
        ws.onmessage = (data) => { this._onHandleMessageWs(data); };
        ws.onerror = (data) => { this._onHandleError(data); };
        ws.onclose = (data) => { this._onHandleClose(data); };*/

        //this._fetchData();
        //console.log('socketssss',this.socket);
        //const self=this;
        //this.socket.on('connect', function (sockets) {
            //console.log('socket',sockets);
            /*self.emit('connectionReady', connection.getSessionid());
            self.sessionReady = true;
            self.testReadiness();*/
            //console.log('connections',self.socket.id)
            //self.socket.emit('connectionReady',self.socket.id)
            /*self.socket.on('message',function(data){
                console.log('datass',data);
            })*/
        //});


        //this.socket.on('message',function(data){
            //console.log('datass',data);
        //})

        this._fetchData()
    }


    componentDidCatch(error, info) {
        // Example "componentStack":
        //   in ComponentThatThrows (created by App)
        //   in ErrorBoundary (created by App)
        //   in div (created by App)
        //   in App
        console.log('errorrrrr',error);
    }

    _fetchData=async()=>{

        const payload={
            accesstoken:this.props.acesstoken,
            userid:this.props.userid,
            header:{'Content-Type':'application/json','Authorization':'Bearer ' + this.props.acesstoken}
        }

        await this.props.actions.chatlist.fetchdatacontact(payload,{Token:this.props.acesstoken});

    }

    _keyExtractor = (item, index) => item.firstName;

    _getNotification=()=>{
        this.props.navigation.navigate('Notification')
    }

    _getProfile=()=>{
        this.props.navigation.navigate('Profile')
    }
    _getOnSearch=()=>{
        this.props.navigation.navigate('Search')
    }

    _onChat(){
        this.props.navigation.navigate('Chats',{
            name:'test',
        })
    }

    _addContact=()=>{
        this.setState({showForm:true,contactForm:true,channelForm:false})
        this.props.actions.contacts.contactlistreset('');
    }

    _addChannel=()=>{
        this.setState({showForm:true,channelForm:true,contactForm:false})
    }

    _closeContactForm=()=>{
        this.setState({showForm:false,contactForm:false,channelForm:false})
    }
    _closeChannelForm=()=>{
        this.setState({showForm:false,channelForm:false,contactForm:false})
    }

    _contactchanged=(text)=>{
        this.props.actions.contacts.contactchanged(text); 
    }
   
    _onContactSearch=async()=>{
        var payload={
            searchvalue:this.props.contacts.search,
            header:{token:this.props.acesstoken}
        }
        
        await this.props.actions.contacts.search(payload);
        //console.log('payload',payload);
        //console.log('accesstoken',this.props.acesstoken);
    }

    _insertcontacts=(datas)=>{

        dataobject={name:datas.Username,userid:datas.UserID}
        //console.log('loggsss',dataobject);

        if(this.props.contacts.contactlist){

            var filterobj = this.props.contacts.contactlist.filter(function(e){
                return e.name==datas.Username;
            });

            if(filterobj.length){
                console.log('Existed',this.props.contactlist);
                //console.log('data List',this.state.data);
            }else{
                this.props.actions.contacts.contactlistupdate(dataobject);
                //console.log('data List',this.state.data);
            }
            //this.props.actions.contacts.search(payload);

         
        }else{
            //console.log('contacts',this.props.contacts.contactlist);
            var arrdata=[];
            arrdata.push(dataobject); 
            console.log('loggs',arrdata); 
            this.props.actions.contacts.contactlist(dataobject);
        }



    }

    _closeContactForm=()=>{
        this.setState({showForm:false,contactForm:false})
    }

    _onrendercontactlist=(status,data)=>{
        console.log('statusss',data)
        if(data){
            switch(status[0]){
                case 0:  
                        return(
                            <View style={{flex:1}}>
                                <Modal
                                    animationType="slide"
                                    transparent={true}
                                    visible={this.state.messageBox}
                                    onRequestClose={() => {
                                        Alert.alert('Modal has been closed.');
                                }}>
                                    <Messagebox message={status[1]} onClose={this._closeMessagebox}/>
            
                                </Modal>
                            </View>
                        );               
                break;
                case 2:
                    return(
                        <View style={styles.sendButtonStyleContainer}>
                            <Spinner size='large' />   
                        </View>    
                    );
                break;
                default:
                    return(
                            <SectionList
                                sections={data}
                                extraData={this.state}
                                renderSectionHeader={ ({section}) => this._onRenderSectionHeader(section)}
                                //renderItem={ ({item}) => this._onRendserSectionItem(item,currentUserId)}
                                renderItem={({item,index})=> this._onRendserSectionItem(item)}
                                /*renderItem={({item,index})=> 
                                        <View style={{flex:1}}>
                                            <Text style={{color:'black'}}>{item.Username}</Text>
                                        </View>
                                }*/
                                
                                keyExtractor={ (item, index) => item + index }
                                //ref={ref => (this.sectionListRef = ref)}
                                //getItemLayout={this.getItemLayout}
                                onEndReached={()=>console.log('end of the line')}
                                //initialScrollIndex={this.props.totalitems}
                                //viewabilityConfig={VIEWABILITY_CONFIG}
                                //removeClippedSubviews={true}
                                //initialNumToRender={50}
                        
                            />
                    );                
            }
        }

    }

    _onRenderSectionHeader=(section)=>{
        console.log('sectionssss',section);
        return(
            <View style={chatstyles.sectionFooterStyle}>
                <Text style={chatstyles.sectionFooterTextStyle}> { section.title } </Text> 
            </View>
        );
    }

    _onGetContactList=(item)=>{
        console.log('Itemsssss123',`${item.Username}`);
        return(
         <Usercontact fullname={`${item.Username}`} />
        )
    }
    _onGetContactChannelList=(item)=>{
        console.log('chanelllistsss',item)
    }
    _onRendserSectionItem=(item)=>{
       
        return(
            <View>
                <View>
                    {
                        item.Username ?
                            //<Text>{`${item.Username}`}</Text>
                            this._onGetContactList(item)
                        :
                            null
                    }
                    
                </View>
                <View>
                    {
                        item.Description ?

                            //<Text>{`${item.Description}`}</Text>
                            this._onGetContactChannelList(item)
                        :
                            null
                    }

                </View>               
            </View>

        )
    }


    _renderForm=()=>{
        if(this.state.contactForm){
            return(
                <View style={{flex:1}}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.contactForm}
                        onRequestClose={() => {
                            Alert.alert('Modal has been closed.');
                        }}>
                            <ContactForm   
                                onClose={this._closeContactForm} 
                                contactchanged={this._contactchanged} 
                                contactsearch={this._onContactSearch}
                                status={this.props.contacts.status}
                                contactstatus={this.props.contacts.contactstatus}
                                searchdata={this.props.contacts.data}
                                contactlist={this.props.contacts.contactlist}
                                insertcontacts={this._insertcontacts}
                                sendcontacts={this._sendcontacts}
                            />
    
                    </Modal>
                </View>  
            );
        }
            return(
                <View style={{flex:1}}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.channelForm}
                        onRequestClose={() => {
                            Alert.alert('Modal has been closed.');
                        }}>
                            <ChannelForm onClose={this._closeChannelForm}/>

                    </Modal>
                </View>  
            );


    }

    _sendcontacts=async()=>{
        
        var userid=this.props.contacts.contactlist.map(function(e){
            return e.userid
        });

        for(x=0;x<userid.length;x++){

            let data= new FormData();
            data.append('contactuserid', userid[x]);
            await this.props.actions.contacts.addcontacts(data,{token:this.props.acesstoken});

        }
        this.setState({messageBox:true});
        
    }

    _closeMessagebox=async()=>{
         await this.props.actions.contacts.contactstatus([3,'']);
         this.setState({messageBox:false})
    }

    _onStatusclick=()=>{
        console.log('status here');
        this.setState({onlineStatusForm:!this.state.onlineStatusForm})
  
    }

    _onChangeOnlineStatus=async(status)=>{
        this.setState({
            onlineStatus:status.name,
            onlineStatusForm:false,
        });

        let data= new FormData()
        let payload={
            statusid:status.id,
            header:{Token:this.props.acesstoken},
            callsign:this.props.profile.callsign,
            userid:this.props.userid,
        }

        //console.log('payloadsss',payload);

        data.append('callsign',payload.callsign);
        data.append('statusID',payload.statusid);
        await this.props.actions.profile.updateonlinestatus(data,payload.header);
        await this.props.actions.profile.fetchprofile(payload);
    }

    _onSelectOnlineStatusClick=(status)=>{
        if(status){
            return(
                <View style={chatstyles.userStatusStyle}>
                                            
                    <Statuspicker 
                        onChangeOnlineStatus={this._onChangeOnlineStatus}
                        onlineStatus={this.state.onlineStatus}
                    />

                </View>  
            );
        }
        return null;

    }
    _onRenderOnlineStatus=(userstatus)=>{

        switch(userstatus){
            case 'Online':
                return(
                    <View style={chatstyles.smallonlineroundavatar}>
                        <TouchableHighlight onPress={this._onStatusclick} >
                            <Text style={chatstyles.smallonlineroundavatar}></Text>
                        </TouchableHighlight>
                    </View>
                );            
            break;

            case 'Busy':
                return(
                    <View style={chatstyles.smallbusyroundavatar}>
                        <TouchableHighlight onPress={this._onStatusclick} >
                            <Text style={chatstyles.smallbusyroundavatar}></Text>
                        </TouchableHighlight>
                    </View>
                );
            break;
            case 'Offline':
                    return(
                        <View style={chatstyles.smallinvisibleroundavatar}>
                            <TouchableHighlight onPress={this._onStatusclick} >
                                <Text style={chatstyles.smallinvisibleroundavatar}></Text>
                            </TouchableHighlight>
                        </View>
                    );

            break;
        }

    }

    _onClose=()=>{
        this.setState({chatsider:false})
    }

    _onAboutUsClick=()=>{
        this._onClose();
        this.props.navigation.navigate('AboutUs')
    }
    _onTermsClick=()=>{
        this._onClose();
        this.props.navigation.navigate('Terms')
    }
    _onPrivacyClick=()=>{
        this._onClose();
        this.props.navigation.navigate('Privacy')
    }
    _onSettingsClick=()=>{
        this._onClose();
        this.props.navigation.navigate('Settings')
    }
    _onLogoutClick=()=>{
        this._onClose();
        console.log('redux reset all')
        AsyncStorage.removeItem('usercredentials');
        this.props.navigation.navigate('Login')
    }
    _onChatSidebarshow=()=>{
        this.setState({chatsider:true})
    }
    render(){
        const data=this.props.chathistory;
        const contactstatus = this.props.status;
        //const onlineStatus =this.props.profile.data.Users;
        const onlineStatus = this.props.profile.onlinestatus;
        const {profile} = this.props;
        console.log('profiles232345',this.props.chathistory);
        //console.log('profile status',this.props.profile.data.Users);
        //console.log('chatsider',this.state.chatsider)
        return(
            <GeneralContainers>

                <GeneralHeaders>
                    <View style={styles.headerTitle}>

                        <View style={chatstyles.notificationIcon}>
                            <Avatar
                                small
                                rounded
                                icon={{name:'notifications',type:'ionic'}}
                                //leftAvatar={{ title:'test', source: { uri: 'https://www.google.com.ph/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjvz96ux-LeAhUTzmEKHaqOBlUQjRx6BAgBEAU&url=http%3A%2F%2Fhxh.wikia.com%2Fwiki%2FGon_Freecss&psig=AOvVaw34CXBJgYgyxdC9mYZs8rLB&ust=1542789089061843' } }}
                                //chevron
                                onPress={() => this._getNotification()}
                                activeOpacity={0.7}
                            /> 
                        </View>
   
                        <View style={chatstyles.profileIcon}>
                            <View style={chatstyles.profileAvatar}> 
                                <View style={{height:50,justifyContent:'center',alignItems:'center',marginLeft:15}}>
                                    {
                                        this.props.nameinitialss[0]==2 ? 
                                            <Spinner />
                                        :
                                            <IconButton onPress={()=>this._getProfile()}>

                                                <Text style={{fontSize:20}}>{this.props.nameinitialss}</Text>
                                                
                                            </IconButton>                                       

                                    }
                                </View>
                                {this._onRenderOnlineStatus(onlineStatus)}
                            </View>
                        </View>     
                        <Modal
                            animationType={"none"}
                            transparent={true}
                            visible={this.state.chatsider}
                            onDismiss={this._onClose}
                            onRequestClose={() => this._onClose()}
                            onBackdropPress={() => this._onClose()}
                        >  

                        <TouchableOpacity
                            style={chatstyles.modalContainer}
                            onPress={() => this._onClose()}
                            //onPressOut={()=>this._closeContactForm()}
                        >
                                <Chatsidebar 
                                    chatsider={this.state.chatsider} 
                                    onClose={this._onClose} 
                                    aboutus={this._onAboutUsClick} 
                                    terms={this._onTermsClick}
                                    privacy={this._onPrivacyClick}
                                    settings={this._onSettingsClick}
                                    logout={this._onLogoutClick}
                                />
                        </TouchableOpacity>                        

                        </Modal>
                        <View style={chatstyles.searchIcon}>
                            <Avatar
                                    small
                                    rounded
                                    icon={{name:'more',type:'ionic'}}
                                    //leftAvatar={{ title:'test', source: { uri: 'https://www.google.com.ph/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjvz96ux-LeAhUTzmEKHaqOBlUQjRx6BAgBEAU&url=http%3A%2F%2Fhxh.wikia.com%2Fwiki%2FGon_Freecss&psig=AOvVaw34CXBJgYgyxdC9mYZs8rLB&ust=1542789089061843' } }}
                                    //chevron
                                    //onPress={() => this._getOnSearch()}
                                    onPress={this._onChatSidebarshow}
                                    activeOpacity={0.7}
                            /> 
                        </View>                                    
                    </View> 
   
                </GeneralHeaders>

                <GeneralContents>
                    {
                        this.state.showForm ?
                            this._renderForm()
                        :                
                            <View>

                                {/*this._onSelectOnlineStatusClick(this.state.onlineStatusForm)*/}
                                {   
                                    this._onrendercontactlist(contactstatus,data)                                   
                                } 
                                {/*<ActionButton buttonColor="rgba(231,76,60,1)">
                                    <ActionButton.Item buttonColor='#9b59b6' title="Add Contacts" onPress={() => this._addContact()}>
                                        <Icon name="md-create" style={styles.actionButtonIcon} />
                                    </ActionButton.Item>
                                    <ActionButton.Item buttonColor='#3498db' title="Invite to Channel" onPress={() => this._addChannel()}>
                                        <Icon name="md-notifications-off" style={styles.actionButtonIcon} />
                                    </ActionButton.Item>
                            </ActionButton>*/}
                            </View>
                    }
   
                </GeneralContents>

            </GeneralContainers>
        );
    }
}
function mapStateToProps(state){
    //console.log('logstates23467',state);
    return{
        acesstoken:state.security.login.accesstoken ? state.security.login.accesstoken : state.security.login.data.Login[0].Token,
        userid:state.security.login.userid,
        profile:state.datareducer.profile,
        contacts:state.datareducer.contacts,
        userlist:state.datareducer.chats,
        routes:state.datareducer.routes,
        fullname:state.datareducer.fullname,
        nameinitialss:state.datareducer.profile.nameinitialss,
        chathistory:state.datareducer.chatlist.data,
        status:state.datareducer.chatlist.status
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            contacts:bindActionCreators(contactActions,dispatch),
            profile:bindActionCreators(profileActions,dispatch),
            chats:bindActionCreators(chatActions,dispatch),
            chatlist:bindActionCreators(chatlistActions,dispatch)
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chatllist)