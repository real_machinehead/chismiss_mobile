import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform,Modal,TouchableHighlight,TouchableOpacity } from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import {GeneralContainers,GeneralHeaders,GeneralContents,Messagebox,Spinner} from '../../components';
import Statuspicker from '../../components/picker/status';
import { bindActionCreators } from 'redux';
import * as chatActions from '../datas/chatlist/actions';
import styles from '../styles';
import chatstyles from './styles';
import {IconButton} from '../../components/common';
import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';
import ContactForm from '../contacts/forms';
import ChannelForm from '../channels/forms';
import * as contactActions from '../datas/contacts/actions';
import * as profileActions from '../datas/profile/actions';


class Chatsidebar extends Component{

    constructor(props){
        super(props);

        this.state={
            loading:false,
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            channelForm:false,
            contactForm:false,
            onlineStatusForm:false,
            messageBox:true,
            onlineStatus:null,

        };
    }
    
    /*static navigationOptions ={
        title: 'CALLS',
        headerStyle:{
            backgroundColor: 'white'
        },
        headerTitleStyle:{
            alignSelf:'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign:'center'
        }
    }*/


    componentDidMount(){
        this._fetchData();
    }


    _fetchData=()=>{

    }

    _keyExtractor = (item, index) => item.firstName;


    _closeContactForm=()=>{
        this.setState({showForm:false,contactForm:false})
    }

    _onStatusclick=()=>{
        console.log('status here');
        this.setState({onlineStatusForm:!this.state.onlineStatusForm})
  
    }

    _onAboutPress=()=>{
        this.props.aboutus()
    }

    _onTermsPress=()=>{
        this.props.terms()
    }
    _onPrivacyPress=()=>{
        this.props.privacy()
    }
    _onSettingsPress=()=>{
        this.props.settings()
    }
    _onLogoutPress=()=>{
        this.props.logout()
    }
    render(){
        //const data=this.state.data.data;
        //const contactstatus = this.props.contacts.contactstatus;
        //const onlineStatus =this.props.profile.data.Users;
        //const onlineStatus = this.props.profile.onlinestatus;
        //console.log('profile status',this.props.profile.data.Users);
        return(
            <View style={chatstyles.chatSidebarContainerStyle}>
                <View style={chatstyles.chatSiderbarContentStyle}>

                    <View style={chatstyles.chatSidebarButtonContainer}>
                        <TouchableOpacity
                            style={chatstyles.chatSidebarButtons}
                            onPress={() => this._onAboutPress()}
                            //onPressOut={()=>this._closeContactForm()}
                        >
                            <Text>About</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={chatstyles.chatSidebarButtonContainer}>
                        <TouchableOpacity
                            style={chatstyles.chatSidebarButtons}
                            onPress={() => this._onTermsPress()}
                        >
                            <Text>Terms</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={chatstyles.chatSidebarButtonContainer}>
                        <TouchableOpacity
                            style={chatstyles.chatSidebarButtons}
                            onPress={() => this._onPrivacyPress()}
                        >
                            <Text>Privacy</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={chatstyles.chatSidebarButtonContainer}>
                        <TouchableOpacity
                            style={chatstyles.chatSidebarButtons}
                            onPress={() => this._onSettingsPress()}
                        >
                            <Text>Settings</Text>
                        </TouchableOpacity>   
                    </View>

                    <View style={chatstyles.chatSidebarButtonContainer}>
                        <TouchableOpacity
                            style={chatstyles.chatSidebarButtons}
                            onPress={() => this._onLogoutPress()}
                        >
                            <Text>Log-out</Text>
                        </TouchableOpacity>
                    </View>

                </View>


            </View>
        );
    }
}
function mapStateToProps(state){
    //console.log('logstates23467',state.datareducer);
    return{
        acesstoken:state.security.login.accesstoken,
        userid:state.security.login.userid,
        profile:state.datareducer.profile,
        contacts:state.datareducer.contacts,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            contacts:bindActionCreators(contactActions,dispatch),
            profile:bindActionCreators(profileActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chatsidebar)