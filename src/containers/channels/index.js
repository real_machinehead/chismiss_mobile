import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform,Modal,TouchableHighlight,SafeAreaView } from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from '../styles';
import Icon from 'react-native-vector-icons/Ionicons';
import chanelStyles from './styles';
import {GeneralContainers,GeneralHeaders,GeneralContents} from '../../components';
import {IconButton,TextArea} from '../../components/common';
import ActionButton from 'react-native-action-button';
import ContactForm from './forms';
import * as channelActions from '../datas/Channel/actions';

 class Channels extends Component{

    constructor(props){
        super(props);
        this.state={
            loading:false,
            data:{
                flagno:1,
                message:'success',
                data:[
                    {
                        firstName:'Kpop',
                        lastName:'Fans',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    },
                    {
                        firstName:'Girls',
                        lastName:'Generation',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    },
                    {
                        firstName:'Black',
                        lastName:'Pink',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    }
                ]
            },
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
        };;
    }
    
    componentDidMount(){
        this._fetchData();
    }


    _fetchData=()=>{

    }

    _keyExtractor = (item, index) => item.firstName;

    _onChat(){
        this.props.navigation.navigate('Chats',{
            name:'test',
        })
    }

    _addContact=()=>{
        this.setState({showForm:true})
    }

    _closeContactForm=()=>{
        this.setState({showForm:false})
    }


    render(){
        const data=this.state.data.data;
        return(
            <GeneralContainers>

                <GeneralHeaders>

                    <View style={styles.headerTitle}>
                        <Text>CONTACTS</Text>
                    </View>

                </GeneralHeaders>

                <GeneralContents>
                    {
                        this.state.showForm ?
                    <SafeAreaView>
                        <View style={{flex:1}}>
                            <Modal
                            animationType="slide"
                            transparent={true}
                            visible={this.state.modalVisible}
                            onRequestClose={() => {
                                Alert.alert('Modal has been closed.');
                            }}>
                                <ContactForm   onClose={this._closeContactForm}/>

                            </Modal>
                        </View>
                    </SafeAreaView>
                        :
                        <View style={chanelStyles.flatlist}>

                        <FlatList
                            data={data}
                            keyExtractor={this._keyExtractor}
                            renderItem={({ item }) => (
                                <ListItem
                                    roundAvatar
                                    title={`${item.firstName} ${item.lastName}`}
                                    subtitle={item.date}
                                    avatar={<Avatar
                                            small
                                                        rounded
                                                        title={`${item.lastName.charAt(0)}${item.firstName.charAt(0)}`}
                                                        onPress={() => this._onChat()}
                                                        activeOpacity={0.7}
                                        />}
                                                    onPress={()=>this._onChat()}
                                />
                                        
                            )}

                        />  
                            <ActionButton buttonColor="rgba(231,76,60,1)">
                                <ActionButton.Item buttonColor='#9b59b6' title="Add Contacts" onPress={() => this._addContact()}>
                                    <Icon name="md-create" style={styles.actionButtonIcon} />
                                </ActionButton.Item>
                                <ActionButton.Item buttonColor='#3498db' title="Invite to Channel" onPress={() => console.log("Notes tapped")}>
                                    <Icon name="md-notifications-off" style={styles.actionButtonIcon} />
                                </ActionButton.Item>
                            </ActionButton>   
                
                    </View>
                    }
                </GeneralContents>

            </GeneralContainers>
        );
    }
}


function mapStateToProps(state){
    //console.log('logstates234',state.datareducer);
    return{
        login:state.security,
        profile:state.datareducer.profile,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            contacts:bindActionCreators(channelActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Channels)