import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform,Modal,TouchableHighlight,SafeAreaView,TextInput } from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import styles from './styles';

export default class FormChannel extends PureComponent{

    constructor(props){
        super(props);
        this.state={
            loading:false,
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            modalVisible: false,
            data:[],
            email:'',
        };
    }

    setModalVisible(visible) {
        this.props.onClose();
    }
    
    _insertcontacts=(datas)=>{
        //dataobject={name:datas}

        //this.state.data.push(dataobject);
        console.log(this.state.data);

        /*var found = Object.keys(this.state.data).filter(function(key) {
            return this.state.data[key] === 'test1';
          });
          if (found.length) {
             alert('exists');
          }*/

    }
    _onEmailChanged=(text)=>{
        this.setState({
            email:text,
        });
    }
    _keyExtractor = (item, index) => item.chatId;


    render(){
        return(
                <View style={{flex:1,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                    <View style={{flexDirection:'column',justifyContent:'center',alignItems:'flex-start',width:'90%',height:Platform.OS==='android'? '90%':'70%',backgroundColor:'red'}}>
                                
                        <View style={styles.textContainerStyle}>
                            <View style={styles.textInput}>
                                <TextInput
                                        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                                        onChangeText={this._onEmailChanged.bind(this)}
                                        value={this.state.email}
                                        placeholder='Channel Name'
                                />
                            </View>
                            <View style={styles.addButtonStyleContainer}>
                                <TouchableHighlight style={styles.addbuttonStyle}
                                    onPress={() => {
                                        this._insertcontacts(this.state.email);
                                    }}>
                                    <Text>Add</Text>
                                </TouchableHighlight>
                            </View>
                        </View>                                 
                        <View style={styles.flatlistForm}>
                            <FlatList
                                data={this.props.data}
                                extraData={this.state}
                                keyExtractor={this._keyExtractor}
                                renderItem={this._renderItem}
                            />
                        </View>
                        <View style={styles.sendButtonStyleContainer}>
                            <TouchableHighlight style={styles.sendButtonStyle}
                                    onPress={() => {
                                        this.setModalVisible(!this.state.modalVisible);
                                    }}>
                                <Text>Send</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
        )
    }
}