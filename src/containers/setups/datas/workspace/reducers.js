import * as actiontypes from './actiontypes';
import { combineReducers } from 'redux';
import { constants } from '../../../../constants';


const initialstate='';
const initialstatus=constants.status.LOADING


export const data = (state=initialstate, action)=>{
    switch(action.type){
        case actiontypes.INITIALIZE:
            return action.payload;
            break;
        case actiontypes.UPDATE:
            return action.payload;
            break;
        default:
            return state;
    }   
};


export const status = (state=initialstatus,action)=>{
    switch(action.type){
        case actiontypes.STATUS:
            return action.payload
            break;
        default:
            return state;
    }
};

export const reducers = combineReducers({
    data:data,
    status:status
});