import *  as api from './api';
import * as actionTypes from './actiontypes';
import {constants} from '../../../../constants';


export const reset = payload=>({
    type:actionTypes.RESET,
    payload:payload 
});


export const update = payload=>({
    type:actionTypes.UPDATE,
    payload:payload
});


export const status = payload=>({
    type:actionTypes.STATUS,
    payload:payload
});

export const init = payload =>({
    type:actionTypes.INITIALIZE,
    payload:payload
});

export const regfullnamechanged = payload=>({
    type:actionTypes.REGFULLNAMECHANGED,
    payload:payload
});

export const regemailchanged = payload=>({
    type:actionTypes.REGEMAILCHANGED,
    payload:payload
});

export const regNicknamechanged=payload=>({
    type:actionTypes.REGNICKNAMECHANGED,
    payload:payload
});

export const regUsernamechanged=payload=>({
    type:actionTypes.REGUSERNAMECHANGED,
    payload:payload
});

export const regPasswordchanged=payload=>({
    type:actionTypes.REGPASSWORDCHANGED,
    payload:payload
});

export const regConfirmPasswordchanged=payload=>({
    type:actionTypes.REGCONFIRMPASSWORDCHANGED,
    payload:payload
});

export const regIsanonymous=payload=>({
    type:actionTypes.REGISANONYMOUS,
    payload:payload
});


export const post = payload => 
    dispatch =>{
        let objResponse={};

        dispatch(status(constants.status.LOADING))
        api.get(payload)
        .then((response)=>response)
        .then((res)=>{
            console.log('loggsss',res);
            objResponse={...res}

            if(res.Status.IsSuccess){
                //console.log('loggsss1234',res.Data.Login[0].Username)
                dispatch(init(res.Data))
                dispatch(status([1,'success']));
                //dispatch(accesstoken(res.Data.Login[0].Token))
                //dispatch(userid(res.Data.Login[0].UserID))
            }else{
                dispatch(status([0,res.Status.Message]));
            }

        })
        .then(()=>{
            /*dispatch(status([
                objResponse.flagno  || 0,
                objResponse.message || constants.status.ERROR[1]
            ]));*/
        })
        .catch((exception)=>{
            dispatch(status([
                0,exception.message
            ]));
        });
    };