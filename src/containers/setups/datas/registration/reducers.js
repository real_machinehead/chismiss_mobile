import * as actiontypes from './actiontypes';
import { combineReducers } from 'redux';
import { constants } from '../../../../constants';


const initialstate='';
const initialstatus=constants.status.INITIALIZE


export const data = (state=initialstate, action)=>{
    switch(action.type){
        case actiontypes.INITIALIZE:
            return action.payload;
            break;
        case actiontypes.UPDATE:
            return action.payload;
            break;
        case actiontypes.RESET:
            return {};    
            break;
        default:
            return state;
    }   
};


export const status = (state=initialstatus,action)=>{
    switch (action.type){
        case actiontypes.STATUS:
            return action.payload;
            //return [2,''];
            break;
        case actiontypes.RESET:
            return [3,''];
            break;
        case actiontypes.EMAIL:
            return [3,''];
            break;
        case actiontypes.PASSWORD:
            return [3,''];
            break;    
        default:
            return state;
    }
};

export const fullname = (state=initialstate,action)=>{
    switch(action.type){
        case actiontypes.REGFULLNAMECHANGED:
            return action.payload
            break;
        case actiontypes.RESET:
            return null;
            break;
        default:
            return state;
    }
}

export const email = (state=initialstate,action)=>{
    switch(action.type){
        case actiontypes.REGEMAILCHANGED:
            return action.payload
            break;
        case actiontypes.RESET:
            return null;
            break;
        default:
            return state;
    }
}

export const nickname=(state=initialstate,action)=>{
    switch(action.type){
        case actiontypes.REGNICKNAMECHANGED:
            return action.payload
            break;
        case actiontypes.RESET:
            return null;
            break;
        default:
            return state;
    }
}

export const username=(state=initialstate,action)=>{
    switch(action.type){
        case actiontypes.REGUSERNAMECHANGED:
            return action.payload
            break;
        case actiontypes.RESET:
            return null;
            break;
        default:
            return state;
    }
}

export const password=(state=initialstate,action)=>{
    switch(action.type){
        case actiontypes.REGPASSWORDCHANGED:
            return action.payload
            break;
        case actiontypes.RESET:
            return null;
        default:
            return state;
    }
}

export const confirmpassword=(state=initialstate,action)=>{
    switch(action.type){
        case actiontypes.REGCONFIRMPASSWORDCHANGED:
            return action.payload
            break;
        case actiontypes.RESET:
            return null;
        default:
            return state;
    }
}

export const isanonymous=(state=initialstate,action)=>{
    switch(action.type){
        case actiontypes.REGISANONYMOUS:
            return action.payload
            break;
        default:
            return state;
    }
}

export const reducers = combineReducers({
    data:data,
    status:status,
    isanonymous:isanonymous,
    fullname:fullname,
    email:email,
    nickname:nickname,
    username:username,
    password:password,
    confirmpassword:confirmpassword,
});