import { combineReducers } from 'redux';
import {reducers as registerReducer } from './registration/reducers';
import {reducers as invitesReducer } from './invites/reducers';
import {reducers as chanelReducer } from './channels/reducers';
import {reducers as workspaceReducer } from './workspace/reducers';

export const reducers = combineReducers({
    register:registerReducer,
    invites:invitesReducer,
    channels:chanelReducer,
    workspace:workspaceReducer
});