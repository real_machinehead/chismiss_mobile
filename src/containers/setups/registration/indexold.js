import React, { Component, PureComponent } from 'react';
import { Text,View,FlatList,Platform } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as registrationActon from '../datas/registration/actions';
import { CardSection } from '../../../components';
import styles from './styles';
import { Button,Textbox,TextArea } from '../../../components/common';
import { CheckBox } from 'react-native-elements'

export class Registration extends Component{

    static navigationOptions = {
        title: 'Registration',
        headerStyle: {
          backgroundColor: 'white'
        },
        headerTitleStyle :{
            alignSelf: 'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign: 'center',
        },
        //headerLeft: null,

    }
    
    
    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:[],
            page:1,
            seed:1,
            error:null,
            refreshing:false,
            showForm:false,
            checked:false,
        };
    }


    componentDidMount(){
        this._fetchData();
    }

    _fetchData=()=>{

    }

    _onRegistration(){

    }

    _onCancel(){
        
    }
    _onCheck(){
        this.setState({checked:!this.state.checked})
    }

    _onFullnameChanged=(text)=>{
        console.log('logggs',text);
        //this.props.actions.registration.regfullnamechanged(text);
    }

    _onEmailChanged=(text)=>{
        this.props.actions.registration.regemailchaged(text);
    }
    
    _onNicknameChanged=(text)=>{
        this.props.actions.registration.regnicknamechanged(text);
    }

    _onUsernameChanged=(text)=>{
        this.props.actions.registration.regusernamechanged(text);
    }

    _onPasswordChanged=(text)=>{
        this.props.actions.registration.regpasswordchanged(text);
    }

    _onConfirmPasswordChanged=(text)=>{
        this.props.actions.registration.regconfirmpasswordchanged(text);
    }

    _renderAnonymous(){
        console.log('anonymous here');
        return(
        <View>       
            <CardSection>
                <View style={styles.textInput}>
                    <TextArea 
                        //label='Password'
                        placeholder='Nickname'
                        secureTextEntry={false}
                        value={this.props.username}
                        onChangeText={this._onNicknameChanged.bind(this)}
                    />
                </View>   
            </CardSection>  

            <CardSection>
                <View style={styles.textInput}>
                    <TextArea 
                        //label='Password'
                        placeholder='Username'
                        secureTextEntry={false}
                        value={this.props.username}
                        onChangeText={this._onUsernameChanged.bind(this)}
                    />
                </View>   
            </CardSection>  

            <CardSection>
                <View style={styles.textInput}>
                    <TextArea 
                        //label='Password'
                        placeholder='Password'
                        secureTextEntry={false}
                        value={this.props.username}
                        onChangeText={this._onPasswordChanged.bind(this)}
                    />
                </View>   
            </CardSection> 
            <CardSection>
                <View style={styles.textInput}>
                    <TextArea 
                        //label='Password'
                        placeholder='Confirm Password'
                        secureTextEntry={false}
                        value={this.props.username}
                        onChangeText={this._onConfirmPasswordChanged.bind(this)}
                    />
                </View>   
            </CardSection> 
            <CardSection>
                <View style={styles.checkboxInput}>
                    <CheckBox
                        title='Anonymous'
                        checked={this.state.checked}
                        onPress={()=>this._onCheck()}
                    />
                </View>   
            </CardSection>  
            <CardSection>
                <View style={styles.buttonStyle}>
                    <Button onPress={()=>this._onRegistration()}>
                            Register
                    </Button>
                </View>
            </CardSection>    
            <CardSection>
            <View style={styles.buttonStyle}>
                <Button onPress={()=>this._onCancel()}>
                        Cancel
                </Button>
            </View>
            </CardSection>  
        </View>  
        );
    }
    _renderRegistration(){
       return(
        <View>
            <CardSection>
                <View style={styles.textInput}>
                    <TextArea 
                        //label='Username'
                        placeholder='Fullname'
                        secureTextEntry={false}
                        value={this.props.fullname}
                        onChangeText={this._onFullnameChanged.bind(this)}
                    />
                </View>                
            </CardSection>

            <CardSection>
                <View style={styles.textInput}>
                    <TextArea 
                        //abel='Email Address'
                        placeholder='Email'
                        secureTextEntry={false}
                        value={this.props.username}
                        onChangeText={this._onEmailChanged.bind(this)}
                    />
                </View>   
            </CardSection>

            <CardSection>
                <View style={styles.textInput}>
                    <TextArea 
                        //label='Password'
                        placeholder='Nickname'
                        secureTextEntry={false}
                        value={this.props.username}
                        onChangeText={this._onEmailChanged.bind(this)}
                    />
                </View>   
            </CardSection>

            <CardSection>
                <View style={styles.textInput}>
                    <TextArea 
                        //label='Confirm Password'
                        placeholder='Username'
                        secureTextEntry={false}
                        value={this.props.username}
                        onChangeText={this._onEmailChanged.bind(this)}
                    />
                </View>   
            </CardSection>   

            <CardSection>
                <View style={styles.textInput}>
                    <TextArea 
                        //label='Confirm Password'
                        placeholder='Password'
                        secureTextEntry={false}
                        value={this.props.username}
                        onChangeText={this._onEmailChanged.bind(this)}
                    />
                </View>   
            </CardSection> 

            <CardSection>
                <View style={styles.textInput}>
                    <TextArea 
                        //label='Confirm Password'
                        placeholder='Confirm Password'
                        secureTextEntry={false}
                        value={this.props.username}
                        onChangeText={this._onEmailChanged.bind(this)}
                    />
                </View>   
            </CardSection>  

            <CardSection>
                <View style={styles.checkboxInput}>
                    <CheckBox
                        title='Anonymous'
                        checked={this.state.checked}
                        onPress={()=>this._onCheck()}
                    />
                </View>   
            </CardSection>   

            <CardSection>
                <View style={styles.buttonStyle}>
                    <Button onPress={()=>this._onRegistration()}>
                            Register
                    </Button>
                </View>
            </CardSection>   

            <CardSection>
                <View style={styles.buttonStyle}>
                    <Button onPress={()=>this._onCancel()}>
                            Cancel
                    </Button>
                </View>
            </CardSection>                                                

        </View>

       );
    }
    render(){
        const data=[]
        let Anonymous=this.state.checked

        return (
            <View style={styles.container}>
                {   Anonymous ?      
                        this._renderAnonymous()
                    :
                        this._renderRegistration()
                }

            </View>

        )
    }

}

/*function mapStateToProps(state){
    console.log('loooooogggs',state);
    return{
        registration:state
    }
}*/

function mapStateToProps(state){
    //console.log('logstates234',state.datareducer);
    console.log('loooooogggs',state);
    return{
        login:state.security,
        //profile:state.datareducer.profile,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            registration:bindActionCreators(registrationActon,dispatch)
        }
    }
}

export default connect(
    mapDispatchToProps,
    mapStateToProps
)(Registration)