import React, { Component, PureComponent } from 'react';
import { Text,View,FlatList,Platform,Image,ScrollView,Alert,Modal,TextInput } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as registrationActon from '../datas/registration/actions';
import { CardSection,Spinner,Messagebox } from '../../../components';
import styles from './styles';
import { Button,Textbox,TextArea } from '../../../components/common';
import { CheckBox } from 'react-native-elements'

export class Registration extends Component{

    /*static navigationOptions = {
        title: 'Login',
        headerStyle: {
          backgroundColor: 'white'
        },
        headerTitleStyle :{
            alignSelf: 'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign: 'center',
        },
        headerLeft: null,

    }*/


    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:[],
            page:1,
            seed:1,
            error:null,
            refreshing:false,
            showForm:false,
            messageBox:true,
        }
    };

    componentDidMount(){
        this._fetchData();
    }

    _fetchData=()=>{

    }

    _onRegistration=async()=>{

        this.setState({messageBox:true})
        var datas= new FormData();

        let Anonymous=this.state.checked;

        if(Anonymous){
            var payload={
                isanonymous:'1',
                nickname:this.props.registration.nickname,
                username:this.props.registration.username,
                password:this.props.registration.password, 
                confirmpassword: this.props.registration.confirmpassword
            }
        }else{
            var payload={
                isanonymous:'0',
                fullname:this.props.registration.fullname,
                email:this.props.registration.email,
                nickname:this.props.registration.nickname,
                username:this.props.registration.username,
                password:this.props.registration.password,
                confirmpassword:this.props.registration.confirmpassword
            }
        }
        
        datas.append('jsonstr',JSON.stringify(payload));
        //const payload={}
        console.log('payload',payload);
        await this.props.actions.registration.post(datas);

    }

    _onCancel(){
        
    }
    _onCheck(){
        this.setState({checked:!this.state.checked})
        this.props.actions.registration.reset()
    }

    _onFullnameChanged=(text)=>{
        this.props.actions.registration.regfullnamechanged(text);
    }

    _onEmailChanged=(text)=>{
        this.props.actions.registration.regemailchanged(text);
    }
    
    _onNicknameChanged=(text)=>{
 
        this.props.actions.registration.regNicknamechanged(text);

    }

    _onUsernameChanged=(text)=>{
        this.props.actions.registration.regUsernamechanged(text);
    }

    _onPasswordChanged=(text)=>{

        this.props.actions.registration.regPasswordchanged(text);

    }

    _onConfirmPasswordChanged=(text)=>{
        this.props.actions.registration.regConfirmPasswordchanged(text);
    }


    _onRenderButton=(status)=>{

        switch (status[0]){
            case 0:
                /*Alert.alert(
                    'Alert Title',
                    status[1],
                    [
                    {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                )  
                return(
                    <Button onPress={()=>this._onRegistration()}>
                            Register
                    </Button>
                );*/
                if(this.state.messageBox){      
                    return(
                        <View style={{flex:1}}>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.messageBox}
                                onRequestClose={() => {
                                    Alert.alert('Modal has been closed.');
                            }}>
                                 <Messagebox message={status[1]} onClose={this._closeMessagebox}/>
        
                            </Modal>
                        </View>
                    );               
                }
                    return(
                        <Button onPress={()=>this._onRegistration()}>
                             Register
                        </Button>
                    );                             
                break;
            case 1:
                return(
                    <Button onPress={()=>this._onRegistration()}>
                            Register
                    </Button>
                );    
            case 2:
                return <Spinner size='large' />     
            case 3:
                return(
                    <Button onPress={()=>this._onRegistration()}>
                        Register
                    </Button>
                );                       
        }


        if(status[0]==1 || status[0]==3){
            return(
                <Button onPress={()=>this._onLogin()}>
                    Login
                </Button>
            );
        }
        if(status[0]==0){
            alert
        }
        return <Spinner size='large' />
    }

    _closeMessagebox=()=>{
        this.setState({messageBox:false})
    }

    render(){
        let Anonymous=this.state.checked
        const status=this.props.registration.status;
        const data = this.props.registration.data;
        //console.log('status',this.props.registration);

        return (
            <ScrollView>
                {
                    Anonymous ?

                        <View style={styles.container}>      
                        <CardSection>
                            <View style={styles.textInput}>
                                <TextInput
                                    style={{height: 40, borderColor: 'gray', borderWidth: 1,width:'100%'}}
                                    //label='Password'
                                    placeholder='Nickname'
                                    secureTextEntry={false}
                                    value={this.props.registration.nickname}
                                    onChangeText={this._onNicknameChanged.bind(this)}
                                />
                            </View>   
                        </CardSection>  

                        <CardSection>
                            <View style={styles.textInput}>
                                <TextInput
                                    style={{height: 40, borderColor: 'gray', borderWidth: 1,width:'100%'}}
                                    //label='Password'
                                    placeholder='Username'
                                    secureTextEntry={false}
                                    value={this.props.registration.username}
                                    onChangeText={this._onUsernameChanged.bind(this)}
                                />
                            </View>   
                        </CardSection>  

                        <CardSection>
                            <View style={styles.textInput}>
                                <TextInput
                                    style={{height: 40, borderColor: 'gray', borderWidth: 1,width:'100%'}}
                                    //label='Password'
                                    placeholder='Password'
                                    secureTextEntry={true}
                                    value={this.props.registration.password}
                                    onChangeText={this._onPasswordChanged.bind(this)}
                                />
                            </View>   
                        </CardSection> 
                        <CardSection>
                            <View style={styles.textInput}>
                                <TextInput
                                    style={{height: 40, borderColor: 'gray', borderWidth: 1,width:'100%'}}
                                    //label='Password'
                                    placeholder='Confirm Password'
                                    secureTextEntry={true}
                                    value={this.props.registration.confirmpassword}
                                    onChangeText={this._onConfirmPasswordChanged.bind(this)}
                                />
                            </View>   
                        </CardSection> 
                        <CardSection>
                            <View style={styles.checkboxInput}>
                                <CheckBox
                                    title='Anonymous'
                                    checked={this.state.checked}
                                    onPress={()=>this._onCheck()}
                                />
                            </View>   
                        </CardSection>  
                        <CardSection>
                            <View style={styles.buttonStyle}>
                                {this._onRenderButton(status)}
                            </View>
                        </CardSection>    
                        <CardSection>
                            <View style={styles.buttonStyle}>
                                <Button onPress={()=>this._onCancel()}>
                                        Cancel
                                </Button>
                            </View>
                        </CardSection>  

                        </View> 
                    :
                        <View style={styles.container}>
                            <CardSection>
                                <View style={styles.textInput}>
                                    <TextInput
                                        style={{height: 40, borderColor: 'gray', borderWidth: 1,width:'100%'}}
                                        onChangeText={this._onFullnameChanged.bind(this)}
                                        value={this.props.registration.fullname}
                                        placeholder='Fullname'
                                    />                       
                                </View>                
                            </CardSection>

                            <CardSection>
                                <View style={styles.textInput}>
                                    <TextInput
                                        style={{height: 40, borderColor: 'gray', borderWidth: 1,width:'100%'}}
                                        onChangeText={this._onEmailChanged.bind(this)}
                                        value={this.props.registration.email}
                                        placeholder='Email'
                                    />                           
                                </View>   
                            </CardSection>

                            <CardSection>
                                <View style={styles.textInput}>
                                    <TextInput
                                        style={{height: 40, borderColor: 'gray', borderWidth: 1,width:'100%'}}
                                        //label='Password'
                                        placeholder='Nickname'
                                        secureTextEntry={false}
                                        value={this.props.registration.nickname}
                                        onChangeText={this._onNicknameChanged.bind(this)}
                                    />
                                </View>   
                            </CardSection>

                            <CardSection>
                                <View style={styles.textInput}>
                                    <TextInput
                                        style={{height: 40, borderColor: 'gray', borderWidth: 1,width:'100%'}}
                                        //label='Confirm Password'
                                        placeholder='Username'
                                        secureTextEntry={false}
                                        value={this.props.registration.username}
                                        onChangeText={this._onUsernameChanged.bind(this)}
                                    />
                                </View>   
                            </CardSection>   

                            <CardSection>
                                <View style={styles.textInput}>
                                    <TextInput
                                        style={{height: 40, borderColor: 'gray', borderWidth: 1,width:'100%'}}
                                        //label='Confirm Password'
                                        placeholder='Password'
                                        secureTextEntry={true}
                                        value={this.props.registration.password}
                                        onChangeText={this._onPasswordChanged.bind(this)}
                                    />
                                </View>   
                            </CardSection> 

                            <CardSection>
                                <View style={styles.textInput}>
                                    <TextInput
                                        style={{height: 40, borderColor: 'gray', borderWidth: 1,width:'100%'}}
                                        //label='Confirm Password'
                                        placeholder='Confirm Password'
                                        secureTextEntry={true}
                                        value={this.props.registration.confirmpassword}
                                        onChangeText={this._onConfirmPasswordChanged.bind(this)}
                                    />
                                </View>   
                            </CardSection>  

                            <CardSection>
                                <View style={styles.checkboxInput}>
                                    <CheckBox
                                        title='Anonymous'
                                        checked={this.state.checked}
                                        onPress={()=>this._onCheck()}
                                    />
                                </View>   
                            </CardSection>   

                            <CardSection>
                                <View style={styles.buttonStyle}>
                                    {this._onRenderButton(status)}
                                </View>
                            </CardSection>   

                            <CardSection>
                                <View style={styles.buttonStyle}>
                                    <Button onPress={()=>this._onCancel()}>
                                            Cancel
                                    </Button>
                                </View>
                            </CardSection>         

                    </View>   
                }
            </ScrollView>
        )
    }
}                   

function mapStateToProps(state){
    return{
        registration:state.setups.register,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            registration:bindActionCreators(registrationActon,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Registration)