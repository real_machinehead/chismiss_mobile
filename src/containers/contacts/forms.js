import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform,Modal,TouchableHighlight,SafeAreaView,TextInput } from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import chatstyles from './styles';
import {Messagebox,Spinner} from '../../components';
import {IconButton,TextArea,Textbox} from '../../components/common';
import ActionButton from 'react-native-action-button';


export default class FormContacts extends PureComponent{

    constructor(props){
        super(props);
        this.state={
            loading:false,
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            modalVisible: false,
            data:this.props.contactlist,
            contact:'',
        };
    }

    componentDidUpdate(prevProps){
        /*const contactstatus=this.props.contactstatus;

        if(contactstatus[0]==1){
            //console.log('update');
            this.setModalVisible(!this.state.modalVisible);
        }*/
    }
    setModalVisible(visible) {
        this.props.onClose();
    }
    
    _searchContacts=()=>{
        //console.log('searchcontact');
        this.props.contactsearch();
    }
    _onEmailChanged=(text)=>{
        this.setState({
            contact:text,
        });
    }

    _keyExtractor = (item, index) => item.Username;

    _contactListExtractor=(item)=>item.name;

    _insertcontacts=(text)=>{
        this.props.insertcontacts(text);
    }
    _renderItem=(item)=>{
        /*<TouchableHighlight style={styles.addbuttonStyle}
            onPress={this._searchContacts}>
            <Text>Search</Text>
        </TouchableHighlight>*/
        return(
            <View style={styles.flatlistListItemContainer}>
                <TouchableHighlight
                    onPress={()=>this._insertcontacts(item)}>
                    <Text style={styles.flatlistListItemTextStyle}>{item.Username}({item.Callsign})</Text>
                </TouchableHighlight>
            </View>
        );
    }
    _contactchanged=(text)=>{
        //console.log('looogggss',this.props);
        this.props.contactchanged(text)
        //this.props.contactchanged(text);
    }
    _onRenderContactList=(item)=>{
        //console.log('items',item);
        return(
            <View style={styles.flatlistListItemContainer}>
                <Text style={styles.flatlistListItemTextStyle}>{item.name}</Text>
            </View>
        );
    }
    _onRenderSearchFlatlist=(status,data)=>{
        switch (status[0]){
            case 0:
                if(this.state.messageBox){      
                    return(
                        <View style={{flex:1}}>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.messageBox}
                                onRequestClose={() => {
                                    Alert.alert('Modal has been closed.');
                            }}>
                                <Messagebox message={status[1]} onClose={this._closeMessagebox}/>
        
                            </Modal>
                        </View>
                    );               
                }
                    return(
                        <View style={styles.flatlistSearchForm}>
                            <FlatList
                                data={data}
                                extraData={this.state}
                                keyExtractor={this._keyExtractor}
                                renderItem={({item}) => <Text>{item.name}</Text>}
                            />
                        </View>
                    );
                break;
            case 1:
                return(
                    <View style={styles.flatlistSearchForm}>
                        <FlatList
                            data={data}
                            extraData={this.state}
                            keyExtractor={this._keyExtractor}
                            renderItem={({item}) => this._renderItem(item)}
                        />
                    </View>
                );    
            case 2:
                return (
                    <View style={styles.flatlistSearchForm}>
                        <Spinner size='large' />    
                    </View>                    
                ); 
            case 3:
                return null;                       
        }   
    }

    _onSendContacts=async()=>{
        await this.props.sendcontacts();
        this.setModalVisible(!this.state.modalVisible);
    }

    _onrenderaddcontact=(status,contactlistdata)=>{
        console.log('statuss',status)
        switch (status[0]){
            case 0:
                if(this.state.messageBox){      
                    return(
                        <View style={{flex:1}}>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.messageBox}
                                onRequestClose={() => {
                                    Alert.alert('Modal has been closed.');
                            }}>
                                <Messagebox message={status[1]} onClose={this._closeMessagebox}/>
        
                            </Modal>
                        </View>
                    );               
                }
                    return(
                        <View style={styles.sendButtonStyleContainer}>
                            <TouchableHighlight style={styles.sendButtonStyle}
                                    onPress={() => {
                                       this._onSendContacts(contactlistdata);
                                    }}>
                                <Text>Send</Text>
                            </TouchableHighlight>
                        </View>
                    );
                break;
            case 1:
                return(
                    <View style={styles.sendButtonStyleContainer}>
                        <TouchableHighlight style={styles.sendButtonStyle}
                                onPress={() => {
                                this._onSendContacts(contactlistdata);
                                }}>
                            <Text>Send</Text>
                        </TouchableHighlight>
                    </View>
                );    
            case 2:
                return (

                    <View style={styles.sendButtonStyleContainer}>
                        <Spinner size='large' />   
                    </View>         
                ); 
            case 3:
                return(
                    <View style={styles.sendButtonStyleContainer}>
                        <TouchableHighlight style={styles.sendButtonStyle}
                                onPress={() => {
                                this._onSendContacts(contactlistdata);
                                }}>
                            <Text>Send</Text>
                        </TouchableHighlight>
                    </View>   
                );               
        } 
    }
    render(){
        const data=this.props.searchdata.Contacts
        const status=this.props.status;
        const contactstatus=this.props.contactstatus;
        const contactlistdata=this.props.contactlist;
        //console.log('logssss',contactlistdata);
        //console.log('formstatus',this.props.searchdata.Contacts);
        return(

                <View style={{flex:1,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                    <View style={{flexDirection:'column',justifyContent:'center',alignItems:'flex-start',width:'90%',height:Platform.OS==='android'? '90%':'70%',backgroundColor:'red'}}>
                                
                        <View style={styles.textContainerStyle}>
                            <View style={styles.textInput}>
                                <TextInput
                                        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                                        onChangeText={this._contactchanged.bind(this)}
                                        //value={}
                                        placeholder='Contact Name'
                                />
                            </View>
                            <View style={styles.addButtonStyleContainer}>
                                <TouchableHighlight style={styles.addbuttonStyle}
                                    onPress={this._searchContacts}>
                                    <Text>Search</Text>
                                </TouchableHighlight>
                            </View>
                        </View>      
                        {this._onRenderSearchFlatlist(status,data)}                           

                        <View style={styles.flatlistForm}>
                            <FlatList
                                data={contactlistdata}
                                extraData={contactlistdata}
                                keyExtractor={this._contactListExtractor}
                                renderItem={({item}) => this._onRenderContactList(item)}
                            />
                        </View>
                        {this._onrenderaddcontact(contactstatus,contactlistdata)}
                    </View>
                </View>
        )
    }
}