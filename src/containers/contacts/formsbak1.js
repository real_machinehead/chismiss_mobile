import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform,Modal,TouchableHighlight,SafeAreaView,TextInput } from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import chatstyles from './styles';
import {GeneralContainers,GeneralHeaders,GeneralContents,CardSection} from '../../components';
import {IconButton,TextArea,Textbox} from '../../components/common';
import ActionButton from 'react-native-action-button';


export default class FormContacts extends PureComponent{

    constructor(props){
        super(props);
        this.state={
            loading:false,
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            modalVisible: false,
            data:[],
            email:'',
        };
    }

    setModalVisible(visible) {
        this.props.onClose();
    }
    
    _insertcontacts=(datas)=>{
        dataobject={name:datas}

        this.state.data.push(dataobject);
        console.log(this.state.data);

        var found = Object.keys(this.state.data).filter(function(key) {
            return this.state.data[key] === 'test1';
          });
          if (found.length) {
             alert('exists');
          }
        //check if existed
        /*const isExisted=this.state.data.name.includes(datas);
        console.log(this.state.data);
        if(datas=![] && datas==!''){
            if(isExisted){
                console.log('existed');
            }else{
                this.state.data.push(datas);
            }
        }*/

    }
    _onEmailChanged=(text)=>{
        this.setState({
            email:text,
        });
    }
    _keyExtractor = (item, index) => item.chatId;


    render(){
        return(

                <View style={{flex:1,flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                    <View style={{flexDirection:'column',justifyContent:'center',alignItems:'flex-start',width:'90%',height:Platform.OS==='android'? '90%':'70%',backgroundColor:'red'}}>
                                
                        <View style={styles.textContainerStyle}>
                            <View style={styles.textInput}>
                                <TextInput
                                        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
                                        onChangeText={(text) => this.setState({text})}
                                        value={this.state.text}
                                    />
                            </View>
                            <View style={styles.addButtonStyleContainer}>
                                <TouchableHighlight style={styles.addbuttonStyle}
                                    onPress={() => {
                                        this._insertcontacts(this.state.email);
                                    }}>
                                    <Text>Add</Text>
                                </TouchableHighlight>
                            </View>
                        </View>                                 
                        <View style={styles.sendButtonStyleContainer}>
                            <TouchableHighlight style={styles.sendButtonStyle}
                                    onPress={() => {
                                        this.setModalVisible(!this.state.modalVisible);
                                    }}>
                                <Text>Send</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
        )
    }
}