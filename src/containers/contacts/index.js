import React, { Component} from 'react'
import {Text, View, FlatList,Modal,SafeAreaView,Alert,InteractionManager} from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from '../styles';
import Icon from 'react-native-vector-icons/Ionicons';
import contactStyle from './styles';
import {GeneralContainers,GeneralHeaders,GeneralContents,Messagebox,Spinner} from '../../components';
import ActionButton from 'react-native-action-button';
import ContactForm from './forms';
import ChannelForm from '../channels/forms';
import * as routesActions from '../datas/routes/actions';
import * as contactActions from '../datas/contacts/actions';
import websockets from '../../globals/websockets';
import { NavigationActions } from 'react-navigation';
//import { threadId } from 'worker_threads';
//import {Messagebox,Spinner} from '../../components';


 class Contacts extends Component{

    constructor(props){
        super(props);
        this.state={
            loading:false,
            data:{
                flagno:1,
                message:'success',
                data:[
                    {
                        firstName:'Kpop',
                        lastName:'Fans',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    },
                    {
                        firstName:'Girls',
                        lastName:'Generation',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    },
                    {
                        firstName:'Black',
                        lastName:'Pink',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    }
                ]
            },
            userOnline:[],
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            contactForm:false,
            channelForm:false,
            chatForm:false,
            contactlist:[],
            messageBox:true,
            isRefresh:false,
            
        };

        //this.ws= new WebSocket('wss://chismischannel.live:9999/');
        //this.socket=io.connect('https://chismischannel.live:8888',{transports: ['websocket']});
        /*this.ws.onopen = (e) => { this._onHandleOpenWs(e); };
        this.ws.onmessage = (data) => { this._onHandleMessageWs(data); };
        this.ws.onerror = (data) => { this._onHandleError(data); };
        this.ws.onclose = (data) => { this._onHandleClose(data); };*/
    }
    
    /*static navigationOptions ={
        title: 'CALLS',
        headerStyle:{
            backgroundColor: 'white'
        },
        headerTitleStyle:{
            alignSelf:'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign:'center'
        }
    }*/


    componentDidMount(){
        this._fetchData();
    }


    _fetchData=()=>{
        //console.log('contacts',this.props.contacts.usercontactlist);
        //this.setState({userOnline:this.props.contacts.usercontactlist})
        let payload={routes:'contact'}
        this.props.actions.routes.onupdateroutes(payload)
        InteractionManager.runAfterInteractions(() => {

            this._onRefreshdata()
        });
        
    }


    _onHandleOpenWs=(events)=>{

        /*const userId=this.props.profile.data.Users[0].UserID;
        console.log('Open ws',events);
          this.ws.send(JSON.stringify({
            'type': 'login',
            'content': {
                'userId': userId,
            }
        }));

        setTimeout(() => {
            this.ws.send(JSON.stringify({
                'type': 'view_clients'
            }));
        }, 1000);*/

    }


    _onHandleMessageWs=(event)=>{


        
        const reader = new FileReader();

        console.log('eventtts',event.data);
    }

    _onHandleError=(data)=>{
        console.log('error',data);
    }

    _onHandleClose=(event)=>{


            console.log('Socket is closed. Reconnect will be attempted in 1 second.', event);
            setTimeout(() => {
                var url='wss://chismischannel.live:9999/';
                const ws= new WebSocket(url);
                console.log('websocket',ws);
                this.ws.onopen = (e) => { this._onHandleOpenWs(e); };
                this.ws.onmessage = (data) => { this._onHandleMessageWs(data); };
                this.ws.onerror = (data) => { this._onHandleError(data); };
                this.ws.onclose = (data) => { this._onHandleClose(data); };
            }, 1000)
    }


    

    _onRecieveMessage=(message,userId)=>{
        console.log('received messages',message)
        let payload={message:message,userId:userId}
        this.props.actions.chats.onreceivemessage(payload)
        //const {channelmessage}=this.props.chats;
        this._onClearChatmessage();
        //this._onRenderSectionList(channelmessage,null);
    }
    
    
    _keyExtractor = (item, index) => item.Callsign+index;

    _onChat=(item)=>{
        //console.log('items',item)
        //this.ws.close

        /*this.ws.send()*/
        //var websocket=new websockets();
        //var ws=websocket.ws;

        /*ws.send(JSON.stringify({
            'type': 'read_message',
            'content': {
                'clientid': this.props.userid,
                'userid': item.UserID
            },
        }));*/


        console.log('items',item);
        //this.props.navigation.state.params.refresh()
        this.props.navigation.navigate('Chats',{
            data:item,
        })

    }

    _addContact=()=>{
        this.setState({showForm:true,contactForm:true})
        this.props.actions.contacts.contactlistreset('');
    }

    _addChannels=()=>{
        this.setState({showForm:true,channelForm:true})
    }

    _closeContactForm=()=>{
        this.setState({showForm:false,contactForm:false,chatForm:false})
    }

    _closeChannelForm=()=>{
        this.setState({showForm:false,channelForm:false})
    }

    _contactchanged=(text)=>{
        this.props.actions.contacts.contactchanged(text); 
    }

    _onContactSearch=async()=>{
        var payload={
            searchvalue:this.props.contacts.search,
            header:{token:this.props.acesstoken}
        }
        
        await this.props.actions.contacts.search(payload);
        //console.log('payload',payload);
        //console.log('accesstoken',this.props.acesstoken);
    }

    _insertcontacts=(datas)=>{


        
        dataobject={name:datas.Username,userid:datas.UserID}
        console.log('loggsss',dataobject);

        /*var dataobj=this.state.contactlist
        var filterobj = dataobj.filter(function(e){
            return e.name==datas;
        });

        if(filterobj.length){
            console.log('Existed',this.props.contactlist);
            //console.log('data List',this.state.data);
        }else{
            dataobj.push(dataobject);
            this.setState({contactlist:dataobj})
            //console.log('data List',this.state.data);
        }*/


        if(this.props.contacts.contactlist){

            var filterobj = this.props.contacts.contactlist.filter(function(e){
                return e.name==datas.Username;
            });

            if(filterobj.length){
                console.log('Existed',this.props.contactlist);
                //console.log('data List',this.state.data);
            }else{
                this.props.actions.contacts.contactlistupdate(dataobject);
                //console.log('data List',this.state.data);
            }
            //this.props.actions.contacts.search(payload);

         
        }else{
            //console.log('contacts',this.props.contacts.contactlist);
            var arrdata=[];
            arrdata.push(dataobject); 
            console.log('loggs',arrdata); 
            this.props.actions.contacts.contactlist(dataobject);
        }



    }

    _sendcontacts=async()=>{
        
        /*var payload={
            searchvalue:this.props.contacts.search,
            header:{token:this.props.acesstoken}
        }
        
        await this.props.actions.contacts.search(payload);*/
  
        //let userid=101;
        //console.log('logggsss',this.props.contacts.contactlist);
        var userid=this.props.contacts.contactlist.map(function(e){
            return e.userid
        });

        console.log('length',userid.length);
        for(x=0;x<userid.length;x++){
            //this.props.actions.contacts.contactstatus([2,'LOADING']);
            console.log('loogggss',userid[x]);
            let data= new FormData();
            data.append('contactuserid', userid[x]);
            console.log('data',data)
            await this.props.actions.contacts.addcontacts(data,{token:this.props.acesstoken});
        }
        this.setState({messageBox:true});
        /*console.log('status',this.props.contacts.contactstatus)
        if(this.props.contacts.contactstatus[0]==1){
            this.props.actions.contacts.contactstatus([1,'SUCCESS']);
        }*/
        //this.props.actions.contacts.contactlistreset('');
        
    }


    _renderForm=()=>{
        if(this.state.contactForm){
            return(
                <SafeAreaView>
                    <View style={{flex:1}}>
                        <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.contactForm}
                        onRequestClose={() => {
                            //Alert.alert('Modal has been closed.');
                            this._closeContactForm()
                        }}>
                            <ContactForm   
                                onClose={this._closeContactForm} 
                                contactchanged={this._contactchanged} 
                                contactsearch={this._onContactSearch}
                                status={this.props.contacts.status}
                                contactstatus={this.props.contacts.contactstatus}
                                searchdata={this.props.contacts.data}
                                contactlist={this.props.contacts.contactlist}
                                insertcontacts={this._insertcontacts}
                                sendcontacts={this._sendcontacts}
                            />
                            
                        </Modal>
                    </View>
                </SafeAreaView>
            );
        }
        return(
            <SafeAreaView>
                <View style={{flex:1}}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.channelForm}
                        onRequestClose={() => {
                            Alert.alert('Modal has been closed.');
                        }}>
                        <ChannelForm  onClose={this._closeChannelForm}/>

                    </Modal>
                </View>
            </SafeAreaView>
        );
    }
    _closeMessagebox=async()=>{
        await this.props.actions.contacts.contactstatus([3,'']);
        this.setState({messageBox:false});
    }


    _onrendercontactlist=(status,data)=>{
        //console.log('statusss',status,this.state.messageBox)
        switch(status[0]){
            case 0:  
                    return(
                        <View style={{flex:1}}>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.messageBox}
                                onRequestClose={() => {
                                    Alert.alert('Modal has been closed.');
                            }}>
                                <Messagebox message={status[1]} onClose={this._closeMessagebox}/>
        
                            </Modal>
                        </View>
                    );               
            break;
            case 1:
                    return(
                        <View style={{flex:1}}>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.messageBox}
                                onRequestClose={() => {
                                    Alert.alert('Modal has been closed.');
                            }}>
                                <Messagebox message={status[1]} onClose={this._closeMessagebox}/>

                            </Modal>
                        </View>
                    ); 
            break;
            case 2:
                return(
                    <View style={styles.sendButtonStyleContainer}>
                        <Spinner size='large' />   
                    </View>    
                );
            break;
            case 3:
                if(data){
                    console.log('dtasss',data)
                    return(
                        <FlatList
                            data={data}
                            keyExtractor={this._keyExtractor}
                            extraData={this.state.refreshing}
                            onRefresh={()=>this._onRefreshdata()}
                            refreshing={this.state.isRefresh}
                            //renderItem={this._onRenderItem}
                            renderItem={({ item }) => (
                                <ListItem
                                    //roundAvatar
                                    //title={`${item.firstName} ${(item.lastName)}`}
                                    model={item}
                                    title={
                                        <View style={{flexDirection:'row'}}>
                                            {
                                                /*item.Status='online' ?
                                                    <View style={contactStyle.onlineroundAvatarStyle} />
                                                :
                                                    <View style={contactStyle.roundAvatarStyle} />*/

                                                this._onOnlineStatus(item)
                                            }
                                            
                                            <View>
                                                <Text>{item.Fullname}({item.Callsign})</Text>
                                            </View>
                                            {
                                                item.Newmessage ?
                                                <View>
                                                    <Text>New Message</Text>
                                                </View>
                                                :
                                                 null
                                            }   

                                        </View>
                                    }
                                    subtitle={item.date}
                                    avatar={<Avatar
                                            small
                                            rounded
                                            //overlayContainerStyle={{backgroundColor: item.IsVisible ? 'green':'gray'}}
                                            title={`${item.Fullname ? item.Fullname.charAt(0):'P'}${item.Callsign.charAt(0)}`}
                                            //onPress={(item) => this._onChat(item)}
                                            activeOpacity={0.7}
                                        />}
                                    onPress={()=>this._onChat(item)}
                                />
                                        
                            )}
            
                        />  
                    );
                }else{
                    return null;
                }           
            break;
        }
      
    }


    _onOnlineStatus=(item)=>{
      
        const data=item;
        console.log('stats',data.Status);
        if(item.Status=='online'){
            return <View style={contactStyle.onlineroundAvatarStyle} />
        }else{
            return <View style={contactStyle.roundAvatarStyle} />
        }
    }

    _onRenderItem=({item})=>{
        
        const {Callsign,Status}=item;
        console.log('renderItem',Callsign+' '+Status);
        return(
            <View>
                <Text>{item.Callsign}</Text>
                <Text>Status{item.Status}</Text>
            </View>
        )
    }

    _onRefreshdata=async()=>{
        this.setState({isRefresh:true})
            const payload={
                accesstoken:this.props.acesstoken,
                userid:this.props.userid,
                header:{'Content-Type':'application/json','Authorization':'Bearer ' + this.props.acesstoken}
            }
        await this.props.actions.contacts.fetchcontactlist(payload,{Token:this.props.acesstoken});
        await this.setState({isRefresh:false})
    }
    render(){
        const data=this.state.data.data;
        const status= this.props.contacts.status;
        const contactstatus = this.props.contacts.contactstatus;
        const usercontactlist=this.props.contacts.usercontactlist;
        //const usercontactlist=this.state.userOnline;
        //const contactstatus=[];
        //console.log('send contacts',this.props.contacts.contactlist);
        return(
            <GeneralContainers>

                <GeneralHeaders>

                    <View style={styles.headerTitle}>
                        <Text>CONTACTS</Text>
                    </View>

                </GeneralHeaders>

                <GeneralContents>
                    {
                        this.state.showForm ?
                            this._renderForm()
                        :
                        
                            <View style={contactStyle.flatlist}>
                                {   
                                    usercontactlist ?
                                        this._onrendercontactlist(contactstatus,usercontactlist)
                                    :
                                        null
                                }
                                <ActionButton buttonColor="rgba(231,76,60,1)">
                                    <ActionButton.Item buttonColor='#9b59b6' title="Add Contacts" onPress={() => this._addContact()}>
                                        <Icon name="md-create" style={styles.actionButtonIcon} />
                                    </ActionButton.Item>
                                    <ActionButton.Item buttonColor='#3498db' title="Invite to Channel" onPress={() => this._addChannels()}>
                                        <Icon name="md-notifications-off" style={styles.actionButtonIcon} />
                                    </ActionButton.Item>
                                </ActionButton>   
                    
                            </View>
                    }
                </GeneralContents>

            </GeneralContainers>
        );
    }
}


function mapStateToProps(state){
    console.log('logstates23411111',state);
    return{
        userid:state.security.login.userid,
        acesstoken:state.security.login.accesstoken,
        profile:state.datareducer.profile,
        contacts:state.datareducer.contacts,
        routes:state.datareducer.routes
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            contacts:bindActionCreators(contactActions,dispatch),
            routes:bindActionCreators(routesActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Contacts)