import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform,Modal } from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from '../styles';
import callstyles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {IconButton} from '../../components/common';
import {GeneralContainers,GeneralHeaders,GeneralContents,Messagebox,Spinner} from '../../components';
import ActionButton from 'react-native-action-button';
import ContactForm from '../contacts/forms';
import ChannelForm from '../channels/forms';
import * as contactActions from '../datas/contacts/actions';

class Calls extends Component{

    constructor(props){
        super(props);
        this.state={
            loading:false,
            data:{
                flagno:1,
                message:'success',
                data:[
                    {
                        firstName:'Kpop',
                        lastName:'Fans',
                        date:'01/01/2018'
                    },
                    {
                        firstName:'Girls',
                        lastName:'Generation',
                        date:'01/01/2018'
                    },
                    {
                        firstName:'Black',
                        lastName:'Pink',
                        date:'01/01/2018'
                    },
                    {
                        firstName:'Momo',
                        lastName:'Land',
                        date:'01/01/2018'
                    },
                    {
                        firstName:'Big',
                        lastName:'Bang',
                        date:'01/01/2018'
                    }                   
                ]
            },
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            channelForm:false,
            contactForm:false,
            messageBox:true,
        };;
    }
    
    /*static navigationOptions ={
        title: 'CALLS',
        headerStyle:{
            backgroundColor: 'white'
        },
        headerTitleStyle:{
            alignSelf:'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign:'center'
        }
    }*/


    componentDidMount(){
        console.log('component mount');
    }


    _fetchData=()=>{

    }

    _keyExtractor = (item, index) => item.firstName;
 
    _addContact=()=>{
        this.props.actions.contacts.contactlistreset('');
        this.setState({showForm:true})
    }

    _addchannel=()=>{
        this.setState({showForm:true,channelForm:true})
    }

    _closeContactForm=()=>{
        this.setState({showForm:false,contactForm:false})
    }

    _closeChannelForm=()=>{
        this.setState({showForm:false,channelForm:false})
    }

    _contactchanged=(text)=>{
        this.props.actions.contacts.contactchanged(text); 
    }

    _onContactSearch=async()=>{
        var payload={
            searchvalue:this.props.contacts.search,
            header:{token:this.props.acesstoken}
        }
        
        await this.props.actions.contacts.search(payload);
        //console.log('payload',payload);
        //console.log('accesstoken',this.props.acesstoken);
    }
 
    _insertcontacts=(datas)=>{


        
        dataobject={name:datas.Username,userid:datas.UserID}
        console.log('loggsss',dataobject);

        /*var dataobj=this.state.contactlist
        var filterobj = dataobj.filter(function(e){
            return e.name==datas;
        });

        if(filterobj.length){
            console.log('Existed',this.props.contactlist);
            //console.log('data List',this.state.data);
        }else{
            dataobj.push(dataobject);
            this.setState({contactlist:dataobj})
            //console.log('data List',this.state.data);
        }*/


        if(this.props.contacts.contactlist){

            var filterobj = this.props.contacts.contactlist.filter(function(e){
                return e.name==datas.Username;
            });

            if(filterobj.length){
                console.log('Existed',this.props.contactlist);
                //console.log('data List',this.state.data);
            }else{
                this.props.actions.contacts.contactlistupdate(dataobject);
                //console.log('data List',this.state.data);
            }
            //this.props.actions.contacts.search(payload);

         
        }else{
            //console.log('contacts',this.props.contacts.contactlist);
            var arrdata=[];
            arrdata.push(dataobject); 
            console.log('loggs',arrdata); 
            this.props.actions.contacts.contactlist(dataobject);
        }



    }
    
    _sendcontacts=async()=>{
        
        /*var payload={
            searchvalue:this.props.contacts.search,
            header:{token:this.props.acesstoken}
        }
        
        await this.props.actions.contacts.search(payload);*/
  
        //let userid=101;
        //console.log('logggsss',this.props.contacts.contactlist);
        var userid=this.props.contacts.contactlist.map(function(e){
            return e.userid
        });

        console.log('length',userid.length);
        for(x=0;x<userid.length;x++){
            //this.props.actions.contacts.contactstatus([2,'LOADING']);
            console.log('loogggss',userid[x]);
            let data= new FormData();
            data.append('contactuserid', userid[x]);
            console.log('data',data)
            await this.props.actions.contacts.addcontacts(data,{token:this.props.acesstoken});
        }
        this.setState({messageBox:true});
        /*console.log('status',this.props.contacts.contactstatus)
        if(this.props.contacts.contactstatus[0]==1){
            this.props.actions.contacts.contactstatus([1,'SUCCESS']);
        }*/
        //this.props.actions.contacts.contactlistreset('');
        
    }

    _renderform=()=>{
        if(this.state.contactForm){
            return(
                <View style={{flex:1}}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            Alert.alert('Modal has been closed.');
                        }}>
                            <ContactForm   
                                onClose={this._closeContactForm} 
                                contactchanged={this._contactchanged} 
                                contactsearch={this._onContactSearch}
                                status={this.props.contacts.status}
                                contactstatus={this.props.contacts.contactstatus}
                                searchdata={this.props.contacts.data}
                                contactlist={this.props.contacts.contactlist}
                                insertcontacts={this._insertcontacts}
                                sendcontacts={this._sendcontacts}
                            />

                    </Modal>
                </View> 
            );
        }
        return(
            <View style={{flex:1}}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                        <ChannelForm onClose={this._closeChannelForm}/>

                </Modal>
            </View> 
        );         
    }

    _closeMessagebox=async()=>{
        await this.props.actions.contacts.contactstatus([3,'']);
        this.setState({messageBox:false})
   }


    _onrendercontactlist=(status,data)=>{
        console.log('statusss',status,this.state.messageBox)
        switch(status[0]){
            case 0:  
                    return(
                        <View style={{flex:1}}>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.messageBox}
                                onRequestClose={() => {
                                    Alert.alert('Modal has been closed.');
                            }}>
                                <Messagebox message={status[1]} onClose={this._closeMessagebox}/>
        
                            </Modal>
                        </View>
                    );               
            break;
            case 1:
                    return(
                        <View style={{flex:1}}>
                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={this.state.messageBox}
                                onRequestClose={() => {
                                    Alert.alert('Modal has been closed.');
                            }}>
                                <Messagebox message={status[1]} onClose={this._closeMessagebox}/>

                            </Modal>
                        </View>
                    ); 
            break;
            case 2:
                return(
                    <View style={styles.sendButtonStyleContainer}>
                        <Spinner size='large' />   
                    </View>    
                );
            break;
            case 3:
                return(
                    <FlatList
                        data={data}
                        keyExtractor={this._keyExtractor}
                        renderItem={({ item }) => (
                            <ListItem
                                title={`${item.firstName} ${item.lastName}`}
                                subtitle={item.message}
                                avatar={<Avatar
                                    small
                                    rounded
                                    title={`${item.lastName.charAt(0)}${item.firstName.charAt(0)}`}
                                    onPress={() => this._onChat()}
                                    activeOpacity={0.7}
                                />}
                                onPress={() => this._onChat()}      
                            />
                        )}
                    />  
                );                
            break;
        }

    }
    
    
    render(){
        const data=this.state.data.data;
        const contactstatus = this.props.contacts.contactstatus;
        console.log('call status',this.props.contacts.status);
        return(
            <GeneralContainers>

                <GeneralHeaders>
                    <View style={styles.headerTitle}>
                        <Text>CALLS</Text>
                    </View>
                </GeneralHeaders>
                {
                    this.state.showForm ?
                        this._renderform()
                    :                   
                    <GeneralContents>
                        
                        <View style={{height:'30%'}}>

                            <View>
                                <Text>Recent Calls</Text>
                            </View>  

                            <View style={callstyles.recentFlatlist}>
                                <FlatList
                                        data={data}
                                        keyExtractor={this._keyExtractor}
                                        renderItem={({ item }) => (
                                            <ListItem
                                                title={`${item.firstName} ${item.lastName}`}
                                                subtitle={item.message}
                                                avatar={<Avatar
                                                    small
                                                    rounded
                                                    title={`${item.lastName.charAt(0)}${item.firstName.charAt(0)}`}
                                                    onPress={() => console.log("Works!")}
                                                    activeOpacity={0.7}
                                                />}
                                                rightIcon={
                                            
                                                    <View style={{flexDirection:'row'}}>
                                                        <View style={{paddingRight:10}}>
                                                            <IconButton>
                                                                <Icon name='ios-call' size={30}/>
                                                            </IconButton>

                                                        </View>
                                                        <View>
                                                            <IconButton>
                                                                <Icon name='md-videocam' size={30}/>
                                                            </IconButton>

                                                        </View> 
                                                    </View>                                          
                                                }    
                                            />
                                        )}
                                    />  
                            </View>  
                        </View>   
                        
                        <View>
                            <Text>People</Text>
                        </View>  
                        
                            <View style={callstyles.recentFlatlist}>  
                                <FlatList
                                        data={data}
                                        keyExtractor={this._keyExtractor}
                                        renderItem={({ item }) => (
                                            <ListItem
                                                title={`${item.firstName} ${item.lastName}`}
                                                subtitle={item.message}
                                                avatar={<Avatar
                                                    small
                                                    rounded
                                                    title={`${item.lastName.charAt(0)}${item.firstName.charAt(0)}`}
                                                    onPress={() => console.log("Works!")}
                                                    activeOpacity={0.7}
                                                />}
                                                rightIcon={
                                            
                                                    <View style={{flexDirection:'row'}}>
                                                        <View style={{paddingRight:10}}>
                                                            <IconButton>
                                                                <Icon name='ios-call' size={30}/>
                                                            </IconButton>

                                                        </View>
                                                        <View>
                                                            <IconButton>
                                                                <Icon name='md-videocam' size={30}/>
                                                            </IconButton>

                                                        </View> 
                                                    </View>                                          
                                                }                                          
            
                                            />
                                        )}
                                    /> 
                                    <ActionButton buttonColor="rgba(231,76,60,1)">
                                        <ActionButton.Item buttonColor='#9b59b6' title="Add Contacts" onPress={() => this._addContact()}>
                                            <Icon name="md-create" style={styles.actionButtonIcon} />
                                        </ActionButton.Item>
                                        <ActionButton.Item buttonColor='#3498db' title="Invite to Channel" onPress={() => this._addchannel()}>
                                            <Icon name="md-notifications-off" style={styles.actionButtonIcon} />
                                        </ActionButton.Item>
                                    </ActionButton> 
                                
                            </View>               
                    </GeneralContents>
                }
            </GeneralContainers>
        );
    }
}
function mapStateToProps(state){
    //console.log('logstates234',state.datareducer);
    return{
        acesstoken:state.security.login.accesstoken,
        //profile:state.datareducer.profile,
        contacts:state.datareducer.contacts,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            contacts:bindActionCreators(contactActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Calls)