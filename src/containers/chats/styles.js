export default{
    flatlist:{
        flex:1,
        flexDirection:'row',
        width:'100%',
        backgroundColor:'transparent',        
    },
    chatMessagesContainer:{
        width:'100%',
        height:'20%',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    txtAreaStyles:{
        width:'85%',
        height:'80%',
        backgroundColor:'blue'
    },
    textAreaContainer:{
        //width:'85%',
        //height:'80%',
        flexDirection:'row',
        width:'80%',
        height:50,
        backgroundColor:'tranparent',
        borderRadius: 10,
    },
    chatButtonContainer:{
        width:'30%',
        height:'80%',
        backgroundColor:'green',
    },
    talkBubblesender:{
        backgroundColor: 'transparent',
        alignItems:'flex-end',
        marginLeft:10,
        marginRight:10, 
        //height:80,
        marginTop:5,
    },

    talkBubblereceiver:{
        backgroundColor: 'transparent',
        alignItems:'flex-start',
        marginRight:10, 
        marginLeft:10,
        marginTop:5,
        //height:80, 
    },
    talkBubbleSquarereceiver: {
        //width: 150,
        //height: 80,
        backgroundColor: 'yellow',
        borderRadius: 10,
        borderWidth:2,
        //marginTop:5,
        
    },
    talkBubbleSquare: {
        //width: 150,
        //height: 80,
        backgroundColor: '#00FF7F',
        borderRadius: 10,
        alignItems:'flex-start',
        borderWidth:1,
        //marginTop:5,
    },
    talkBubbleTriangle: {
        position: 'absolute',
        left: -26,
        top: 26,
        width: 0,
        height: 0,
        borderTopColor: 'transparent',
        borderTopWidth: 13,
        borderRightWidth: 26,
        borderRightColor: 'red',
        borderBottomWidth: 13,
        borderBottomColor: 'transparent'
    },
    sectionFooterStyle:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    sectionFooterTextStyle:{
        fontFamily:'helvetica',
        fontSize:18,
    },
    attachmentsContainterStyle:{
        //flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width:'100%',
        
    },
    scrollView:{
        flexWrap: 'wrap',
        flexDirection: 'row',
        //width:'90%',
        backgroundColor:'green',
    },
    chatMessageFont:{
        fontSize:18,
        fontFamily:'helvetica',
        color:'black',
        marginLeft:5,
        marginRight:10,
    },
    chatHeaders:{
        justifyContent:'center',
        //alignItems:'center',
        flexDirection:'row',
        width:'100%',
        height:50,
        backgroundColor:'blue',           
    },
    generalContents:{
        flex:1,
        //justifyContent:'center',
        flexDirection:'column',    
        width:'100%'    
    },
    frwdmsgsectionheader:{
        flex:1,
        flexDirection:'row',   
    },
    frwdmsgitemcontainer:{
        //height:'10%',
        width:'100%',
        //flexDirection:'row',
        marginTop:10
    },
    frwdmsgroundAvatarStyle:{
        width: 50,
        height: 50,
        marginLeft:-5,
        borderRadius: 100/2,
        backgroundColor: 'red',
        marginLeft:10,      
        justifyContent:'center',
        alignItems:'center',
    },
    frwdmsgitemlineseparator:{
        height: 1,
        backgroundColor: 'grey',
        justifyContent:'center',
        alignItems:'center',
    },
    frwdmsgcontentStyle:{
        flexDirection:'row',
        width:'100%',
    },
    headerTitle:{
        justifyContent:'flex-start',
        flexDirection:'row',
        alignItems:'center',
        width:'100%',
    },
}