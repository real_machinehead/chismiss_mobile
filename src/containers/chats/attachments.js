import React, { Component, PureComponent} from 'react'
import { 
    Text, 
    View, 
    FlatList,
    Platform,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    SectionList,
    CameraRoll,
    Modal,
    ScrollView,
    Image,
    Dimensions
} from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import {GeneralContainers,GeneralHeaders,GeneralContents} from '../../components';
import { bindActionCreators } from 'redux';
import * as chatActions from '../datas/chats/actions';
import styles from '../styles';
import chatstyles from './styles';
import {IconButton,TextArea,Button,Spinner} from '../../components/common';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import RNFetchBlob from 'rn-fetch-blob';
import { decode } from 'punycode';

const { width } = Dimensions.get('window');
export class Attachements extends Component{

    /*static navigationOptions = {
        title: 'Login',
        headerStyle: {
          backgroundColor: 'white'
        },
        headerTitleStyle :{
            alignSelf: 'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign: 'center',
        },
        headerLeft: null,

    }*/


    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:{
                flagno:1,
                message:'success',
            },
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            chatMessage:'',
            Usercontact:'',
            buttonSendEnable:false,
            concatmsg:'',
            attachment:null,
            attachdata:null,

        };
    }

    componentDidMount(){
        this._fetchData();
        //setTimeout(() => console.log('Five Seconds'), 5000)
    }


    _fetchData=async()=>{

    }

    _keyExtractor = (item, index) => 'key'+index;

    _getNotification=()=>{
  
    }

    setIndex = (index) => {
        if (index === this.state.index) {
          index = null
        }
        this.setState({ index })
    }

    /*_getPictures=(image)=>{
        console.log('image location',image)
        RNFetchBlob.fs.readFile(image, 'base64')
        .then((data) => {
            const images={
                url: `image/jpg;base64,${data}`,
            }
           //remote.setState({attachdata:images})
           console.log('images',images);
           this._onsendPictures(image,images.url)
        // handle the data ..
        })
        //console.log('images',remote.state.attachdata);
        //this.props.onSendPictures(image)
    }*/

    _getPictures=(image)=>{
        console.log('image location',image)
        this.props.onSendPictures(image)
    }

    /*_onsendPictures=async(image,decodeImage)=>{
        //console.log('imagesss',decodeImage);
        this.props.onSendPictures(image,decodeImage)
    }*/
    render(){
        const data=this.state.data.data;
        const {userlist,channelid,channelmessage}=this.props.chats;
        const {chatMessage,}=this.state;
        const currentUserId=this.props.profile.data.Users[0].UserID;
        console.log('channelMessages',this.props.onClose);

        return(
                    <View style={chatstyles.attachmentsContainterStyle}> 

                        <TouchableOpacity
                            onPress={this.props.onClose}>
                            <Text> Close </Text>
                        </TouchableOpacity>  
                            {
                                this.props.attachment ?
                                    <ScrollView
                                        contentContainerStyle={styles.scrollView}>
                                        {
                                            this.props.attachment.map((p, i) => {
                                            return (
                                            <TouchableOpacity
                                                style={{opacity: i === this.state.index ? 0.5 : 1,marginTop:10}}
                                                key={i}
                                                underlayColor='transparent'
                                                onPress={() => {this._getPictures(p.node)}}
                                                >
                                                <Image
                                                    style={{
                                                    width: width/1.5,
                                                    height: width/3
                                                    }}
                                                    source={{uri: p.node.image.uri}}
                                                />
                                                </TouchableOpacity>
                                            )
                                            })
                                        }
                                    </ScrollView> 
                                :
                                    null
                            }         
                    </View>
        );
    }
}                   

function mapStateToProps(state){
    //console.log('logstates234',state.datareducer);
    return{
        acesstoken:state.security.login.accesstoken,
        profile:state.datareducer.profile,
        contacts:state.datareducer.contacts,
        chats:state.datareducer.chats,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            chats:bindActionCreators(chatActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Attachements)