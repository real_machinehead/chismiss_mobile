import React, { Component} from 'react'
import { 
    Text, 
    View, 
    TextInput,
    TouchableOpacity,
    SectionList,
    CameraRoll,
    Modal,
    PermissionsAndroid,
    Image,
    Dimensions,
    BackHandler, 
    TouchableHighlight
} from 'react-native';
import { connect } from 'react-redux';
import {GeneralContainers,Spinner,Messagebox} from '../../components';
import { bindActionCreators } from 'redux';
import * as chatActions from '../datas/chats/actions';
import styles from '../styles';
import chatstyles from './styles';
import Attachments from './attachments';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import * as routesActions from '../datas/routes/actions';
import websockets from '../../globals/websockets';
import Socketconnection from '../../globals/socketsioconnection';
import Forwardmessages from './frwdmessage';
import base64 from 'react-native-base64'

const { width } = Dimensions.get('window');

//import sectionListGetItemLayout from 'react-native-section-list-get-item-layout'

//url='https://chismischannel.live:8888'

const ITEM_HEIGHT = 100; 
export class Chat extends Component{

    /*static navigationOptions = {
        title: 'Login',
        headerStyle: {
          backgroundColor: 'white'
        },
        headerTitleStyle :{
            alignSelf: 'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign: 'center',
        },
        headerLeft: null,

    }*/


    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:{
                flagno:1,
                message:'success',
            },
            page:1,
            pagesize:0,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            chatMessage:'',
            Usercontact:'',
            buttonSendEnable:false,
            concatmsg:'',
            attachment:null,
            attachModal:false,
            isFocused:false,
            channelMessages:null,
            connectionId:null,
            isTyping:false,
            isTypingpayload:{},
            isDeleteMessage:false,
            isForwardMessages:false,
            isHeader:true,
            frwdmsgpayload:'',
        };
    }

    _isMounted=false;
    componentDidMount=async()=>{
        BackHandler.removeEventListener('hardwareBackPress', this._onBackPress);
        this._isMounted = true;

        const userid=this.props.userid;
       
        let dateposted=moment().format('YYYY-MM-DD,H:mm:ss');
        let dayposted=moment().format('l');
        let timeposted=moment().format('LT');

        const userdata=this.props.navigation.getParam('data');
        const clientId= userdata.UserID;

        var websocket=new websockets();
        var ws=websocket.ws;
        let socketconnection;
        
        const self=this;
        connection=new Socketconnection();
            
            //socketconnection= new Socketconnection.SocketIoConnection('https://chismischannel.live:8888',{transports: ['websocket']});
           
            this.socketconnection=connection.SocketIoConnection('https://chismischannel.live:8888',{transports: ['websocket']});
           
            this.socketconnection.on('connect',function(){
                self.socketconnection.emit('connectionReady', connection.GetSessionId());
                //self.setState({connectionId:connection.GetSessionId()})
                //self.sessionReady = true;
                //self.testReadiness();
            })
        
          
          
        await this.props.actions.chats.reset();  
          await this.socketconnection.on('message', function (message) {
                console.log('messagesssss',message);
                switch(message.type){
                    case 'chat':
                        console.log('chattttssss',message.payload)
                        let payload={
                            oldmessage:self.props.chats.channelmessage,
                            newmessages:message.payload,
                            DatePosted:dateposted,
                            TimePosted:timeposted,DayPosted:dayposted
                        }
                        self.props.actions.chats.onreceivemessage(payload);
                        self.props.actions.chats.onupdatetotalitems(self.props.totalitems)
                        if(!self.peer){
                            console.log('self peerr',self.peer);
                            self.peer={
                                id:message.from,
                                type:message.roomType,
                                enableDataChannels:null,
                                receiveMedia: {
                                    offerToReceiveAudio: 0,
                                    offerToReceiveVideo: 0
                                }                                       
                            }
                            //console.log('createdpeer',peer);
                            self.setState({connectionId:message.from})
                            self.socketconnection.emit('createdPeer', self.peer);                           
                        } 
                        //self.scrollToSection(self.state.channelMessages);  
                        //self.setState({pagesize:self.state.pagesize+1})
                        //self.setState(prevState=>({pagesize:prevState.pagesize+1}))
                        self.scrollToSection(self.props.totalitems); 
                    break;
                    
                    case 'typing':
                        self.setState({isTyping:true,isTypingpayload:message.payload})
                        if(!self.peer){
                            console.log('self peerr',self.peer);
                            self.peer={
                                id:message.from,
                                type:message.roomType,
                                enableDataChannels:null,
                                receiveMedia: {
                                    offerToReceiveAudio: 0,
                                    offerToReceiveVideo: 0
                                }                                       
                            }
                            //console.log('createdpeer',peer);
                            self.setState({connectionId:message.from})
                            self.socketconnection.emit('createdPeer', self.peer);   
                            self.scrollToSection(self.props.totalitems);                         
                        }                    
                    break;
                    case 'stoppedTyping':
                        self.setState({isTyping:false,isTypingpayload:{}})
                        self.scrollToSection(self.props.totalitems); 
                    break;
                    case 'offer':
                        if(!self.peer){
                            self.peer={
                                id:message.from,
                                type:message.roomType,
                                enableDataChannels:null,
                                receiveMedia: {
                                    offerToReceiveAudio: 0,
                                    offerToReceiveVideo: 0
                                }                                       
                            }
                            //console.log('createdpeer',peer);
                            self.setState({connectionId:message.from})
                            self.socketconnection.emit('createdPeer', self.peer);                           
                        }
                        //self.scrollToSection()
                    break;
                    default:

                }
            });

 
            await this._onCreateJoinRoom({userid:userid,clientid:clientId});  
            await this._fetchData();  
            //await  this.scrollToSection(this.props.chats.channelmessage)       
            //await this._sectionList();
        //console.log('clientid userid',clientId,userid);
        
        ws.send(JSON.stringify({
            'type':'read_message',
            'content':{
                'clientid':clientId,
                'userid':userid,
            }
        }));


    }

    _onBackPress(){
        console.log('backbutton1234566')
            //return this.props.navigation.navigate("Contacts");
        //this.setState({isHeader:true})
    }
    shouldComponentUpdate(nextProps) {
        /*if (this.props.message === nextProps.message) {
          return false
        }
    
        return true*/
        if(this.props.chats.channelmessage===nextProps.chats.channelMessages){
            return false
        }
        return true
        //console.log('nexpropssss',nextProps.chats)
    }
    
    
    componentDidUpdate(prevProps){
        console.log('prevProps',prevProps.chats,this.props.chats.channelmessage)
        if(prevProps.chats.channelmessage!=this.props.chats.channelmessage){
            console.log('update234567',this.props.chats.channelmessage)
            this.setState({channelMessages:this.props.chats.channelmessage})
            //this.scrollToSection(this.props.chats.channelmessage)
        }
    }

    _onCreateJoinRoom=(params)=>{
        var self=this;
        //console.log('params',params)
         this.roomName=this._roomGenerator({ userid: params.userid, clientid: params.clientid });

             this.socketconnection.on('connect',function(){
                 console.log('connectsssss')
                self.socketconnection.emit('join', self.roomName,function(errr,roomDescription){
                    if(errr){
                        self.socketconnection.emit('error',errr);
                    }else{
                        var id,
                        client,
                        type,
                        peer;
                        console.log('roomDescription.clients',roomDescription.clients)
                        for (id in roomDescription.clients) {
                            client = roomDescription.clients[id];
                            console.log('peeerrrss12234',client)
                            for (type in client) {
                                 if(client[type]){
                                    self.peer={
                                        id:id,
                                        type:type,
                                        enableDataChannels:null,
                                        receiveMedia: {
                                            offerToReceiveAudio: 0,
                                            offerToReceiveVideo: 0
                                        }                                       
                                    }
                                    console.log('createdpeer',self.peer);
                                    self.setState({connectionId:id})
                                    self.socketconnection.emit('createdPeer', self.peer);

                                    self._onSendOffer(
                                        {
                                            id:id,
                                            roomType:'video',type:'offer',
                                            callsign:self.props.profile.data.Users[0].Callsign,
                                            userid:self.props.profile.data.Users[0].UserID
                                        }
                                    );                                   
                                }
                            }
                        }                        
                    }
                });  
                self.socketconnection.emit('joinedRoom', self.roomName); 
            });

    }

    _onSendOffer=(payload)=>{
        let self=this;

        var message = {
            to: payload.id,
            sid: null,
            broadcaster: null,
            roomType: payload.roomType,
            type: payload.type,
            payload: {nick:payload.callsign,nickid:payload.userid},
            prefix: null
        };

        console.log('send offerrrrr',message);

        this.socketconnection.emit('message', message,(description)=>{

            console.log('errrors212323',description)
        });

        this.socketconnection.on('connect_error', (err) => {
            console.log('error123334',err)
        });

    }
    _roomGenerator=(params)=>{
        const {userid,clientid}=params;
        //let window;
        //window.btoa = require('Base64').btoa;
        let preRoomName = [userid, clientid, 'CHAT'];
        preRoomName = preRoomName.sort();
        preRoomName = preRoomName.join(',');
        //preRoomName = window.btoa(preRoomName);
        preRoomName = base64.encode(preRoomName);
        console.log('preRoomName',preRoomName)
        return preRoomName;
    }
    componentWillUnmount=()=>{
        this._isMounted = false;
        //this.props.actions.chats.onresetdata();
        //this.setState({chatMessage:'',buttonSendEnable:false})
        //this.backhanlder.remove();
        this.socketconnection.emit('leave');
        this.socketconnection.emit('leftRoom', this.roomName);
        //this.props.navigation.navigate('BottomTab');
        BackHandler.removeEventListener('hardwareBackPress', this._onBackPress);
        console.log('unmount')
    }
    componentDidCatch(error, info) {
        // Example "componentStack":
        //   in ComponentThatThrows (created by App)
        //   in ErrorBoundary (created by App)
        //   in div (created by App)
        //   in App
        console.log('errorrrrr',error);
    }
    _onHandleOpenWs=(events)=>{

        const userId=this.props.profile.data.Users[0].UserID;
        console.log('Open ws',events);
          this.ws.send(JSON.stringify({
            'type': 'login',
            'content': {
                'userId': userId,
            }
        }));

        setTimeout(() => {
            this.ws.send(JSON.stringify({
                'type': 'view_clients'
            }));
        }, 1000);

    }

    _onCreatePeers=(event)=>{

    }
    _onHandleMessageWs=(event)=>{


        
        const reader = new FileReader();

        console.log('eventtts',event.data);
        /*console.log('events',event.data);*/
        if(event.data instanceof ArrayBuffer){

            const datas=String.fromCharCode.apply(null, new Uint8Array(event.data));
            console.log('data',JSON.parse(datas))
            /*reader.onload = () => {
                console.log("Result:234 " + reader.result);
            }*/
        }

        /*let data;
        console.log('eventdata ',event);
        if (event.data instanceof ArrayBuffer) {
            const reader = new FileReader();

            reader.onload = () => {
                console.log("Result:234 " + reader.result);
                data = reader.result;
                data = JSON.parse(data);

                switch (data.type) {
                    case 'clients_list':
                        this.setState({ onlineUsers: data.content });
                        break;
                    case 'send_message':
                        const userClientDetails = this.state.userClientDetails;
                        if (localStorage.getItem("tabKey") === sessionStorage.getItem("tabKey")) {
                            this.playMessageAudio();
                        }
                        try {
                            if (userClientDetails.UserID !== data.content.userid) {
                                const newMessageElem = $(`#new-message-${data.content.userid}`);

                                newMessageElem.addClass("badge new");
                            }
                        } catch (err) {
                            console.log(err);
                        }
                        break;
                    // client is calling
                    case 'call':
                        // Api.getUserDetails(data.content.userid).then(result => {
                        //     this.setState({ userClientDetails: result })
                        // })
                        // alert('client is calling ' + JSON.stringify(data.content));

                        this.setState({ callerUserId: data.content.userid });

                        if (localStorage.getItem("tabKey") === sessionStorage.getItem("tabKey")) {
                            this.playIncomingCallAudio();
                        }

                        if (!document.hasFocus()) {
                            pushNotification({ body: 'Someone is calling', timeout: '4000', requireInteraction: true });
                        }
                        this.setState({ clientSocket: data.clientsocket });
                        answer_modal_instance.open();

                        setTimeout(() => {
                            answer_modal_instance.close();
                        }, 30000)
                        break;
                    // user cancelled the call
                    case 'cancel_call':
                        answer_modal_instance.close();
                        if (localStorage.getItem("tabKey") === sessionStorage.getItem("tabKey")) {
                            this.playEndCallAudio();
                        }
                        break;
                    // client answer the call
                    case 'answer':
                        const roomName = roomNameGenerator({ userid: data.content.userid, clientid: data.content.clientid })

                        // windowOpen({ URL: 'https://localhost:3000', timestamp: Date.now(), roomname: window.btoa(roomName) })
                        // windowOpen({ URL: 'https://chismis.live', timestamp: Date.now(), roomname: window.btoa(roomName) })

                        this.setState({ showVideoCall: true });

                        call_modal_instance.close();
                        break;
                    // user answer the call and close on other tabs
                    case 'answer_self':
                        answer_modal_instance.close();
                        break;
                    // client end the call 
                    case 'end_call':
                        call_modal_instance.close();
                        if (localStorage.getItem("tabKey") === sessionStorage.getItem("tabKey")) {
                            this.playDropCallAudio();
                        }
                        break;
                    // user end call and close on the other tabs
                    case 'end_call_self':
                        answer_modal_instance.close();
                        if (localStorage.getItem("tabKey") === sessionStorage.getItem("tabKey")) {
                            this.playEndCallAudio();
                        }
                        break;
                    case 'read_message':
                        const newMessageElem = $(`#new-message-${data.content.userid}`);
                        newMessageElem.removeClass("badge new");
                        break;
                    case 'send_direct_message':
                        console.log('send_direc_message', data);
                        break;
                    default:
                        break;
                };
            };

            reader.readAsText(event.data);
            } else {
            console.log("Result:1234 " + event.data);
            }

            if (data === undefined) {
            return;
            }
            console.log('onmessage', data);

            switch (data.type) {
            case 'clients_list':
                this.setState({ onlineUsers: data.content });
                break;
            case 'send_message':
                const userClientDetails = this.state.userClientDetails;
                if (localStorage.getItem("tabKey") === sessionStorage.getItem("tabKey")) {
                    this.playMessageAudio();
                }
                try {
                    if (userClientDetails.UserID !== data.content.userid) {
                        const newMessageElem = $(`#new-message-${data.content.userid}`);

                        newMessageElem.addClass("badge new");
                    }
                } catch (err) {
                    console.log(err);
                }
                break;
            // client is calling
            case 'call':
                // Api.getUserDetails(data.content.userid).then(result => {
                //     this.setState({ userClientDetails: result })
                // })
                // alert('client is calling ' + JSON.stringify(data.content));

                this.setState({ callerUserId: data.content.userid });

                if (localStorage.getItem("tabKey") === sessionStorage.getItem("tabKey")) {
                    this.playIncomingCallAudio();
                }

                if (!document.hasFocus()) {
                    pushNotification({ body: 'Someone is calling', timeout: '4000', requireInteraction: true });
                }
                this.setState({ clientSocket: data.clientsocket });
                answer_modal_instance.open();

                setTimeout(() => {
                    answer_modal_instance.close();
                }, 30000)
                break;
            // user cancelled the call
            case 'cancel_call':
                answer_modal_instance.close();
                if (localStorage.getItem("tabKey") === sessionStorage.getItem("tabKey")) {
                    this.playEndCallAudio();
                }
                break;
            // client answer the call
            case 'answer':
                const roomName = roomNameGenerator({ userid: data.content.userid, clientid: data.content.clientid })

                // windowOpen({ URL: 'https://localhost:3000', timestamp: Date.now(), roomname: window.btoa(roomName) })
                // windowOpen({ URL: 'https://chismis.live', timestamp: Date.now(), roomname: window.btoa(roomName) })

                this.setState({ showVideoCall: true });

                call_modal_instance.close();
                break;
            // user answer the call and close on other tabs
            case 'answer_self':
                answer_modal_instance.close();
                break;
            // client end the call 
            case 'end_call':
                call_modal_instance.close();
                if (localStorage.getItem("tabKey") === sessionStorage.getItem("tabKey")) {
                    this.playDropCallAudio();
                }
                break;
            // user end call and close on the other tabs
            case 'end_call_self':
                answer_modal_instance.close();
                if (localStorage.getItem("tabKey") === sessionStorage.getItem("tabKey")) {
                    this.playEndCallAudio();
                }
                break;
            case 'read_message':
                const newMessageElem = $(`#new-message-${data.content.userid}`);
                newMessageElem.removeClass("badge new");
                break;
            case 'send_direct_message':
                console.log('send_direc_message', data);
                break;
            default:
                break;
            };*/
    }

    _fetchData=async()=>{
        //console.log('User Information',this.props.navigation.getParam('data'))
        
        const userdata=this.props.navigation.getParam('data');
        console.log('fetchdatassss123',userdata);
        this.setState({Usercontact:userdata.Username})
        var payload={
            userContactId:userdata.UserID,
            header:{token:this.props.acesstoken},
            timezone:8,
            routes:'chat'
        }
        console.log('payloadssss',payload);
        
        this.props.actions.routes.onupdateroutes(payload)

        await this.props.actions.chats.fetchMessages(payload,payload.header);
        await this.setState({pagesize:this.props.totalitems})
    }

    _keyExtractor = (item, index) => 'key'+index;

    _getNotification=()=>{
  
    }

    _getProfile=()=>{
        this.props.navigation.navigate('Profile')
    }
    _getOnSearch=()=>{
        this.props.navigation.navigate('Search')
    }

    _onChat(){
        this.props.navigation.navigate('Chats',{
            name:'test',
        })
    }

    _onSendMessage=async(messagetext,contactuserid,channelmessage)=>{
        /*
          let data = new FormData();
        data.append('messagetext', params.messagetext);
        data.append('contactuserid', params.contactuserid);*/
        let payload = new FormData();
        
        const userdata=this.props.navigation.getParam('data');
        const {concatmsg}=this.state;
        var timeout;

        console.log('userdata',userdata);
        /*const socket=this.props.chats.socketinfo;

        if(socket){
            socket.emit('private_chat',{to:userdata.Username,message:messagetext,userId:userdata});
        }*/
        let dateposted=moment().format('YYYY-MM-DD,H:mm:ss');
        let dayposted=moment().format('l');
        let timeposted=moment().format('LT');
        console.log('dateposted',dateposted);
        //var timePosted=moment(item.TimePosted,"h:mm:ss A").format('LT');
        clearTimeout(timeout)
        if(concatmsg){
            var uniqString='橄欖'+messagetext;
            var newmsg=concatmsg.concat(uniqString);
            console.log('new Messages',newmsg);
            this.setState({concatmsg:newmsg});
            //this.props.actions.chats.updateMessages(messagetext)
        }else{
            var newmsg=concatmsg.concat(messagetext);
            this.setState({concatmsg:newmsg})
        }
        
        let payloadmsg={
            message:messagetext,
            userId:contactuserid,
            datePosted:dateposted,
            dayPosted:dayposted,
            timePosted:timeposted,
        }

        this.props.actions.chats.updateMessages(payloadmsg);

        this._onChatSendMessage(
            {
                callsign:this.props.profile.data.Users[0].Callsign,
                userid:this.props.profile.data.Users[0].UserID,
                message:messagetext,
                dateposted:dateposted,
                dayposted:dayposted,
                timePosted:timeposted,
            }
        )
        
        this._onSendUserStopTyping({callsign:this.props.profile.data.Users[0].Callsign,userid:this.props.profile.data.Users[0].UserID})

        //this._onRenderSectionList(channelmessage,contactuserid)
        //this._onClearChatmessage();
        this.props.actions.chats.onchatmessagesreset('');
        //this.props.actions.chats.onupdatetotalitems(this.props.totalitems);
        //this.props.totalitems
        this.setState(prevState=>({pagesize:prevState.pagesize+1}))
        this.props.actions.chats.onupdatetotalitems(this.props.totalitems)
        this.scrollToSection(this.props.totalitems);
        setTimeout(() => this._sendMessagestoDb(this.state.concatmsg,userdata), 5000)

    }

    _onChatSendMessage=(payload)=>{

        var message = {
            to: this.state.connectionId,
            sid: null,
            broadcaster: null,
            roomType: 'video',
            type: 'chat',
            payload: {nick:payload.callsign,nickid:payload.userid,message:payload.message},
            prefix: null
        };


        this.socketconnection.emit('message', message,(description)=>{

            console.log('errrors212323',description)
        });

        this.socketconnection.on('connect_error', (err) => {
            console.log('error123334',err)
        });

    }

    _sendMessagestoDb=async(messagetext,userdata)=>{

        console.log('record saveddd')
        let payload = new FormData();
        var header = {token:this.props.acesstoken};
        payload.append('messagetext', messagetext);
        payload.append('contactuserid', userdata.UserID);
        console.log('payload',payload);
        
        await this.props.actions.chats.sendMessages(payload,header);
        //await this._fetchData();
        
    }

    _onChatMessageChanged=(text)=>{
        var timeout;

            //this.setState({chatMessage:text,buttonSendEnable:true})
        this.props.actions.chats.onupdatechatmessages(text)

        clearTimeout(timeout);

        this._onSendUsertyping({callsign:this.props.profile.data.Users[0].Callsign,userid:this.props.profile.data.Users[0].UserID})

        setTimeout(() => this._onSendUserStopTyping({callsign:this.props.profile.data.Users[0].Callsign,userid:this.props.profile.data.Users[0].UserID}), 3000)
    
    }

    _onSendUserStopTyping=(payload)=>{

        const self=this;
        var message = {
            to: this.state.connectionId,
            sid: null,
            broadcaster: null,
            roomType: 'video',
            type: 'stoppedTyping',
            payload: {nick:payload.callsign,nickid:payload.userid},
            prefix: null
        };


        this.socketconnection.emit('message', message,(description)=>{

            console.log('errrors212323',description)
        });

        this.socketconnection.on('connect_error', (err) => {
            console.log('error123334',err)
        });
    }
    _onClearChatmessage=()=>{
        console.log('mounted',this._isMounted);
        if(this._isMounted){
            this.setState({chatMessage:'',buttonSendEnable:false})
        }else{
             //this.setState({chatMessage:'',buttonSendEnable:false})
            //this.state.chatMessage='';
            //this._onChatMessageChanged.bind(this);
        }
        //this._onChatMessageChanged.bind('');

    }

     _onRenderSectionList=(channelmessage,currentUserId)=>{
        //const currentUserId=currentUserId;
        console.log('channel messages245',channelmessage);
        const {status}=this.props.chats;
        
        console.log('statussss',status);
        switch(status[0]){
            case 2:
                console.log('catch statussss')
                    return (
                            <Spinner size='large' />                
                    );          
            break
            case 3:
            console.log('catch statussss123455')
                return (
                    <Spinner size='large' />                
                );                    
            default:
                if(channelmessage){
                    const VIEWABILITY_CONFIG= {
                        minimumViewTime: 300,
                        viewAreaCoveragePercentThreshold: 100,
                        waitForInteraction:true,
                      }
                    console.log('channel messages245',channelmessage);
                    return(
        
                        <SectionList
                            sections={this.state.channelMessages}
                            extraData={this.state}
                            renderSectionHeader={ ({section}) => this._onRenderSectionFooter(section)}
                            //renderItem={ ({item}) => this._onRendserSectionItem(item,currentUserId)}
                            renderItem={({item})=> this._onRendserSectionItem(item,currentUserId)}
                            keyExtractor={ (item, index) => item + index }
                            ref={ref => (this.sectionListRef = ref)}
                            getItemLayout={this.getItemLayout}
                            onEndReached={()=>console.log('end of the line')}
                            initialScrollIndex={this.props.totalitems}
                            viewabilityConfig={VIEWABILITY_CONFIG}
                            removeClippedSubviews={true}
                            //initialNumToRender={50}
                        
                        />
                    )
                }else{
                    return null
                }

        }

 
    }
    
    _sectionList=()=>{

        if(this.sectionListRef){
            this.sectionListRef.scrollToLocation({
                animated: true,
                sectionIndex: 0,
                itemIndex:0,
                viewPosition: 20
            });    
        }

    }

     _onHandlePicturePress = async () => {
        //Before calling getPhotos, request permission
        if (await this.requestExternalStoreageRead()){
            //console.log('aasdfasdf');
            CameraRoll.getPhotos({
                first: 20,
                assetType: 'All'
            })
            .then((r) => {
                console.log('aasdfasdf',r);
                 this.setState({ attachment: r.edges })
        })
        .catch((error) => {
            //this.setState({errorMsg: error.message});
            console.log('error123',error);
        })
      }


    }

    _toggleModal=()=>{
        this.setState({attachModal:!this.state.attachModal})
    }

    _onClose=()=>{
        this.setState({attachModal:false,isDeleteMessage:false,isForwardMessages:false,isHeader:true})
    }


    _onSendPictures=async(image)=>{
        //console.log('on Send Pictures',image)
        let dateposted=moment().format('YYYY-MM-DD,H:mm:ss');
        let dayposted=moment().format('l');
        let timeposted=moment().format('LT');

        let payload = new FormData();
        var header = {token:this.props.acesstoken};
        const channelId=this.props.chats.channelid;
        console.log('decodeImage',channelId);

        payload.append('attachment_files',{uri:image.image.uri,type:image.type,name:'photo.jpg'});
        payload.append('thread_id', channelId);
        console.log('payload',payload);
        const {channelmessage}=this.props.chats;
        const userdata=this.props.navigation.getParam('data');
        const currentUserId=this.props.profile.data.Users[0].UserID;

        const payload_={
            image:image.image.uri,
            userId:currentUserId,
            DatePosted:dateposted,
            TimePosted:timeposted,DayPosted:dayposted
        }

        await this.props.actions.chats.updatePictures(payload_);
        await this._onRenderSectionList(channelmessage,currentUserId)
        await this.props.actions.chats.sendPictures(payload,header,userdata.UserID);
        await this._onClose();

        //await this._fetchData();
    }


    requestExternalStoreageRead=async()=>{
        try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
              {
                'title': 'Cool Photo App Camera Permission',
                'message': 'Cool Photo App needs access to your camera ' +
                           'so you can take awesome pictures.'
              }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                return granted == PermissionsAndroid.RESULTS.GRANTED;
              console.log("You can use the camera")
              return PermissionsAndroid.RESULTS.GRANTED
            } else {
              console.log("Camera permission denied")
            }
          } catch (err) {
            console.warn(err)
          }
    }

    _onSendUsertyping=(payload)=>{
       
        const self=this;
        var message = {
            to: this.state.connectionId,
            sid: null,
            broadcaster: null,
            roomType: 'video',
            type: 'typing',
            payload: {nick:payload.callsign,nickid:payload.userid},
            prefix: null
        };


        this.socketconnection.emit('message', message,(description)=>{

            console.log('errrors212323',description)
        });

        this.socketconnection.on('connect_error', (err) => {
            console.log('error123334',err)
        });


    }
 
    _onRenderSectionFooter=(section)=>{
        console.log('sectionssss',section);
        return(
            <View style={chatstyles.sectionFooterStyle}>
                <Text style={chatstyles.sectionFooterTextStyle}> { section.DayPosted } </Text> 
            </View>
        );
    }
    _onForwardLongPress=()=>{
        this.setState({isHeader:false,frwdmsgpayload:msgpayload})
        console.log('longpressssss');
    }
    _onRendserSectionItem=(item,currentUserId)=>{
        //console.log('itemsssss234',item,currentUserId);
        var timePosted=moment(item.TimePosted,"h:mm:ss A").format('LT');
            return(
                    <View style={item.UserID==currentUserId ? chatstyles.talkBubblesender : chatstyles.talkBubblereceiver}>
                        <View style={{flexDirection:'row'}}>
                                <TouchableOpacity
                                        //onPress={(item)=>this.scrollToSection(this.state.channelMessages)}
                                        //enabled={this.state.buttonSendEnable}
                                        //onPress={()=>this._onForwardLongPress(item.MessageText)}
                                        onLongPress={()=>this.setState({isHeader:false,frwdmsgpayload:item.MessageText})}
                                        >

                                    <View style={chatstyles.talkBubbleSquare}>
                                        <View>
                                                {
                                                    item.LocationReference ?
                                                        
                                                        <Image
                                                            style={{
                                                            //width: width/1.5,
                                                            //height: width/3
                                                            width: 200,
                                                            height: 150,
                                                            resizeMode: 'stretch',
                                                            }}
                                                            source={{uri: item.LocationReference,cache:'only-if-cached'}}
                                                            resizeMethod={'auto'}
                                                            //resizeMode={Image.resizeMode.contain}
                                                            //source={require(item.LocationReference)}
                                                        />
                                                        
                                                    :
                                                        <Text style={chatstyles.chatMessageFont}>{item.MessageText}</Text>
                                                }
                                        </View>

                                        <View>
                                            <Text style={chatstyles.chatMessageFont}>{timePosted}</Text>
                                        </View>
                                    </View>
                                    
                                    </TouchableOpacity>
                        </View>
                </View>
        )
    }
    _onDeleteMessages=()=>{
        console.log('actionssss')
        this.props.actions.chats.deleteMessages()
        //this.setState({isDeleteMessage:true})
    }

    _onShowDeleteMessages=()=>{
        console.log('actionssss')
        this.props.actions.chats.oninitdelemessages()
        this.setState({isDeleteMessage:true})
    }

    _onShowForwardMessages=()=>{
        console.log('forward messages')
        this.setState({isForwardMessages:true})
    }

    getItemLayout=(data,index)=>({
        length:0,
        offset:60 * index,       
        //offset:ITEM_HEIGHT, 
        index,       
    });

    scrollToSection = (item) => {
        /*console.log('itemsssss',item);

        
        var lengths = item.map(function(word){
            console.log('loggsss1234',word.data)
            return word.data.length
        });
        
        let totallenght=0;
        for(x=0;x<lengths.length;x++){
            console.log('lengthesss',lengths[x]);
            totallenght=totallenght+lengths[x];
        }
        console.log('lengthesss123456',item);*/
        console.log('itemmmmms',item);
        this.sectionListRef.scrollToLocation({
            animated: true,
            sectionIndex: 0,
            itemIndex:item,
            viewPosition: 0
          });
    };

    _onRenderdeleteStatus=(status)=>{
        console.log('delstatussss',status);
        switch(status[0]){
            case 1:
                return <Messagebox message={'SUCCESS'} onClose={this._onClose} type={'success'} />

            break;
            case 2:

                return <Spinner size='large' />   

            break;
            case 3:
                return <Messagebox message={status[1]} onClose={this._onClose} type={'error'} />
            break;
            default:

                return <Messagebox message={'Delete Message'} OnDelete={this._onDeleteMessages} onClose={this._onClose} type={'deletemsg'} />
        }
    }
    
    render(){
        const data=this.state.data.data;
        const {userlist,channelid,channelmessage}=this.props.chats;
        const {chatMessage,}=this.state;
        const currentUserId=this.props.profile.data.Users[0].UserID;
        //const messageIndex=this.state.channelMessages[0].data.length
        console.log('contacts23434',this.props.contacts.usercontactlist);
        console.log('header',this.state.isHeader);
        return(
            <GeneralContainers>

                {/*<GeneralHeaders>
                    <View style={styles.headerTitle}>
                        <Text style={{fontSize:20,fontFamily:'Helvetica'}}>{this.state.Usercontact}</Text>
                    </View>
                </GeneralHeaders>*/}
                <View style={chatstyles.chatHeaders}>
                    {   
                        this.state.isHeader ?
                            <View style={styles.headerTitle}>
                                <View style={{marginLeft:10,width:'40%',justifyContent:'flex-start'}}>
                                    <TouchableOpacity
                                        onPress={(item)=>this.props.navigation.navigate('BottomTab')}
                                    >
                                        <Icon name='ios-arrow-back' size={40} color="white" /> 
                                    </TouchableOpacity>    
                                </View>
                                <View style={{width:'50%',marginRight:10,justifyContent:'center'}}>
                                    <Text style={{fontSize:20,fontFamily:'Helvetica',color:'white'}}>{this.state.Usercontact}</Text>
                                </View>
                                
                            </View>
                        :
                             <View style={chatstyles.headerTitle}>
                                <View style={{marginLeft:10,width:'45%',justifyContent:'flex-start'}}>
                                    <TouchableOpacity
                                        onPress={(item)=>this.setState({isHeader:true})}
                                    >
                                        <Icon name='ios-arrow-back' size={40} color="white" /> 
                                    </TouchableOpacity>    
                                </View>                             
                                <View style={{width:'35%',marginRight:10,justifyContent:'center'}}>
                                    <TouchableOpacity
                                        onPress={(item)=>this._onShowDeleteMessages()}
     
                                    >
                                        <Icon name='ios-trash' size={40} color="white"/> 
                                    </TouchableOpacity>    
                                </View>
                                <View style={{marginLeft:10}}>
                                    <TouchableOpacity
                                        onPress={(item)=>this._onShowForwardMessages()}
                                    >
                                        <Icon name='ios-redo' size={40} color="white"/> 
                                    </TouchableOpacity>                                 
                                </View>
                            </View>                           
                    }
                </View>


                <View style={chatstyles.generalContents}>                        
                    <Modal
                        animationType={"slide"}
                        transparent={false}
                        visible={this.state.attachModal}
                        onRequestClose={() => this._onClose()}
                            >
                            <Attachments attachment={this.state.attachment} onClose={this._onClose} onSendPictures={this._onSendPictures} />
                    </Modal>
                    <Modal
                        animationType={"slide"}
                        transparent={true}
                        visible={this.state.isDeleteMessage}
                        onRequestClose={() => this._onClose()}
                            >
                            {   
                                this._onRenderdeleteStatus(this.props.delstatus)
                            }
                             
                    </Modal>
                    <Modal
                        animationType={"none"}
                        transparent={true}
                        visible={this.state.isForwardMessages}
                        onRequestClose={() => this._onClose()}
                            >
                        <Forwardmessages contactlist={this.props.contacts.usercontactlist} frwdmsgpayload={this.state.frwdmsgpayload} onClose={this._onClose}/>
                             
                    </Modal>
                    <View style={chatstyles.flatlist}>   
                        {
                            this.state.channelMessages ?
                            <View style={{width:'100%'}}> 
                                {this._onRenderSectionList(this.state.channelMessages,currentUserId)}
                                    {/*<View style={{backgroundColor:'transparent',position:'absolute',bottom: 0, left: 0, right: 10 }}>
                                            <View style={{alignItems:'flex-end'}}>
                                                <TouchableOpacity
                                                    onPress={(item)=>this.scrollToSection(this.state.channelMessages)}
                                                    enabled={this.state.buttonSendEnable}
                                                >
                                                    <Icon name='ios-arrow-down' size={50}/> 
                                                </TouchableOpacity>
                                            </View>
                    
                                        </View>*/}
                                    
                                </View>
                            :
                                null    
                        }                   
                                              
                    </View>
                    <View>
                        {
                            this.state.isTyping ?
                                <Text>{this.state.isTypingpayload.nick} is typing</Text>
                            :
                                null
                        }

                    </View>
                </View>

                <View style={chatstyles.chatMessagesContainer}>
                    <View style={chatstyles.textAreaContainer}>
                        <View style={{width:'80%'}}>
                            <TextInput
                                onChangeText={this._onChatMessageChanged.bind(this)}
                                //value={this.state.chatMessage}
                                value={this.props.chatmessagess}
                                placeholder='Message'
                                //autoFocus={true}
                                //style={styles.txtAreaStyles}
                                multiline = {true}
                                numberOfLines = {4}
                            />
                        </View>
                        <View style={{marginRight:10}}>
                            <TouchableOpacity
                                onPress={()=>{this._toggleModal();this._onHandlePicturePress()}}
                                enabled={this.state.buttonSendEnable}
                            >
                                <Icon name='ios-camera' size={30}/> 
                            </TouchableOpacity>
        
                        </View> 
                        <View>
                            <TouchableOpacity
                                onPress={()=>console.log('attachment')}
                                enabled={this.state.buttonSendEnable}
                            >
                                <Icon name='ios-attach' size={30}/> 
                            </TouchableOpacity>
       
                        </View> 
                    </View>   
            
                    <View>
                        {
                            //this._onRenderSendButton(this.props.chatmessagess)
                            this.props.chatmessagess=='' || null ?
                                <TouchableOpacity
                                    onPress={()=>this._onSendMessage(this.props.chatmessagess,currentUserId,channelmessage)}
                                    disabled={true}
                                >
                                    <Icon name='ios-send' size={30}/> 
                                
                                </TouchableOpacity>  
                            :
                                <TouchableOpacity
                                    onPress={()=>this._onSendMessage(this.props.chatmessagess,currentUserId,channelmessage)}
                                    disabled={false}
                                >
                                    <Icon name='ios-send' size={30}/> 
                                
                                </TouchableOpacity>                                
                        }
                    </View>                            

                </View>
            </GeneralContainers>
        );
    }
}                   

function mapStateToProps(state){
    //console.log('logstates2345637',state);
    return{
        acesstoken:state.security.login.accesstoken,
        profile:state.datareducer.profile,
        contacts:state.datareducer.contacts,
        chats:state.datareducer.chats,
        chatmessagess:state.datareducer.chats.chatmessagess,
        delstatus:state.datareducer.chats.delstatus,
        websocket:state.security.login.websocketinfo,
        webrtc:state.security.login.webrtcInfo,
        routes:state.datareducer.routes,
        profile:state.datareducer.profile,
        userid:state.security.login.userid,
        totalitems:state.datareducer.chats.totalitems
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            chats:bindActionCreators(chatActions,dispatch),
            routes:bindActionCreators(routesActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Chat)