import React, {PureComponent} from 'react';
import { 
    Text, 
    View, 
    FlatList,
    Platform,
    TextInput,
    TouchableHighlight,
    TouchableOpacity,
    SectionList,
    CameraRoll,
    Modal,
    PermissionsAndroid,
    Image,
    Dimensions 
} from 'react-native';
import chatstyles from './styles';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Ionicons';

export default class messages extends PureComponent{

    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:{
                flagno:1,
                message:'success',
            },
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            chatMessage:'',
            Usercontact:'',
            buttonSendEnable:false,
            concatmsg:'',
            attachment:null,
            attachdata:null,

        };        
    }

    componentDidMount(){
        this._fetchData();
    }


    _fetchData=async()=>{

    }

    _keyExtractor = (item, index) => 'key'+index;

    _getNotification=()=>{
  
    }



    setIndex = (index) => {
        if (index === this.state.index) {
          index = null
        }
        this.setState({ index })
    }

    _getPictures=(image)=>{
        console.log('image location',image)
        this.props.onSendPictures(image)
    }

    _onRenderSectionFooter=(section)=>{
        console.log('sectionssss',section);
        return(
            <View style={chatstyles.sectionFooterStyle}>
                <Text style={chatstyles.sectionFooterTextStyle}> { section.DayPosted } </Text> 
            </View>
        );
    }

    _onRendserSectionItem=(item,currentUserId)=>{
        //console.log('itemsssss234',item,currentUserId);
        var timePosted=moment(item.TimePosted,"h:mm:ss A").format('LT');
        //console.log('loggs render section',item.UserID+' '+currentUserId);
        if(item.UserID==currentUserId){
            return(
                <View style={chatstyles.talkBubblesender}>
                    <View style={chatstyles.talkBubbleSquare}>
                        <View>
                            {
                                item.LocationReference ?
                                    <Image
                                        style={{
                                        //width: width/1.5,
                                        //height: width/3
                                        width: 200,
                                        height: 150,
                                        resizeMode: 'stretch',
                                        }}
                                        source={{uri: item.LocationReference}}
                                        //resizeMode={Image.resizeMode.contain}
                                        //source={require(item.LocationReference)}
                                    />
                                :
                                    <Text style={chatstyles.chatMessageFont}>{item.MessageText}</Text>
                            }
                        </View>

                        <View>
                            <Text style={chatstyles.chatMessageFont}>{timePosted}</Text>
                        </View>
                    </View>

                </View>
            )
        }else{
            return(
                
                <View style={chatstyles.talkBubblereceiver} /*style={chatstyles.talkBubblesender}*/>
                    <View style={chatstyles.talkBubbleSquare}>

                        <View>
                                {
                                    item.LocationReference ?
                                        <Image
                                            style={{
                                            width: 200,
                                            height: 150,
                                            resizeMode: 'stretch',
                                            }}
                                            source={{uri: item.LocationReference}}
                                            //resizeMode={Image.resizeMode.contain}
                                            //source={require(item.LocationReference)}
                                        />
                                    :
                                        <Text style={chatstyles.chatMessageFont}>{item.MessageText}</Text>
                                }
                        </View>

                        <View>
                            <Text style={chatstyles.chatMessageFont}>{timePosted}</Text>
                        </View>

                    </View>

                </View>
            )            
        }
    }

    getItemLayout=(data,index)=>({
        length:0,
        offset:60 * index,       
        //offset:ITEM_HEIGHT, 
        index,       
    });

    scrollToSection = (item) => {
        //console.log('itemsssss',item[0].data.length);

        
        var lengths = item.map(function(word){
            console.log('loggsss1234',word.data)
            return word.data.length
        });
        
        let totallenght=0;
        for(x=0;x<lengths.length;x++){
            console.log('lengthesss',lengths[x]);
            totallenght=totallenght+lengths[x];
        }
        console.log('lengthesss123456',item);

        this.sectionListRef.scrollToLocation({
          animated: true,
          sectionIndex: 0,
          itemIndex:totallenght,
          viewPosition: 0
        });
    };

    render(){
        console.log('channell messagesss',this.props.channelMessages)
        return(
            <View style={{width:'100%'}}> 
                <View>                        
                    <SectionList
                        sections={this.props.channelMessages}
                        extraData={this.props}
                        renderSectionFooter={ ({section}) => this._onRenderSectionFooter(section)}
                        //renderItem={ ({item}) => this._onRendserSectionItem(item,currentUserId)}
                        renderItem={({item})=> this._onRendserSectionItem(item,this.props.currentUserId)}
                        keyExtractor={ (item, index) => item + index }
                        ref={ref => (this.sectionListRef = ref)}
                        getItemLayout={this.getItemLayout}
                        onEndReached={()=>console.log('end of the line')}
                        initialNumToRender={50}
                    />
                                    
                </View>
                <View style={{backgroundColor:'transparent',position:'absolute',bottom: 0, left: 0, right: 10 }}>
                        <View style={{alignItems:'flex-end'}}>
                            <TouchableOpacity
                                onPress={(item)=>this.scrollToSection(this.props.channelMessages)}
                                enabled={this.state.buttonSendEnable}
                            >
                                <Icon name='ios-arrow-down' size={50}/> 
                            </TouchableOpacity>
                        </View>

                </View> 
            </View>
        );
    }
                    
}