import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform,Modal,TouchableHighlight,TouchableOpacity,SectionList } from 'react-native'
import { connect } from 'react-redux';
import {GeneralContainers,GeneralHeaders,GeneralContents,Messagebox,Spinner} from '../../components';
import { bindActionCreators } from 'redux';
import * as chatActions from '../datas/chats/actions';
import styles from '../styles';
import chatstyles from './styles';


import * as contactActions from '../datas/contacts/actions';



class Forwardmessages extends Component{

    constructor(props){
        super(props);

        this.state={
            loading:false,
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            channelForm:false,
            contactForm:false,
            onlineStatusForm:false,
            messageBox:true,
            onlineStatus:null,
            onclickstatus:false,
        };
    }
    
    /*static navigationOptions ={
        title: 'CALLS',
        headerStyle:{
            backgroundColor: 'white'
        },
        headerTitleStyle:{
            alignSelf:'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign:'center'
        }
    }*/

    
    componentDidMount(){
        this._fetchData(this.props.contactlist);
    }


    _fetchData=(contacts)=>{
        //console.log('fetchsssssss',this.props.actions);
        this.props.actions.chats.fetchgroups(contacts)
    }

    _keyExtractor = (item, index) => item.firstName;


    _closeContactForm=()=>{
        this.setState({showForm:false,contactForm:false})
    }

    _onRenderSectionHeader=(section)=>{
        console.log('sectionssss12345777',section);
        return(
            <View style={chatstyles.frwdmsgsectionheader}>
                <Text style={chatstyles.sectionFooterTextStyle}>{section.title}</Text> 
            </View>
        );
    }

    _onSendForwardMessage=async(payload)=>{
        console.log('onpayload23234',payload);
        await this.props.actions.chats.onupdatecontactstatus(payload)
        await this.setState({onclickstatus:true})
        
        await setTimeout(() => {
            this._sendMessagestoDb(this.props.frwdmsgpayload,{UserID:payload.newpayload.UserID})
            this.props.actions.chats.onupdatecontactstatus({prevpayload:payload.prevpayload,newpayload:{Callsign:payload.newpayload.Callsign,status:[1,'']}})
            this.setState({onclickstatus:false})
        }, 2000)
        
    }
    _sendMessagestoDb=async(messagetext,userdata)=>{

        console.log('record saveddd')
        let payload = new FormData();
        var header = {token:this.props.acesstoken};
        payload.append('messagetext', messagetext);
        payload.append('contactuserid', userdata.UserID);
        console.log('payload',payload);
        
        await this.props.actions.chats.sendMessages(payload,header);
        //await this._fetchData();
        
    }
    _onRendserSectionItem=(item)=>{
        console.log('sectionssss12345666',item);
       return(
           <View style={chatstyles.frwdmsgitemcontainer}>
                <View style={chatstyles.frwdmsgcontentStyle}>
                    <View style={chatstyles.frwdmsgroundAvatarStyle}>
                        <View style={{justifyContent:'center',alignItems:'center'}}>
                            <Text>{item.Callsign}</Text>
                        </View>
                    </View>
                    <View style={{width:'75%',alignItems:'flex-end',justifyContent:'center'}}>
                        <View style={{width:'30%',alignItems:'center',borderRadius:20,borderColor:'gray',borderWidth:1}}>
                            {
                                this._onRenderItemStatus(item)
                            }
   
                        </View>
  
                    </View>
                </View>
                <View style={chatstyles.frwdmsgitemlineseparator}></View>
           </View>
       )
    }   
    _onRenderItemStatus=(item)=>{

        switch(item.status[0]) {
            case 1:
                return(
                    <TouchableOpacity style={{height:30,alignItems:'center',justifyContent:'center'}}
                        onPress={()=>this._onSendForwardMessage({prevpayload:this.props.chatgroups,newpayload:{Callsign:item.Callsign,status:[2,'']}})}
                        disabled={true}
                    >
                        <Text>SENT</Text>
                    </TouchableOpacity>  
                );                
            break;
            case 2:
                return <Spinner />
            break;
            default:
              return(
                <TouchableOpacity style={{height:30,alignItems:'center',justifyContent:'center'}}
                    onPress={()=>this._onSendForwardMessage({prevpayload:this.props.chatgroups,newpayload:{Callsign:item.Callsign,UserID:item.UserID,status:[2,'']}})}
                >
                    <Text>SEND</Text>
                </TouchableOpacity>   
              );
        }
    }


    
    
    render(){
        //const data=this.state.data.data;
        //const contactstatus = this.props.contacts.contactstatus;
        //const onlineStatus =this.props.profile.data.Users;
        //const onlineStatus = this.props.profile.onlinestatus;
        //console.log('profile status',this.props.profile.data.Users);
        const usercontactlist=this.props.chatgroups;
        console.log('usercontactlist12345',this.props.frwdmsgpayload)
        return(
            <GeneralContainers> 
                <View style={chatstyles.chatHeaders}>
                    <View style={styles.headerTitle}>
                        <Text style={{fontSize:20,fontFamily:'Helvetica'}}>SEND TO</Text>
                    </View>
                </View>               
                {   
                    
                    usercontactlist ?
                        <View style={{width:'100%',height:'80%'}}>
                            <View style={{marginLeft:20,marginRight:20}}>
                                <SectionList
                                    sections={usercontactlist}
                                    extraData={this.props}
                                    renderSectionHeader={ ({section}) => this._onRenderSectionHeader(section)}
                                    renderItem={({item})=> this._onRendserSectionItem(item)}
                                    keyExtractor={ (item, index) => item + index }
                                    //ref={ref => (this.sectionListRef = ref)}
                                    //getItemLayout={this.getItemLayout}
                                    //onEndReached={()=>console.log('end of the line')}
                                    //initialScrollIndex={this.props.totalitems}
                                    //viewabilityConfig={VIEWABILITY_CONFIG}
                                    removeClippedSubviews={true}
                                />
                            </View>
                        </View>

                    :
                     null
                }

            </GeneralContainers>
        )    
    }
}
function mapStateToProps(state){
    //console.log('logstates2346789',state.datareducer);
    return{
        acesstoken:state.security.login.accesstoken,
        userid:state.security.login.userid,
        profile:state.datareducer.profile,
        chatgroups:state.datareducer.chats.chatgroups,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            contacts:bindActionCreators(contactActions,dispatch),
            chats:bindActionCreators(chatActions,dispatch),
            //profile:bindActionCreators(profileActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Forwardmessages)