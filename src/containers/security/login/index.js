import React, { Component, PureComponent,YellowBox } from 'react';
import { Text, View, FlatList,TextInput,Image,Platform,Alert,Modal,TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as loginActions from '../datas/login/actions';
import * as profileActions from '../../datas/profile/actions';
import * as contactActions from '../../datas/contacts/actions';
import styles from './styles';
import { Button,Textbox } from '../../../components/common';
import { CardSection,Spinner,Messagebox } from '../../../components';
import ActionButton from 'react-native-action-button';
import {AsyncStorage} from 'react-native';

export class Login extends Component{

    /*static navigationOptions = {
        title: 'Login',
        headerStyle: {
          backgroundColor: 'white'
        },
        headerTitleStyle :{
            alignSelf: 'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign: 'center',
        },
        headerLeft: null,

    }*/


    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:[],
            page:1,
            seed:1,
            error:null,
            refreshing:false,
            showForm:false,
            messageBox:true,
        }

        console.ignoredYellowBox = [
            'warning:Setting a timer'
        ];



    };

    componentDidMount(){
        this._fetchData();
    }



    componentDidUpdate(prevProps){
        const data=this.props.login.login.data;
        const status=this.props.login.login.status;
        const datas = prevProps.login.login;

        //console.log('Prevprops234',prevProps.login.login.userid);
        if(prevProps.login.login.userid!=this.props.login.login.userid){
            const payload={
                username:this.props.login.login.email,
                password:this.props.login.login.password,
                accesstoken:this.props.login.login.accesstoken,
                userid:this.props.login.login.userid,
                header:{'Content-Type':'application/json','Authorization':'Bearer ' + this.props.login.login.accesstoken}
            }
            console.log('loggss343556',this.props.login.login.accesstoken);
            this.props.actions.profile.fetchprofile(payload);
            //this.props.actions.contacts.fetchcontactlist(payload,{Token:this.props.login.login.accesstoken});


            //this.props.navigation.navigate('BottomTab')
            //console.log('Prevprops23466',payload);
        }

        if(status[0]==1){
            let accesstoken=this.props.login.login.data.Login[0].Token
            let userid=this.props.login.login.data.Login[0].UserID
            console.log('logggssdfsdf',this.props.login.login.data.Login);
            let payload={
                username:'',
                password:'',
                accesstoken:accesstoken,
                userid:userid,
                header:{'Content-Type':'application/json','Authorization':'Bearer ' + accesstoken}
            }

            AsyncStorage.setItem('usercredentials', JSON.stringify({
                    accesstoken:accesstoken,
                    userid:userid,
                    header:{'Content-Type':'application/json','Authorization':'Bearer ' + accesstoken}
                })
            );
            
            this.props.actions.profile.fetchprofile(payload);
            this.props.navigation.navigate('BottomTab')
            
        }
        /*if(data){
            return this.props.navigation.navigate('BottomTab')
        }*/

    }
    _fetchData=()=>{

    }

    _onEmailChanged=(text)=>{
        this.props.actions.login.emailChanged(text);
    }

    _onPasswordChanged=(text)=>{
        this.props.actions.login.passwordChanged(text);
    }

    _onLogin=async()=>{
        var data= new FormData();
        this.setState({messageBox:true})
        const payload={
            username:this.props.login.login.email,
            password:this.props.login.login.password,
            //accesstoken:this.props.login.login.accesstoken,
            //userid:this.props.login.login.userid,
        }
        //dispatch(accesstoken(res.Data.Login[0].Token))
        
        data.append('username',this.props.login.login.email);
        data.append('password',this.props.login.login.password);

        console.log('logs',data);
        await this.props.actions.login.reset();
        await this.props.actions.login.fetchlogin(data);
        
        //this.props.navigation.navigate('BottomTab')
    }

    _onRegister(){
        this.props.navigation.navigate('Registration')
    }

    _onForgotPassword(){

    }
    _onAlertPressOK(){
        //this.props.actions.login.reset()
    }
    _onRenderButton=(status)=>{

        switch (status[0]){
            case 0:
                /*Alert.alert(
                    'Alert Title',
                    status[1],
                    [
                    {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => console.log('OK Pressed')},
                    ],
                    { cancelable: false }
                )  
                return(
                    <Button onPress={()=>this._onLogin()}>
                        Login
                    </Button>
                );*/  
                if(this.state.messageBox){      
                    return(
                        <View style={{flex:1}}>
                            <Modal
                                animationType="none"
                                transparent={true}
                                visible={this.state.messageBox}
                                onRequestClose={() => {
                                    Alert.alert('Modal has been closed.');
                            }}>
                                <Messagebox message={status[1]} onClose={this._closeMessagebox}/>
        
                            </Modal>
                        </View>
                    );               
                }
                    return(
                        <Button onPress={()=>this._onLogin()}>
                            Login
                        </Button>
                    );
                break;
            case 1:
                return(
                    <Button onPress={()=>this._onLogin()}>
                        Login
                    </Button>
                );    
            case 2:
                return <Spinner size='large' />     
            case 3:
                return(
                    <Button onPress={()=>this._onLogin()}>
                        Login
                    </Button>
                );                       
        }


        if(status[0]==1 || status[0]==3){
            return(
                <Button onPress={()=>this._onLogin()}>
                    Login
                </Button>
            );
        }
        if(status[0]==0){
            alert
        }
        return <Spinner size='large' />
    }
    _closeMessagebox=()=>{
        this.setState({messageBox:false})
    }

    _openMessagebox=()=>{
        this.setState({showForm:true})
    }
    render(){
        //const data=this.props.profile
        const status=this.props.login.login.status;

        //console.log('loggsss2345',this.props);
        return(
            <View style={styles.container}>
                <CardSection>
                    <View style={styles.logoSplash}>
                            <Image source={require('../../../image/logosketch.png')} style={{height:150,width:150,marginTop:50}} />
                    </View>                  
                </CardSection>
                
                <CardSection>
                    <View style={styles.textInput}>
                        <Textbox 
                            label='Username'
                            placeholder='Username or Email'
                            secureTextEntry={false}
                            value={this.props.username}
                            returnKeyType = { "next" }
                            onChangeText={this._onEmailChanged.bind(this)}
                            onSubmitEditing={() => { this.inputPassword.focus(); }}
                        />
                    </View>
                </CardSection>
        
                <CardSection>
                    <View style={styles.textInput}>
                        <Textbox 
                            label='Password'
                            placeholder='Password'
                            reference={(input) => { this.inputPassword = input }}
                            returnKeyType = { "done" }
                            secureTextEntry={true}
                            value={this.props.password}
                            onChangeText={this._onPasswordChanged.bind(this)}
                        />
                    </View>
                </CardSection>
                
                    <CardSection>
                        <View style={styles.buttonStyle}>
                            {this._onRenderButton(status)}
                        </View>
                    </CardSection>

                <CardSection>
                    <View style={styles.buttonStyle}>
                        <Button onPress={()=>this._onRegister()}>
                            Register
                        </Button>
                    </View>
                </CardSection>
                <CardSection>
                    <View style={styles.buttonStyle}>
                        <Button onPress={()=>this._onForgotPassword()}>
                                Forgot Password
                        </Button>
                    </View>
                </CardSection>
            
            </View>
        );
    }
}                   

function mapStateToProps(state){
    console.log('logstates2349999',state.security);
    return{
        login:state.security,
        profile:state.datareducer.profile,
        contacts:state.datareducer.contacts,
        //accesstoken:state.security.login.data.Lo
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            login:bindActionCreators(loginActions,dispatch),
            profile:bindActionCreators(profileActions,dispatch),
            contacts:bindActionCreators(contactActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)