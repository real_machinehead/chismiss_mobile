export default{
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'column',
        backgroundColor:'green',
    },
    content:{
        height:'100%',
        width:'100%',
        backgroundColor: 'powderblue',
        justifyContent:'center',
        alignItems:'center',
    },
    textInput:{
        justifyContent:'center',
        alignItems:'center',
        width:'98%',
        height:40,
        marginTop:10,
        shadowColor:'#000',
        shadowOffset:{width:0,height:2}
    },
    buttonStyle:{
        justifyContent:'center',
        alignItems:'center',
        width:'98%',
        height:40,
        marginTop:10,
        shadowColor:'#000',
        shadowOffset:{width:0,height:2}
    },
    inputStyle:{
        color:'#000',
        paddingRight:5,
        paddingLeft:5,
        fontSize:18,
        lineHeight:23,
        height:70,
        flex:3.5,
    },
    logoSplash:{
        justifyContent:'center',
        alignItems:'center',
        width:'98%',
        height:'15%',
        marginTop:15,
        shadowColor:'#000',
        shadowOffset:{width:0,height:2}
    },
}