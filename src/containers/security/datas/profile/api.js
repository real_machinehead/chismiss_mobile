import {fetchApi} from '../../../../globals/api';
import * as endPoints from '../../../../globals/endpoints';


export let fetch = payload=>fetchApi(endPoints.reports.userdetails.get(payload),payload,'get');