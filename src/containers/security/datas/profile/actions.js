import * as api from './api';
import * as actiontypes from './actiontypes';
import {constants} from '../../../../constants';

export const reset = payload=>({
    type:actiontypes.RESET,
    payload:payload
});

export const update = payload=>({
    type:actiontypes.UPDATE,
    payload:payload
});

export const status = payload=>({
    type:actiontypes.STATUS,
    payload:payload
});

export const init = payload=>({
    type:actiontypes.INITIALIZE,
    payload:payload
});

export const fetchprofile = payload =>
    dispatch=>{
        let objResponse={};
        console.log('payloads',payload);
        dispatch(status(constants.status.LOADING));
        dispatch(reset())
        api.fetch(payload)
        .then((response)=>response.json())
        .then((res)=>{
            //console.log('fetchlogs1234',res.Data);
            dispatch(init(res.Data))
            //dispatch(status([1,'success']));
            //dispatch(init(res.data))
            objResponse={...res}
        })
        .then(()=>{
            /*dispatch(status([
                objResponse.flagno || 0,
                objResponse.message || constants.ERROR.SERVER
            ]));*/
        })
        .catch((exception)=>{
            dispatch(status([
                0,
                exception.message
            ]));    
        });
    };