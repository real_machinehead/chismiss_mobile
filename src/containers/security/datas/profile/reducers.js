import * as actionTypes from './actiontypes';
import { combineReducers } from 'redux';
import { constants } from '../../../../constants';

const initstate='';
const initstatus =constants.status.LOADING;

export const data = (state=initstate, action )=>{
    switch(action.type){
        case actionTypes.INITIALIZE:
            return action.payload;
            break;
        case actionTypes.UPDATE:
            //oState.resultLists = oHelper.arrayMerge(oState.resultLists, action.payload, 'notimeinid');
            return action.payload;
            break;
        default:
            return state;     
    }
};


const status =(state = initstatus, action)=>{
    switch (action.type){
        case actionTypes.STATUS:
            return action.payload;
            break;
        default:
            return state;
    }
}

export const reducers = combineReducers({
    data:data,
    status:status
});