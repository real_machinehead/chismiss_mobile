import * as actionTypes from './actiontypes';
import { combineReducers, bindActionCreators } from 'redux';
import { constants } from '../../../../constants';
import io from 'socket.io-client';
import {
    RTCPeerConnection,
    RTCMediaStream,
    RTCIceCandidate,
    RTCSessionDescription,
    RTCView,
    MediaStreamTrack,
    getUserMedia,
    mediaDevices
} from 'react-native-webrtc';

const initialstate=null;
const initialstatus=constants.status.INITIALIZE;

export const data=(state=initialstate,action)=>{
    //console.log('loogggsss',action.type);
    switch(action.type){
        case actionTypes.INITIALIZE:
            return action.payload;
            break;
        case actionTypes.UPDATE:
            return action.payload;
            break;
        default:
            return state;
    }
};

export const status=(state=initialstatus, action)=>{
    switch (action.type){
        case actionTypes.STATUS:
            return action.payload;
            //return [2,''];
            break;
        case actionTypes.RESET:
            return [3,''];
            break;
        case actionTypes.EMAIL:
            return [3,''];
            break;
        case actionTypes.PASSWORD:
            return [3,''];
            break;    
        default:
            return state;
    }
}

export const email=(state=initialstate, action)=>{
    switch(action.type){
        case actionTypes.EMAIL:
            return action.payload;
            break;
        default:
            return state;
    }
}

export const password=(state=initialstate,action)=>{
    switch(action.type){
        case actionTypes.PASSWORD:
            return action.payload;
            break;
        default:
            return state;
    }
}

export const accesstoken=(state=initialstate,action)=>{
    //console.log('accessstokenss',action.type)
    switch(action.type){
        case actionTypes.ACCESSTOKEN:
            //console.log('accessstokenss2345',action.payload)
            return action.payload;
            break;
        default:
            console.log('accessstokenss2345',state)
            return state;
    }
}

export const userid=(state=initialstate,action)=>{
    switch(action.type){
        case actionTypes.USERID:
            return action.payload;
            break;
        case actionTypes.ACCESSTOKEN:
            return action.payload; 
            break;
        default:
            return state;
    }
}

export const webRtcInfo=(state=initialstate,action)=>{
    switch(action.type){
        case actionTypes.WEBRTCONNECTION:
                //connection
                const {stunurl,turnurl,username,credentials}=action.payload;

                const configuration = { 
                    iceServers: 
                    [
                        { url:stunurl },
                        { urls:stunurl},
                        { url:turnurl,username:username,credential:credentials},
                        { urls:turnurl,username:username,credential:credentials },
                        { urls:[stunurl,turnurl],username:username,credentials:credentials}
                    ] 
                };
                const pc= new RTCPeerConnection(configuration);    
                console.log('webrtc',pc);
                return pc;
            break;
        default:
            return state;
    }
}

export const socketinfo=(state=initialstate, action)=>{
    switch(action.type){
        case actionTypes.SOCKETCONNECTION:
                
                var ipaddress=action.payload.ipaddress;
                var socket = io(ipaddress,{transports: ['websocket'],  pingTimeout: 85000,
                pingInterval: 85000});    

            
            return socket;  
        break;
        default:
            return state
    }
}
export const websocketinfo=(state=initialstate,action)=>{
    switch(action.type){
        case actionTypes.WEBSOCKETCONNECTION:
            const urls=action.payload.urls;

            const websocket=new WebSocket(urls);
            return websocket
        break;
        default:
            return state
    }
}


export const reducer = combineReducers({
    email:email,
    password:password,
    data:data,
    status:status,
    accesstoken:accesstoken,
    userid:userid,
    webRtcInfo:webRtcInfo,
    socketinfo:socketinfo,
    websocketinfo:websocketinfo
});
