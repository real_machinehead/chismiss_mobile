import * as api from './api';
import * as actionTypes from './actiontypes';
import {constants} from '../../../../constants'


export const reset = payload=>({
    type:actionTypes.RESET,
    payload:payload
});


export const update = payload=>({
    type:actionTypes.UPDATE,
    payload:payload
});

export const status = payload=>({
    type:actionTypes.STATUS,
    payload:payload
});


export const init = payload=>({
    type:actionTypes.INITIALIZE,
    payload:payload
});

export const emailChanged=payload=>({
    type:actionTypes.EMAIL,
    payload:payload
});

export const passwordChanged=payload=>({
    type:actionTypes.PASSWORD,
    payload:payload
});

export const accesstoken=payload=>({
    type:actionTypes.ACCESSTOKEN,
    payload:payload
});

export const userid = payload=>({
    type:actionTypes.USERID,
    payload:payload
});

export const websocketconnection = payload =>({
    type:actionTypes.WEBSOCKETCONNECTION,
    payload:payload
});

export const socketconnection = payload=>({
    type:actionTypes.SOCKETCONNECTION,
    payload:payload
});

export const webrtconnection = payload=>({
    type:actionTypes.WEBRTCONNECTION,
    payload:payload
});


export const onaccesstoken = payload=>
    dispatch =>{
        let objResponse={};
        dispatch(reset())
        dispatch(accesstoken(payload));
};

export const onuserid = payload=>
    dispatch =>{
        let objResponse={};
        dispatch(reset())
        dispatch(userid(payload));
};

export const fetchlogin = payload=>
    dispatch =>{
        let objResponse={};
        dispatch(reset())
        dispatch(status(constants.status.LOADING));

        //console.log('payloadsss',payload);

        api.get(payload)
        .then((response)=>response)
        .then((res)=>{
            //check the status
            console.log('login success',res);
            if(res.Status.IsSuccess){
                const webrtcpayload={
                    stunurl:'stun:chismischannel.live:3478',
                    turnurl:'turn:chismischannel.live:3478',
                    username:'chismis',
                    credentials:'sup3rpassw0rd',
                }

                //const socketpayload={ipaddress:'https://chismischannel.live:8888'}
                //const websocketpayload={urls:'wss://chismischannel.live:9999'}
                //console.log('loggsss1234',res.Data.Login[0].Username)
                dispatch(init(res.Data))
                dispatch(status([1,'success']));
                //dispatch(webrtconnection(webrtcpayload));
                //dispatch(websocketconnection(websocketpayload))
                //dispatch(socketconnection(socketpayload))
                dispatch(accesstoken(res.Data.Login[0].Token))
                dispatch(userid(res.Data.Login[0].UserID))
              
            }else{
                dispatch(status([0,res.Status.Message]));
            }

            //dispatch(acesstoken(res))
            //objResponse={...res}
        })
		.then(() => {
			//dispatch(status([1,'success']));
        })
        .catch((exception) => {
			dispatch(status([
				0,
				exception.message + '.'
			]));
			console.log('exception: ' + exception.message);
		});
    };