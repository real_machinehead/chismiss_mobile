import { combineReducers } from 'redux';
import { reducer as loginReducer } from './login/reducers';


export const reducers = combineReducers({
    login:loginReducer
});