export default{
    flatlist:{
        flex:1,
        flexDirection:'row',
        width:'100%',
        backgroundColor:'white',             
    },
    searchBoxContainer:{
        flexDirection:'row',
        width:'90%',
    }
}