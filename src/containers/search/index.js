import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform } from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import {GeneralContainers,GeneralContents,SearchBox} from '../../components';
import { bindActionCreators } from 'redux';
import * as chatActions from '../datas/chatlist/actions';
import styles from '../styles';
import searchStyles from './styles';

import Icon from 'react-native-vector-icons/Ionicons';


export default class Search extends Component{

    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:{
                flagno:1,
                message:'success',
                data:[
                    {
                        firstName:'Kpop',
                        lastName:'Fans',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    },
                    {
                        firstName:'Girls',
                        lastName:'Generation',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    },
                    {
                        firstName:'Black',
                        lastName:'Pink',
                        message:'Lorem Ipsum is simply dummy text of the printing and typesetting industry'
                    }
                ]
            },
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
        };
    }
    
    /*static navigationOptions ={
        title: 'CALLS',
        headerStyle:{
            backgroundColor: 'white'
        },
        headerTitleStyle:{
            alignSelf:'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign:'center'
        }
    }*/


    componentDidMount(){
        this._fetchData();
    }


    _fetchData=()=>{

    }

    _keyExtractor = (item, index) => item.firstName;

    render(){
        const data=this.state.data.data;
        return(
            <GeneralContainers>
                <GeneralContents>
                        <View style={searchStyles.searchBoxContainer}>
                            <SearchBox placeholder='Search'/>
                        </View>
                        <View style={searchStyles.flatlist}>
                            <FlatList
                                data={data}
                                keyExtractor={this._keyExtractor}
                                renderItem={({ item }) => (
                                    <ListItem
                                        roundAvatar
                                        title={`${item.firstName} ${item.lastName}`}
                                        subtitle={item.date}
                                        avatar={<Avatar
                                            small
                                            rounded
                                            title={`${item.lastName.charAt(0)}${item.firstName.charAt(0)}`}
                                            onPress={() => console.log("Works!")}
                                            activeOpacity={0.7}
                                        />}
                                    />
                                
                                )}
                            /> 
                        </View>
 
                </GeneralContents>

            </GeneralContainers>
        );
    }
}
/*function mapStateToProps(state){

    return{
        chats:state
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            chats:bindActionCreators(chatActions,dispatch),
        }
    }
}

export default connect(
    mapDispatchToProps,
    mapStateToProps
)(Chat)*/