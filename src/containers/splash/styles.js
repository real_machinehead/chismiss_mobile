export default{
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'column',
        backgroundColor:'green',
    },
    content:{
        height:'100%',
        backgroundColor: 'powderblue',
        justifyContent:'center',
        alignItems:'center',
    },
    logoSplash:{
        paddingBottom:70,
    },
    buttonSplash:{
        height:50,
        width:150,
    }
}