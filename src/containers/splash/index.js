import React, {Component, PureComponent } from 'react';
import { Text,View,Image } from 'react-native';
import { connect } from 'react-redux';
import * as splashActions from '../datas/splash/actions';
import { bindActionCreators } from 'redux';
import { Button } from '../../components/common/';
import {Spinner} from '../../components';
import styles from './styles';
import {AsyncStorage} from 'react-native';

import * as loginActions from '../security/datas/login/actions';
import * as profileActions from '../datas/profile/actions';
import * as contactActions from '../datas/contacts/actions';

//import { access } from 'fs';

export class Splash extends Component{
    
    /*static navigationOptions = {
        title: 'Splash',
        headerStyle: {
          backgroundColor: 'white'
        },
        headerTitleStyle :{
            alignSelf: 'center',
            width: '90%',
            textAlign: 'center',
        },
        headerLeft: null,

    }*/

    constructor(props){
        
        super(props);

        this.state={
            loading:false,
            data:[],
            page:1,
            seed:1,
            error:null,
            refresh: false,
            showForm:false,
            isLoading:true,
        }

    }

    componentDidMount(){

        AsyncStorage.getItem('usercredentials')
        .then((value)=>
            this._fetchData(value)
        )
        .then((value)=>this.props.navigation.navigate('BottomTab'))
        .catch(err=>this.props.navigation.navigate('Login'))
    }

    _fetchData=async(usercredentials)=>{

        const credentials= JSON.parse(usercredentials);
         payload={
            username:'',
            password:'',
            accesstoken:credentials.accesstoken,
            userid:credentials.userid,
            header:credentials.header
        }
    console.log('payloadcredentials',credentials);
    await this.props.actions.login.onaccesstoken(payload.accesstoken);
    await this.props.actions.login.onuserid(payload.userid);
    await this.props.actions.profile.fetchprofile(payload);
    await this.props.actions.contacts.fetchcontactlist(payload,{Token:payload.accesstoken});
    

        /*if(usercredentials){
            const payload={
                username:null,
                password:null,
                accesstoken:usercredentials.accesstoken,
                userid:usercredentials.userid,
                header:usercredentials.header
            }

            await this.props.actions.profile.fetchprofile(payload);
            await this.props.actions.contacts.fetchcontactlist(payload,{Token:payload.accesstoken});
            this.props.navigation.navigate('BottomTab')
        }else{
            this.props.navigation.navigate('Login')
        }*/
    }
    _onNavigation(){
        //this.props.navigation.navigate('Webrtc');
        this.props.navigation.navigate('Login')
        //this.props.navigation.navigate('BottomTab')
        //this.props.navigation.navigate('Profile')
    }
    render(){
        const data=[];
        return(
            <View styles={styles.container}>
                <View style={styles.content}>
                    <View style={styles.logoSplash}>
                        <Image source={require('../../image/logosketch.png')} style={{height:200,width:200}} />
                    </View>
                    <View style={styles.buttonSplash}>
                        
                        { /*<Spinner />
                        <Button onPress={() => this._onNavigation()}>
                            Navigate Here
                        </Button> */
                            this.state.isLoading ? <Spinner /> : null
                        }
                    </View>

                </View>
            </View>
        );
    }
}

function mapStateToProps(state){
    console.log('statessss',state)
    return{
        //login:state.security.login,
        profile:state.datareducer.profile,
        contacts:state.datareducer.contacts,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            login:bindActionCreators(loginActions,dispatch),
            profile:bindActionCreators(profileActions,dispatch),
            contacts:bindActionCreators(contactActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Splash)