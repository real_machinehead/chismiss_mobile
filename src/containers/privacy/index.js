import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform,Modal,TouchableHighlight,TouchableOpacity } from 'react-native'
import {  List , ListItem,Avatar } from "react-native-elements";
import { connect } from 'react-redux';
import {GeneralContainers,GeneralHeaders,GeneralContents,Messagebox,Spinner} from '../../components';
import { bindActionCreators } from 'redux';
//import * as chatActions from '../datas/chatlist/actions';
import privacyStyles from './styles';
import {IconButton} from '../../components/common';
import Icon from 'react-native-vector-icons/Ionicons';
import ActionButton from 'react-native-action-button';



class Privacy extends Component{

    constructor(props){
        super(props);

        this.state={
            loading:false,
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
            channelForm:false,
            contactForm:false,
            onlineStatusForm:false,
            messageBox:true,
            onlineStatus:null,

        };
    }
    
    /*static navigationOptions ={
        title: 'CALLS',
        headerStyle:{
            backgroundColor: 'white'
        },
        headerTitleStyle:{
            alignSelf:'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign:'center'
        }
    }*/


    componentDidMount(){
        this._fetchData();
    }


    _fetchData=()=>{

    }

    _keyExtractor = (item, index) => item.firstName;


    _closeContactForm=()=>{
        this.setState({showForm:false,contactForm:false})
    }

    _onStatusclick=()=>{
        console.log('status here');
        this.setState({onlineStatusForm:!this.state.onlineStatusForm})
  
    }

    _onAboutPress=()=>{
        this.props.aboutus()
    }

    _onTermsPress=()=>{
        this.props.terms()
    }
    _onPrivacyPress=()=>{
        this.props.privacy()
    }
    _onSettingsPress=()=>{
        this.props.settings()
    }
    _onLogoutPress=()=>{
        this.props.logout()
    }
    render(){

        return(
            <View>
                <Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque non arcu diam. 
                    Morbi vel est mollis, efficitur nibh id, pretium metus. Nunc ac fringilla magna, 
                    ac fermentum odio. Nulla euismod feugiat lectus, id porttitor leo semper a. 
                    Suspendisse in tempus enim, tristique varius arcu. 
                    Donec at massa eu ipsum scelerisque molestie. 
                    Mauris et nunc ac nibh egestas cursus eget nec urna. 
                    Integer tincidunt hendrerit luctus. Nulla facilisi. 
                    Quisque elementum nibh felis, rhoncus mattis elit iaculis a. 
                    Aliquam pulvinar maximus dui vitae rutrum. Ut vitae fringilla mauris, vitae faucibus odio. 
                    Donec auctor convallis leo at porta.

                    Phasellus commodo quam quam, vitae rutrum erat gravida at. 
                    Cras vehicula sollicitudin velit at laoreet. Ut gravida a felis sed gravida. 
                    Proin feugiat tortor tellus, quis porttitor erat placerat a. Nam arcu libero, 
                    laoreet vel euismod sit amet, tempor nec urna. Nam pulvinar neque ac quam pharetra, 
                    a dapibus leo imperdiet. Sed porttitor tortor felis. Cras eu egestas diam. 
                    Suspendisse rhoncus dictum elit, malesuada mollis tellus semper quis. Vivamus ex purus, 
                    tempor vel nisl sit amet, tempor auctor mi. Cras sed nibh a elit ultricies porta nec sit amet nulla. 
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat volutpat. 
                    Curabitur sit amet est in est tempor pretium a ullamcorper dui. Ut euismod dui viverra, 
                    gravida lacus sit amet, venenatis orci. Nam vulputate, sem eget ornare dictum, 
                    mauris neque vulputate lorem, at posuere eros nibh vel odio.

                    Suspendisse potenti. Aenean ut dapibus nulla, eget convallis enim. Nulla venenatis sollicitudin quam, 
                    vitae ultrices odio finibus eu. Donec est purus, interdum vitae nulla at, accumsan elementum velit. 
                    Nunc tempus ut ante quis faucibus. Pellentesque habitant morbi tristique senectus et netus et 
                    malesuada fames ac turpis egestas. Maecenas finibus maximus aliquam. Suspendisse tristique venenatis 
                    orci at commodo. Suspendisse vitae mauris est. Donec volutpat velit ac diam eleifend bibendum. 
                    Nunc consequat ante vel libero pellentesque viverra.

                    Morbi ornare placerat blandit. Cras fringilla interdum erat quis maximus. Vivamus dignissim cursus urna,
                    et porttitor lectus pulvinar in. Phasellus aliquam hendrerit arcu, ut vestibulum orci pretium et. 
                    Donec elementum malesuada metus pulvinar tincidunt. Fusce posuere dolor diam, 
                    vitae mattis orci dictum et. Sed interdum tempus ipsum, et hendrerit felis mollis in. 
                    Fusce in nibh rutrum, congue erat vel, hendrerit magna.

                    Ut commodo mauris cursus mauris porta, et egestas sem consectetur. Donec ac velit bibendum 
                    ligula hendrerit facilisis. Vivamus fringilla sollicitudin mi sit amet fermentum. 
                    Etiam neque massa, bibendum sed enim bibendum, semper finibus nisl. 
                    Fusce nunc sapien, imperdiet ut pharetra ac, volutpat at tellus. 
                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. 
                    Etiam porttitor sed ante eu aliquam. Nunc ac ullamcorper urna, at efficitur velit. 
                    Sed felis orci, condimentum eget magna ac, auctor rhoncus justo. Donec bibendum sagittis est 
                    nec eleifend. Vestibulum at maximus lectus. 
                    Mauris scelerisque convallis ipsum in condimentum. Vivamus nec mauris consectetur, 
                    commodo ipsum vitae, vulputate diam. Maecenas in leo sed nisl condimentum rutrum.                   
                </Text>
            </View>
        );
    }
}
function mapStateToProps(state){
    //console.log('logstates23467',state.datareducer);
    return{
        acesstoken:state.security.login.data.Login[0].Token,
        userid:state.security.login.data.Login[0].UserID,
        profile:state.datareducer.profile,
        contacts:state.datareducer.contacts,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            //contacts:bindActionCreators(contactActions,dispatch),
            //profile:bindActionCreators(profileActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Privacy)