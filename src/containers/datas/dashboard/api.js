import {fetchApi} from '../../../globals/api';
import * as endPoints from '../../../globals/endpoints';

export let get = payload=>fetchApi(endPoints.setups.dashboard.get(payload),payload, 'get');