import * as actionTypes from './actiontypes';
import { combineReducers } from 'redux';
import { constants } from '../../../constants';

const initialstate='';
const initialStatus = constants.status.LOADING;

export const data = (state=initialstate,action)=>{
    switch(action.type){
        case actionTypes.INITIALIZE:
            return action.payload;
            break;
        case actionTypes.UPDATE:
            return action.payload;
            break;
        default:
        return state;
    };
};


const status = (state =initialStatus,action)=>{
    switch (action.type){
        case actionTypes.STATUS:
            return action.payload;
            break;
        default:
            return state;
    }
}

export const reducers = combineReducers({
    data:data,
    status:status
})