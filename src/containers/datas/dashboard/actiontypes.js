export const INITIALIZE = 'dashboard/initialize';
export const UPDATE = 'dashboard/update';
export const STATUS ='dashboard/status';
export const RESET ='dashboard/reset'