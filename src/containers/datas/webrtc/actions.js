import * as api from './api';
import * as actiontypes from './actiontype';
import {constants} from '../../../constants';


export const reset = payload=>({
    type:actiontypes.RESET,
    payload:payload,
});

export const update = payload=>({
    type:actiontypes.UPDATE,
    payload:payload,
});


export const status = payload=>({
    type:actiontypes.STATUS,
    payload:payload,
});

export const init = payload=>({
    type:actiontypes.INIT,
    payload:payload,
});

/*export const createpc = payload=>{
    dispatch => {
        const socket=io.connect('wss://chismischannel.live:9999',{transports: ['websocket']});
        const configuration ={"iceServers": [{"url": "stun:stun.l.google.com:19302"}]};
        const pc = new RTCPeerConnection(configuration);

        pcPeers[socketId] = pc;

        pc.onicecandidate = function (event) {
            console.log('onicecandidate', event.candidate);
            if (event.candidate) {
              socket.emit('exchange', {'to': socketId, 'candidate': event.candidate });
            }
        };

        function createOffer() {
            pc.createOffer(function(desc) {
              console.log('createOffer', desc);
              pc.setLocalDescription(desc, function () {
                console.log('setLocalDescription', pc.localDescription);
                socket.emit('exchange', {'to': socketId, 'sdp': pc.localDescription });
              }, logError);
            }, logError);
          }

        pc.onnegotiationneeded = function () {
            console.log('onnegotiationneeded');
            if (isOffer) {
              createOffer();
            }
        }

        pc.oniceconnectionstatechange = function(event) {
            console.log('oniceconnectionstatechange', event.target.iceConnectionState);
            if (event.target.iceConnectionState === 'completed') {
              setTimeout(() => {
                getStats();
              }, 1000);
            }
            if (event.target.iceConnectionState === 'connected') {
              createDataChannel();
            }
        };   

        pc.onsignalingstatechange = function(event) {
            console.log('onsignalingstatechange', event.target.signalingState);
        };     
        
        pc.onaddstream = function (event) {
            console.log('onaddstream', event.stream);
            container.setState({info: 'One peer join!'});
        
            const remoteList = container.state.remoteList;
            remoteList[socketId] = event.stream.toURL();
            container.setState({ remoteList: remoteList });
        };  

        pc.onremovestream = function (event) {
            console.log('onremovestream', event.stream);
        };  

        pc.addStream(localStream);
        function createDataChannel() {
          if (pc.textDataChannel) {
            return;
          } 

          const dataChannel = pc.createDataChannel("text");

          dataChannel.onerror = function (error) {
            console.log("dataChannel.onerror", error);
          };
          dataChannel.onmessage = function (event) {
            console.log("dataChannel.onmessage:", event.data);
            container.receiveTextData({user: socketId, message: event.data});
          };
      
          dataChannel.onopen = function () {
            console.log('dataChannel.onopen');
            container.setState({textRoomConnected: true});
          };
      
          dataChannel.onclose = function () {
            console.log("dataChannel.onclose");
          };
      
          pc.textDataChannel = dataChannel;
        }
        return pc;
    }         
                      
};*/