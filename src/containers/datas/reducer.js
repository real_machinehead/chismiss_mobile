import { combineReducers } from 'redux';
import {reducers as dashboardReducer } from './dashboard/reducers';
import {reducers as splashReducer } from './splash/reducers';
//import {reducers as callReducer } from './calls/reducers';
//import {reducers as chatReducer } from './chatlist/reducers';
import {reducers as profileReducer } from './profile/reducers';
import {reducers as contactReducer } from './contacts/reducers';
import {reducers as chatlistReducer } from './chatlist/reducers';
import {reducers as chatReducer } from './chats/reducers';
import {reducers as routeReducer } from './routes/reducers';
//import {reducers as contactsReducer } from './contacts/reducers';


export const reducers = combineReducers({
    dashboard:dashboardReducer,
    splash:splashReducer,
    chats:chatReducer,
    profile:profileReducer,
    contacts:contactReducer,
    chatlist:chatlistReducer,
    routes:routeReducer,
});