import * as actiontype from './actiontype';
import { combineReducers } from 'redux';
import { constants } from '../../../constants';

const initialstate ='';
const initialstatus = constants.status.LOADING;


export const data =(state=initialstate, action)=>{
    switch(action.type){
        case actiontype.INITIALIZE:
            return action.payload;
            break;
        case actiontype.UPDATE:
            return action.payload;
            break;
        default:
            return state;
    }
}

export const status = (state=initialstatus,action)=>{
    switch(action.type){
        case actiontype.status:
            return action.payload
    }
}


export const reducers = combineReducers({
    data:data,
    //status:status
});