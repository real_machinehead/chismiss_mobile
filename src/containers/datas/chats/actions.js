import * as api from './api';
import * as actiontypes from './actiontypes';
import {constants} from '../../../constants';
import { delstatus } from './reducers';

export const reset = payload=>({
    type:actiontypes.RESET,
    payload:payload
});

export const update = payload=>({
    type:actiontypes.UPDATE,
    payload:payload
});

export const status = payload=>({
    type:actiontypes.STATUS,
    payload:payload
});

export const init = payload=>({
    type:actiontypes.INITIALIZE,
    payload:payload
});

export const userlist=payload=>({
    type:actiontypes.USERLIST,
    payload:payload
});

export const channelid=payload=>({
    type:actiontypes.CHANNELID,
    payload:payload
});

export const channelmessage=payload=>({
    type:actiontypes.CHANNELMESSAGE,
    payload:payload
});

export const sendmessage=payload=>({
    type:actiontypes.SENDMESSAGE,
    payload:payload
});

export const channelupdatemessage=payload=>({
    type:actiontypes.CHANNELUPDATEMESSAGE,
    payload:payload
});

export const sendpicture=payload=>({
    type:actiontypes.SENDPICTURES,
    payload:payload
});

export const socketconnection=payload=>({
    type:actiontypes.SOCKETCONNECT,
    payload:payload
});

export const receivemessage=payload=>({
    type:actiontypes.RECEIVEMESSAGE,
    payload:payload
});

export const updateuserlist =payload=>({
    type:actiontypes.UPDATEUSERLIST,
    payload:payload
});

export const channelreset=payload=>({
    type:actiontypes.CHANNELRESET,
    payload:payload
});

export const updatereceivemessages=payload=>({
    type:actiontypes.UPDATERECIEVEMESSAGES,
    payload:payload
});

export const chatmessagesreset=payload=>({
    type:actiontypes.CHATMESSAGESRESET,
    payload:payload
});

export const chatupdatemessages=payload=>({
    type:actiontypes.CHATUPDATEMESSAGES,
    payload:payload
});

export const totalitems=payload=>({
    type:actiontypes.CALCULATEITEMS,
    payload:payload
});

export const delstatusreset=payload=>({
    type:actiontypes.DELSTATUSRESET,
    payload:payload
});

export const delmessages=payload=>({
    type:actiontypes.DELMESSAGES,
    payload:payload
});

export const initdelmessages=payload=>({
    type:actiontypes.INITDELMESSAGES,
    payload:payload
});

export const groupreset=payload=>({
    type:actiontypes.GROUPRESET,
    payload:payload
});

export const contactgroup=payload=>({
    type:actiontypes.CONTACTGROUP,
    payload:payload
});

export const errmessages=payload=>({
    type:actiontypes.ERRMESSAGES,
    payload:payload
});

export const updatecontactstatus=payload=>({
    type:actiontypes.UPDATECONTACTSTATUS,
    payload:payload
});

export const updatetotalitems=payload=>({
    type:actiontypes.UPDATETOTALITEMS,
    payload:payload
});

export const onupdatecontactstatus = (payload) => 
    dispatch =>{
        //dispatch(chatmessagesreset());
        //dispatch(groupreset())
        dispatch(updatecontactstatus(payload));
};

export const onupdatechatmessages = (payload) => 
    dispatch =>{
        //dispatch(chatmessagesreset());
        dispatch(chatupdatemessages(payload));
};
export const onchatmessagesreset= (payload) => 
    dispatch =>{
        dispatch(chatmessagesreset());
};
export const onupdateuserlist = (payload) => 
    dispatch =>{
        dispatch(updateuserlist(payload));
    };

export const onSocketconnection = (payload) => 
    dispatch =>{
        let objResponse={};

        dispatch(socketconnection(payload));
    };

export const onreceivemessage = (payload) => 
    dispatch =>{
        let objResponse={};
        console.log('received message',payload);
        //dispatch(channelreset())
        dispatch(receivemessage(payload));
};

export const onupdaterecievemessage = (payload) => 
    dispatch =>{
        let objResponse={};
        dispatch(receivemessage(payload));
};


export const onresetdata = () => 
    dispatch =>{
        let objResponse={};
        //console.log('resetdata',payload);
        //dispatch(reset())
        dispatch(reset());
};

export const oninitdelemessages = () => 
    dispatch =>{
        let objResponse={};
        //console.log('resetdata',payload);
        //dispatch(reset())
        dispatch(initdelmessages());
};

export const onupdatetotalitems = (payload) => 
    dispatch =>{
        let objResponse={};
        //console.log('resetdata',payload);
        //dispatch(reset())
        dispatch(updatetotalitems(payload));
};

export const fetchMessages = (payload,token) => 
    dispatch =>{
        let objResponse={};

        dispatch(status(constants.status.LOADING))
        dispatch(reset())
        console.log('payloads123455',payload);
        api.getmessages(payload,token)
        .then((response)=>response)
        .then((res)=>{
            //console.log('loggsssss',res);
            objResponse={...res}
            if(res.Status.IsSuccess){
                console.log('successss',res.Data.ChannelMessages[0]);
                //console.log('loggsssss234',res.Data.Contacts);
                dispatch(userlist(res.Data.UserList))
                dispatch(channelid(res.Data.ChannelID))
                dispatch(channelmessage(res.Data.ChannelMessages))
                dispatch(totalitems(res.Data.ChannelMessages))
                dispatch(status([1,'success']));

            }else{
                dispatch(status([0,res.Status.Message]));
            }
        })
        .then(()=>{

        })
        .catch((exception)=>{
            dispatch(status([
                0,exception.message
            ]));
        });
    };

    
    export const updateMessages = (payload,token) => 
    dispatch =>{
        let objResponse={};

        dispatch(channelupdatemessage(payload))
    };

    export const updatePictures=(payload,token)=>
    dispatch=>{
        dispatch(sendpicture(payload))
    }

    export const sendMessages = (payload,token) => 
    dispatch =>{
        let objResponse={};

        //dispatch(status(constants.status.LOADING))
        api.sendmessages(payload,token)
        .then((response)=>response)
        .then((res)=>{
            console.log('loggsssss',res);
            objResponse={...res}
            if(res.Status.IsSuccess){
                dispatch(status([1,'success234']));

            }else{
                dispatch(status([0,res.Status.Message]));
            }
        })
        .then(()=>{

        })
        .catch((exception)=>{
            dispatch(status([
                0,exception.message
            ]));
        });
    };

    export const sendPictures = (payload,token,userId) => 
    dispatch =>{
        let objResponse={};

        console.log('payload1234',payload);
        dispatch(status(constants.status.LOADING))
        api.uploadfile(payload,token)
        .then((response)=>response)
        .then((res)=>{
            console.log('loggsssss23456',res);
            let attachpayload= new FormData();
            attachpayload.append('filename', res.result[1]);
            attachpayload.append('locationreference', res.result[0]);
            attachpayload.append('contactuserid', userId);
            
            objResponse={...res}
            api.sendfiletodb(attachpayload,token)
            .then((response)=> response)
            .then((res)=>{
                console.log('loggsssss2345',res);
                dispatch(status([1,'success']));

            })
        })
        .then(()=>{

        })
        .catch((exception)=>{
            dispatch(status([
                0,exception.message
            ]));
        });
    };

    export const deleteMessages = (payload,token,userId) => 
    dispatch =>{
        let objResponse={};

        console.log('payload1234',payload);
        dispatch(delstatusreset())
        
        setTimeout(() => {
           dispatch(errmessages([3,'Under Construction']))
        }, 2000);

        /*api.uploadfile(payload,token)
        .then((response)=>response)
        .then((res)=>{
            console.log('loggsssss23456',res);
            let attachpayload= new FormData();
            attachpayload.append('filename', res.result[1]);
            attachpayload.append('locationreference', res.result[0]);
            attachpayload.append('contactuserid', userId);
            
            objResponse={...res}
            api.sendfiletodb(attachpayload,token)
            .then((response)=> response)
            .then((res)=>{
                console.log('loggsssss2345',res);
                dispatch(status([1,'success']));

            })
        })
        .then(()=>{

        })
        .catch((exception)=>{
            dispatch(status([
                0,exception.message
            ]));
        });*/
    };   

    export const fetchgroups = (payload) => 
    dispatch =>{
        let objResponse={};

        dispatch(groupreset())
        dispatch(contactgroup(payload))
    };      