import * as actiontype from './actiontypes';
import { combineReducers } from 'redux';
import { constants } from '../../../constants';
import io from 'socket.io-client';
//import SimpleWebRTC from 'simplewebrtc';


const initialstate ='';
const initialstatus = constants.status.LOADING;


export const data =(state=initialstate, action)=>{
    switch(action.type){
        case actiontype.INITIALIZE:
            return action.payload;
            break;
        case actiontype.UPDATE:
            return action.payload;
            break;
        case actiontype.RESET:
            return [];    
            break;
        default:
            return state;
    }
}

export const status =(state = initialstatus, action)=>{
    switch (action.type){
        case actiontype.STATUS:
            return action.payload;
            break;
        case actiontype.CONTACTLISTRESET:
            return [3,''];
            break;
        case actiontype.RESET:
            return [3,''];
            break;
        default:
            return state;
    }
}

export const userlist=(state=initialstate, action)=>{
    switch(action.type){
        case actiontype.RESET:
            return []
            break;
        case actiontype.USERLIST:

            const usrlist=[];

            for(x=0;x<action.payload.length;x++){
                usrlist.push(
                    {
                        Username:action.payload[x].Username,
                        Callsign:action.payload[x].Callsign,
                        UserID:action.payload[x].UserID,
                        PhotoLocationReference:action.payload[x].PhotoLocationReference,
                        PhotoFilename:action.payload[x].PhotoFilename,
                        Status:'offline'
                    }
                )
            }
            //return action.payload;
            return usrlist;
            break;
        case actiontype.UPDATEUSERLIST:
            var currentState=state;
            const {onlineUser}=action.payload;
            
            for(x=0;x<onlineUser.length;x++){
                for(y=0;y<currentState.length;y++){
                    if(onlineUser[x].UserID=currentState[y].UserID){
                        currentState[y].Status='online'
                    }
                }
            }

            console.log('current state',currentState)
            return currentState;
        default:
            return state;
    }
}

export const channelid=(state=initialstate, action)=>{
    switch(action.type){
        case actiontype.RESET:
            return [];
            break;
        case actiontype.CHANNELID:
            return action.payload;
            break;
        default:
            return state;
    }
}

export const chatmessagess=(state=initialstate, action)=>{
    switch(action.type){
        case actiontype.CHATMESSAGESRESET:
        console.log('resettttttt')
            return '';
            break;
        case actiontype.CHATUPDATEMESSAGES:
            //console.log('chatmessagess',action.payload);
            return action.payload;
            break;
        default:
            return state;
    }
}


export const channelmessage=(state=initialstate, action)=>{

    switch(action.type){
        case actiontype.RESET:
            return [];
            break;
        case actiontype.CHANNELMESSAGE:
            
            var payload=action.payload;
            var dataMsg=[]
            for(x=0;x<payload.length;x++){
                var DayPosted=payload[x].DayPosted;
                var Messages=payload[x].Messages;
                var channelmessage=[];
                let urlparent = 'https://chismis.live/api';

                for(y=0;y<Messages.length;y++){
                    
                    var splitmessage=Messages[y].MessageText.split('橄欖');
                    var DatePosted = Messages[y].DatePosted;
                    var UserID = Messages[y].UserID;
                    var TimePosted = Messages[y].TimePosted;
                    var MessageID = Messages[y].MessageID;
                    var LocationReference = Messages[y].LocationReference ? urlparent+Messages[y].LocationReference : null;
                    
                      for(z=0;z<splitmessage.length;z++){
                        var msgobj={
                            DatePosted:DatePosted,
                            MessageText:splitmessage[z],
                            UserID:UserID,
                            TimePosted:TimePosted,
                            MessageID:MessageID,
                            LocationReference:LocationReference
                        }

                        channelmessage.push(msgobj)

                    }
                    
                    
                }
                payload[x].Messages=channelmessage;
                dataMsg[x]={data:channelmessage,DayPosted:DayPosted}
            }

            console.log('payloads23545',dataMsg)
            return dataMsg;

            break;
        case actiontype.CHANNELUPDATEMESSAGE:
                console.log('payloads23',action.payload.userId)
                let lastIndex=state.length-1;
                let prevMsg=state;
      
                if(lastIndex>=0){
                    let msgData={
                        DatePosted:action.payload.datePosted,
                        MessageText:action.payload.message,
                        UserID:action.payload.userId,
                        TimePosted:action.payload.timePosted,
                        MessageID:null,
                        imgsource:null,   
                    }   
                    prevMsg[lastIndex].data.push(msgData)
                    prevMsg[lastIndex].DayPosted=action.payload.dayPosted
                }else{

                    prevMsg.push({
                        data:[{
                            DatePosted:action.payload.datePosted,
                            MessageText:action.payload.message,
                            UserID:action.payload.userId,
                            TimePosted:action.payload.timePosted,
                            MessageID:null,
                            imgsource:null
                        }],DayPosted:action.payload.dayPosted
                    });

                }

                console.log('Update Messages',prevMsg);

            return prevMsg;
            break;
        case actiontype.RECEIVEMESSAGE:
            //let msgLastIndex=state.length-1;
            let msgLastIndex=action.payload.oldmessage.length-1;
            let lastMsg=action.payload.oldmessage;
            console.log('action3456',action.payload)

            let objMsg={
                DatePosted:action.payload.DatePosted,
                MessageText:action.payload.newmessages.message,
                UserID:action.payload.newmessages.nickid,
                TimePosted:action.payload.TimePosted,
                MessageID:null,
                imgsource:null,                 
            }

            //console.log('received messages reducer',action.payload);
            if(msgLastIndex>=0){
                lastMsg[msgLastIndex].data.push(objMsg)
            }else{

                lastMsg.push({
                    data:[{
                        DatePosted:action.payload.DatePosted,
                        MessageText:action.payload.newmessages.message,
                        UserID:action.payload.newmessages.nickid,
                        TimePosted:action.payload.TimePosted,
                        MessageID:null,
                        imgsource:null,  
                    }],DayPosted:action.payload.DayPosted
                });
            }

            console.log('message345667',lastMsg)
            return lastMsg;
            break;

        case actiontype.SENDPICTURES:

                const lastIndex_=state.length-1;
                const prevMsg_=state;
                const picpayload=action.payload;
                
                console.log('picpayload',action.payload);
                const msgData_={
                    DatePosted:action.payload.DatePosted,
                    MessageText:'',
                    UserID:picpayload.userId,
                    TimePosted:action.payload.TimePosted,
                    MessageID:null,      
                    LocationReference:picpayload.image           
                }
                
                if(lastIndex_>=0){
                    console.log('aasssss',lastIndex_);
                    prevMsg_[lastIndex_].data.push(msgData_);
                }else{
                    prevMsg_[0].data.push(msgData_);
                }

                return prevMsg_;

                //return action.payload;
            break;
        case actiontype.CHANNELRESET:
            return[];
            break;
        default:
            return state;
    }
}

export const socketinfo=(state=initialstate, action)=>{
    switch(action.type){
        case actiontype.SOCKETCONNECT:
                var ipaddress=action.payload.ipaddress;
                var socket = io(ipaddress,{transports: ['websocket'],  pingTimeout: 30000,
                pingInterval: 30000});     
            return socket;  
        break;
        default:
            return state
    }
}


export const webrtcInfo=(state=initialstate,action)=>{
    switch(action.type){
        case actiontype.NoVideoAudioWebRTC:
                /*const parameters = {
                    media: { 'video': false, 'audio': false },
                    receiveMedia: { offerToReceiveAudio: 0, offerToReceiveVideo: 0 },
                    enableDataChannels: true,
                    url: "https://chismischannel.live:8888",
                    socketio: {
                        forceNew: true
                    },
            
                    nickid: params.nickid || 0,
                    nick: params.nick || 'User A',
                    debug: params.debug || false
                }
            
                const webrtc = new SimpleWebRTC(parameters);*/
        return webrtc;
                /*const mediaParams = {
                    video: (params.video !== "boolean") ? [{ sourceId: params.video }] : params.video,
                    audio: (params.audio !== "boolean") ? [{ sourceId: params.audio }] : params.audio
                }
    
            const parameters = {
                autoRequestMedia: true,
                media: mediaParams,
                url: "https://chismischannel.live:8888",
                socketio: {
                    forceNew: true
                },
                autoRemoveVideos: params.autoRemoveVideos || true,
                localVideoEl: params.localVideoEl || 'localVideo',
                remoteVideosEl: params.remoteVideosEl || 'remoteVideos',
                detectSpeakingEvents: params.detectSpeakingEvents || false,
                nickid: params.nickid || "0",
                nick: params.nick || "User A",
                debug: params.debug || false,
            }
        
        
            const webrtc = new SimpleWebRTC(parameters);

            return webrtc*/
            break;
        case actiontype.YesVideoAudioWebRTC:
            
        
            return webrtc
            break;
        default:
            return state
    }
}

export const totalitems=(state=initialstate,action)=>{   
    switch(action.type){
        case actiontype.RESET:
            return 0;
            break;
        case actiontype.CALCULATEITEMS:
                //console.log('loggsss1234',action.payload);
                var lengths = action.payload.map(function(word){
                    console.log('loggsss1234',word)
                    return word.Messages.length
                });
                
                let totallenght=0;
                for(x=0;x<lengths.length;x++){
                    console.log('lengthesss',lengths[x]);
                    totallenght=totallenght+lengths[x];
                }       
                
                console.log('loggsss1234',lengths);
                return totallenght
            
        break;
        case actiontype.UPDATETOTALITEMS:
        
            return action.payload + 1; 
        default:
            return state;
    }
}

export const delstatus=(state=initialstate,action)=>{   
    switch(action.type){
        case actiontype.DELSTATUSRESET:

            return [2,''];
            break;
        case actiontype.DELMESSAGES:
            
            return [1,'SUCCESS']
        break;
        case actiontype.ERRMESSAGES:
            return action.payload
        break;
        case actiontype.INITDELMESSAGES:
            return [0,'']
        break;
        default:
            return state;
    }
}

export const chatgroups=(state=initialstate,action)=>{   
    //console.log('contacts23456',action)
    switch(action.type){
        case actiontype.GROUPRESET:

            return [];
        break;
        case actiontype.CONTACTGROUP:
            
            let contactgroup=[];
            let contactpayload=[]
            for(x=0;x<action.payload.length;x++){
                contactpayload[x]={Callsign:action.payload[x].Callsign,UserID:action.payload[x].UserID,status:[3,'']}
            }
            contactgroup=[{'title':'contacts','data':contactpayload}];
            //console.log('contacts23456789',contactgroup);
            return contactgroup;
        break;
        case actiontype.UPDATECONTACTSTATUS:
            let prevpayload=action.payload.prevpayload[0].data;
            let newpayload=action.payload.newpayload;
            for(x=0;x<prevpayload.length;x++){
                if(prevpayload[x].Callsign==newpayload.Callsign){
                    prevpayload[x]=newpayload
                }
                
            }
            action.payload.prevpayload[0].data=prevpayload
            console.log('UPDATECONTACTSTATUS',action.payload.prevpayload);
            return action.payload.prevpayload;
        break
        default:    
            return state;
    }
}


export const reducers = combineReducers({
    data:data,
    userlist:userlist,
    channelid:channelid,
    channelmessage:channelmessage,
    status:status,
    delstatus:delstatus,
    socketinfo:socketinfo,
    webrtcInfo:webrtcInfo,
    chatmessagess:chatmessagess,
    totalitems:totalitems,
    chatgroups:chatgroups
});