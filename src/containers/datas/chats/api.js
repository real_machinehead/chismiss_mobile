import {fetchApi} from '../../../globals/api';
import * as endPoints from '../../../globals/endpoints';


export let getmessages = (payload,header)=>fetchApi(endPoints.chats.getMessagesFromDb.get(payload),payload,'get',header);
export let sendmessages = (payload,header)=>fetchApi(endPoints.chats.sendMessageToDb.get(payload),payload,'post',header);
export let uploadfile = (payload,header)=>fetchApi(endPoints.chats.attachementUpload.get(payload),payload,'post',header);
export let sendfiletodb=(payload,header)=>fetchApi(endPoints.chats.sendAttachmentToDb.get(payload),payload,'post',header);
//export let deletemessage
//export let search = (payload,header)=>fetchApi(endPoints.search.userSearch.get(payload),payload,'get',header);