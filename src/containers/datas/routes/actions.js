
import * as actiontypes from './actiontypes';

export const update = payload=>({
    type:actiontypes.UPDATE,
    payload:payload
});


export const init = payload=>({
    type:actiontypes.INITIALIZE,
    payload:payload
});

export const onupdateroutes = payload => 
    dispatch =>{
        console.log('routesaction',payload)
        dispatch(init(payload.routes))
        dispatch(update(payload.routes))
       
};


