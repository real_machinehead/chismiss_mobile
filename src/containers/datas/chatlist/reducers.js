import * as actiontype from './actiontypes';
import { combineReducers } from 'redux';
import { constants } from '../../../constants';

const initialstate ='';
const initialstatus = constants.status.LOADING;


export const data =(state=initialstate, action)=>{
    switch(action.type){
        case actiontype.RESET:
            let initdata=[
                {title:'CONTACT LIST',data:[]},
                {title:'CHANNEL LIST',data:[]}
            ]
            return initdata;
            break;
        case actiontype.USERCONTACTLIST:
            let usrcontactata=state;
            usrcontactata[0].data=(action.payload)
            return usrcontactata;
            break;
        case actiontype.USERCHANNELLIST:
            let usrchanneldata=state;
            usrchanneldata[1].data=(action.payload)
            return usrchanneldata;
            break;
        default:
            return state;
    }
}


export const status =(state = initialstatus, action)=>{
    switch (action.type){
        case actiontype.STATUS:
            return action.payload;
            break;
        case actiontype.CONTACTLISTRESET:
            return [3,''];
            break;
        case actiontype.RESET:
            return [3,''];
            break;
        default:
            return state;
    }
}


export const reducers = combineReducers({
    data:data,
    status:status
});