export const INITIALIZE = 'chatlist/initialize';
export const UPDATE ='chatlist/initialize';
export const STATUS ='chatlist/status';
export const RESET ='chatlist/reset';
export const USERCONTACTLIST='chatlist/usercontactlist';
export const USERCHANNELLIST='chatlist/userchannellist';
export const CHANNELMESSAGES='chatlist/channelmessages';
export const USERMESSAGES='chatlist/usermessages'