import * as api from './api';
import * as actiontypes from './actiontypes';
import {constants} from '../../../constants';

export const reset = payload=>({
    type:actiontypes.RESET,
    payload:payload
});

export const update = payload=>({
    type:actiontypes.UPDATE,
    payload:payload
});

export const status = payload=>({
    type:actiontypes.STATUS,
    payload:payload
});

export const init = payload=>({
    type:actiontypes.INITIALIZE,
    payload:payload
});

export const usercontactlist = payload=>({
    type:actiontypes.USERCONTACTLIST,
    payload:payload
});

export const userchannellist = payload=>({
    type:actiontypes.USERCHANNELLIST,
    payload:payload
});


export const fetchdatacontact = (payload,token) => 
    dispatch =>{
        let objResponse={};

        dispatch(status(constants.status.LOADING))
        dispatch(reset())
        console.log('payloads123455',payload.header);
        api.fetchdatacontact(payload,token)
        .then((response)=>response)
        .then((res)=>{
            
            objResponse={...res}
            if(res.Status.IsSuccess){
                console.log('loggsssss',res);
                //console.log('successss',res.Data.ChannelMessages[0]);
                //console.log('loggsssss234',res.Data.Contacts);
                dispatch(usercontactlist(res.Data.Contacts))
                dispatch(userchannellist(res.Data.Channels))
                dispatch(status([1,'success']));
            }else{
                dispatch(status([0,res.Status.Message]));
            }
        })
        .then(()=>{

        })
        .catch((exception)=>{
            dispatch(status([
                0,exception.message
            ]));
        });
    };


/*export const fetchdatacontact = payload =>
    dispatch=>{
        let objResponse={};

        dispatch(status(constants.status.LOADING));
        dispatch(reset())

        api.fetchdatacontact(payload)
        .then((response)=>response.json())
        .then((res)=>{
            if(res.Status.IsSuccess){
                dispatch(usercontactlist(res.data.Contacts))
                dispatch(userchannellist(res.data.Channels))
                dispatch(status([1,'success']));
            }else{
                dispatch(contactstatus([0,res.Status.Message]));
            }           

        })
        .then(()=>{
            //dispatch(contactstatus([0,res.Status.Message]));
        })
        .catch((exception)=>{
            dispatch(status([
                0,
                exception.message
            ]));    
        });
    };*/