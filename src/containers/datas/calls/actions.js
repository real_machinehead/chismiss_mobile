import * as api from './api';
import * as actiontypes from './actiontypes';
import {constant} from '../../../constants';

export const reset = payload=>({
    type:actiontypes.RESET,
    payload:payload
});

export const update = payload=>({
    type:actiontypes.UPDATE,
    payload:payload
});

export const status = payload=>({
    type:actiontypes.STATUS,
    payload:payload
});

export const init = payload=>({
    type:actiontypes.INITIALIZE,
    payload:payload
});

export const get = payload =>
    dispatch=>{
        let objResponse={};

        dispatch(status(constants.status.LOADING));
        dispatch(reset())

        api.get(payload)
        .then((response)=>response.json())
        .then((res)=>{
            dispatch(init(res.data))
            objResponse={...res}
        })
        .then(()=>{
            dispatch(status([
                objResponse.flagno || 0,
                objResponse.message || constants.ERROR.SERVER
            ]));
        })
        .catch((exception)=>{
            dispatch(status([
                0,
                exception.message
            ]));    
        });
    };