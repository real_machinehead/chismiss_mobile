import {fetchApi} from '../../../globals/api';
import * as endPoints from '../../../globals/endpoints';


//export let post = payload=>fetchApi(endPoints.setups.contacts.get(payload),payload,'post');

export let search = (payload,header)=>fetchApi(endPoints.search.userSearch.get(payload),payload,'get',header);
export let addcontact = (payload,token)=>fetchApi(endPoints.setups.contacts.post(payload),payload,'post',token);
export let contactlist =(payload,token)=>fetchApi(endPoints.reports.contactlist.get(payload),payload,'get',token);