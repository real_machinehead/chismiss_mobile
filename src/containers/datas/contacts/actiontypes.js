export const INITIALIZE = 'contacts/initialize';
export const UPDATE ='contacts/initialize';
export const STATUS ='contacts/status';
export const RESET ='contacts/reset';
export const CONTACTSEARCH='contacts/contactsearch';
export const CONTACTLIST = 'contacts/contactlist';
export const CONTACTLISTUPDATE='contacts/contactlistupdate';
export const CONTACTLISTRESET ='contacts/contactlistreset';
export const CONTACTSTATUS = 'contacts/contactstatus';
export const CONTACTSTATUSRESET ='contact/contactstatusreset';
export const USERCONTACTLIST ='contact/usercontactlist';
export const USERCONTACTLISTRESET ='contact/usercontactlistreset';
export const ONLINEUSERS ='contact/onlineusers';
export const NEWMESSAGE='contact/newmessage';
export const READMESSAGE='contact/readmessage';




