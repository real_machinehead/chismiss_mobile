import * as api from './api';
import * as actiontypes from './actiontypes';
import {constants} from '../../../constants';

export const reset = payload=>({
    type:actiontypes.RESET,
    payload:payload
});

export const update = payload=>({
    type:actiontypes.UPDATE,
    payload:payload
});

export const status = payload=>({
    type:actiontypes.STATUS,
    payload:payload
});

export const init = payload=>({
    type:actiontypes.INITIALIZE,
    payload:payload
});

export const contactchanged = payload=>({
    type:actiontypes.CONTACTSEARCH,
    payload:payload
});

export const contactlist = payload=>({
    type:actiontypes.CONTACTLIST,
    payload:payload
});

export const contactlistupdate = payload=>({
    type:actiontypes.CONTACTLISTUPDATE,
    payload:payload
});

export const contactlistreset = payload=>({
    type:actiontypes.CONTACTLISTRESET,
    payload:payload
});

export const contactstatus =payload=>({
    type:actiontypes.CONTACTSTATUS,
    payload:payload
});

export const constactstatusreset=payload=>({
    type:actiontypes.CONTACTSTATUSRESET,
    payload:payload
});

export const usercontactlist=payload=>({
    type:actiontypes.USERCONTACTLIST,
    payload:payload
});

export const usercontactlistreset=payload=>({
    type:actiontypes.USERCONTACTLISTRESET,
    payload:payload
});

export const onlineusers=payload=>({
    type:actiontypes.ONLINEUSERS,
    payload:payload
});

export const newmessage=payload=>({
    type:actiontypes.NEWMESSAGE,
    payload:payload
});

export const readmessage=payload=>({
    type:actiontypes.READMESSAGE,
    payload:payload
});

export const onnewmessage = payload => 
    dispatch =>{
        let objResponse={};
        dispatch(usercontactlistreset())
        dispatch(newmessage(payload))
    };

export const onreadmessage = payload => 
    dispatch =>{
        let objResponse={};
        dispatch(usercontactlistreset())
        dispatch(readmessage(payload))
    };    

export const onlineusersupdate = payload => 
    dispatch =>{
        let objResponse={};
        dispatch(usercontactlistreset())
        dispatch(onlineusers(payload))
    };


export const search = payload => 
    dispatch =>{
        let objResponse={};

        dispatch(status(constants.status.LOADING))
        dispatch(reset())

        api.search(payload,payload.header)
        .then((response)=>response)
        .then((res)=>{
            //console.log('loggsssss',res);
            objResponse={...res}

            if(res.Status.IsSuccess){

                dispatch(init(res.Data))
                dispatch(status([1,'success']));

            }else{
                dispatch(status([0,res.Status.Message]));
            }

        })
        .then(()=>{
            /*dispatch(status([
                objResponse.flagno  || 0,
                objResponse.message || constants.status.ERROR[1]
            ]));*/
        })
        .catch((exception)=>{
            dispatch(status([
                0,exception.message
            ]));
        });
    };

    export const addcontacts = (payload,token) => 
    dispatch =>{
        let objResponse={};

        dispatch(contactstatus(constants.status.LOADING))
        //dispatch(constactstatusreset())
        //console.log('payloads',payload);
        api.addcontact(payload,token)
        .then((response)=>response)
        .then((res)=>{
            //console.log('loggsssss',res);
            objResponse={...res}
            if(res.Status.IsSuccess){

                //console.log('loggsssss',res.Data);
                dispatch(init(res.Data))

                dispatch(contactstatus([1,'success']));

            }else{
                dispatch(contactstatus([0,res.Status.Message]));
            }
        })
        .then(()=>{

        })
        .catch((exception)=>{
            dispatch(status([
                0,exception.message
            ]));
        });
    };

    export const fetchcontactlist = (payload,token) => 
    dispatch =>{
        let objResponse={};

        dispatch(status(constants.status.LOADING))
        dispatch(usercontactlistreset())
        //console.log('payloads123455',payload);
        api.contactlist(payload,token)
        .then((response)=>response)
        .then((res)=>{
            //console.log('loggsssss',res);
            objResponse={...res}
            if(res.Status.IsSuccess){
            
                console.log('loggsssss23463',res.Data.Contacts);
                dispatch(usercontactlist(res.Data.Contacts))

                dispatch(status([1,'success']));

            }else{
                dispatch(contactstatus([0,res.Status.Message]));
            }
        })
        .then(()=>{

        })
        .catch((exception)=>{
            dispatch(status([
                0,exception.message
            ]));
        });
    };