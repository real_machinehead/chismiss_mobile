import * as actionTypes from './actiontypes';
import { combineReducers } from 'redux';
import { constants } from '../../../constants';

const initstate='';
const initstatus =constants.status.INITIALIZE;

export const data = (state=initstate, action )=>{
    switch(action.type){
        case actionTypes.INITIALIZE:
            return action.payload;
            break;
        case actionTypes.UPDATE:
            //oState.resultLists = oHelper.arrayMerge(oState.resultLists, action.payload, 'notimeinid');
            return action.payload;
            break;
        case actionTypes.CONTACTLISTRESET:
            return [];
            break;
        default:
            return state;     
    }
};


const status =(state = initstatus, action)=>{
    switch (action.type){
        case actionTypes.STATUS:
            return action.payload;
            break;
        case actionTypes.CONTACTLISTRESET:
            return [3,''];
            break;
        default:
            return state;
    }
}

const contactstatus=(state=initstatus,action)=>{
    switch(action.type){
        case actionTypes.CONTACTSTATUS:
            return action.payload;
        break;
        case actionTypes.RESET:
            return [3,''];
        break;
        default:
            return state;
    }
}

const search =(state=initstate, action)=>{
    switch(action.type){
        case actionTypes.CONTACTSEARCH:
            return action.payload
        break;
        case actionTypes.CONTACTLISTRESET:
            return null;
    default:
        return state;
    }
}

const contactlist = (state=[], action)=>{
    let objState=[...state];
    switch(action.type){
        case actionTypes.CONTACTLIST:
            return action.payload
        break;
        case actionTypes.CONTACTLISTUPDATE:
            objState.push(action.payload);
            return objState;
        break;   
        case actionTypes.CONTACTLISTRESET:
            return [];
        break; 
    default:
        return state;
    }
}

const usercontactlist = (state=[], action)=>{
    let objState=[...state];
    
    switch(action.type){
        case actionTypes.USERCONTACTLIST:
       
            var arrcontactlist=[];
            
            for(x=0;x<action.payload.length;x++){
                const contactlist={
                    Callsign:action.payload[x].Callsign,
                    ChannelID:action.payload[x].ChannelID,
                    Email:action.payload[x].Email,
                    UserID:action.payload[x].UserID,
                    Username:action.payload[x].Username,
                    Status:'offline',
                    Newmessage:false,
                }
                
                arrcontactlist.push(contactlist);
            }
            
            //return contactlist;
            console.log('contact payload',arrcontactlist)
            return arrcontactlist;
            break; 
        case actionTypes.ONLINEUSERS:
            
            const {onlineusers,contactlist}=action.payload
            
            //console.log('current route',this.props.navigator.screenIsCurrentlyVisible());
            for(x=0;x<onlineusers.length;x++){
                var userid=onlineusers[x];
                //console.log('onlineusers ',contactlist);
                for(y=0;y<contactlist.length;y++){
                    
                    if(userid==contactlist[y].UserID){
                        console.log('onlines',contactlist[y].Status)
                        contactlist[y].Status='online'
                    }
                }

            }
            console.log('contacts',contactlist);
            return contactlist;
            break;
        case actionTypes.NEWMESSAGE:

            const{content,usrcontactlist}=action.payload;
                              
            for(x=0;x<usrcontactlist.length;x++){
                console.log('usrcontactlist',usrcontactlist[x].UserID,content)   
                if(content.userid==usrcontactlist[x].UserID){
                    
                    usrcontactlist[x].Newmessage=true;
                }
            }

            console.log('newmessage',usrcontactlist);
            return usrcontactlist;
            break;
        case actionTypes.READMESSAGE:
            const{contents,usrcontactlists}=action.payload;

            for(x=0;x<contents.length;x++){
                for(y=0;y<usrcontactlists.length;y++){
                    if(contents[x].UserID==usrcontactlists[y].UserID){
                        usrcontactlists[y].Newmessage=true;
                    }
                }
            }           

            break;
        case actionTypes.USERCONTACTLISTRESET:
            console.log('actiontypess',actionTypes);
            return [];
        break; 
    default:
        return state;
    }
}


export const reducers = combineReducers({
    data:data,
    status:status,
    search:search,
    contactlist:contactlist,
    usercontactlist:usercontactlist,
    contactstatus:contactstatus,
});