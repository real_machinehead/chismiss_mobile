export const INITIALIZE = 'profile/initialize';
export const UPDATE ='profile/update';
export const STATUS ='profile/status';
export const RESET ='profile/reset';
export const ONLINESTATUS='profile/onlinestatus';
export const UPDATESTATUS='profile/updatestatus';
export const CALLSIGN = 'profile/callsign';
export const FULLNAME = 'profile/fullname';
export const NAMEINITIALS ='profile/nameinitials';

