import {fetchApi} from '../../../globals/api';
import * as endPoints from '../../../globals/endpoints';


export let fetch = (payload,headers)=>fetchApi(endPoints.reports.userdetails.get(payload),payload,'get',headers);
export let updatestatus=(payload,headers)=>fetchApi(endPoints.security.updateUserStatus.put(payload),payload,'put',headers);