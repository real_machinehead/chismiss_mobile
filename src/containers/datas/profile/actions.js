import * as api from './api';
import * as actiontypes from './actiontypes';
import {constants} from '../../../constants';

export const reset = payload=>({
    type:actiontypes.RESET,
    payload:payload
});

export const update = payload=>({
    type:actiontypes.UPDATE,
    payload:payload
});

export const status = payload=>({
    type:actiontypes.STATUS,
    payload:payload
});

export const init = payload=>({
    type:actiontypes.INITIALIZE,
    payload:payload
});

export const onlinestatus=payload=>({
    type:actiontypes.ONLINESTATUS,
    payload:payload
});

export const updatestatus=payload=>({
    type:actiontypes.UPDATESTATUS,
    payload:payload
});

export const callsign=payload=>({
    type:actiontypes.CALLSIGN,
    payload:payload
});

export const fullnames=payload=>({
    type:actiontypes.FULLNAME,
    payload:payload
});

export const nameinitialss=payload=>({
    type:actiontypes.NAMEINITIALS,
    payload:payload
});

export const fetchprofile = payload =>
    dispatch=>{
        let objResponse={};
        console.log('profile payload',payload)
        dispatch(status(constants.status.LOADING));
        dispatch(reset())
        api.fetch(payload,payload.header)
        .then((response)=>response)
        .then((res)=>{
            console.log('fetchlogs1234',res.Data.Users[0].Status);
            let fullname=res.Data.Users[0].Fullname.split(" ")
            
            let firstname=fullname[0] ? fullname[0].charAt(0):' ';
            let lastname =fullname[1] ? fullname[1].charAt(0):' ';
            var nameinitial=firstname + lastname
            //console.log('fullname1234',firstname + lastname);

            dispatch(init(res.Data))
            dispatch(onlinestatus(res.Data.Users[0].Status))
            dispatch(callsign(res.Data.Users[0].Callsign))
            dispatch(fullnames(res.Data.Users[0].Fullname))
            dispatch(nameinitialss(nameinitial))
            //console.log('payloads123',res);
            //dispatch(status([1,'success']));
            //dispatch(init(res.data))
            objResponse={...res}
        })
        .then(()=>{
            /*dispatch(status([
                objResponse.flagno || 0,
                objResponse.message || constants.ERROR.SERVER
            ]));*/
        })
        .catch((exception)=>{
            dispatch(status([
                0,
                exception.message
            ]));    
        });
    };

export const updateonlinestatus = (payload,header) =>
    dispatch=>{
        let objResponse={};
        console.log('payloadstatus',payload.header);
        dispatch(status(constants.status.LOADING));

        api.updatestatus(payload,header)
        .then((response)=>response)
        .then((res)=>{
            console.log('response',res);
        })
        .then(()=>{

        })
        .catch((exception)=>{
            dispatch(status([
                0,
                exception.message
            ]));    
        });
    };