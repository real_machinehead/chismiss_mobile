import React, { Component, PureComponent} from 'react'
import {Text, View, FlatList,Platform } from 'react-native'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as profileActions from '../datas/profile/actions';
import {  List , ListItem,Avatar } from "react-native-elements";

import {ProfileContainers,GeneralContents,SearchBox,Spinner} from '../../components';
import {IconButton} from '../../components/common';


import styles from '../styles';
import profileStyles from './styles';

import Icon from 'react-native-vector-icons/Ionicons';
import Statuspicker from '../../components/picker/status';

export class Profile extends Component{

    constructor(props){
        super(props);

        this.state={
            loading:false,
            data:{
                flagno:1,
                message:'success',
                profile:[
                    {
                        type:'Sign-In As',
                        value:'lyndell.dobluis',
                        icon:'ios-person',
                        icontype:'ionicon'
                    },
                    {
                        type:'Chismis Name',
                        value:'Lyndell Dobluis',
                        icon:'ios-image',
                        icontype:'ionicon'
                    },
                    {
                        type:'Birth Day',
                        value:'01/01/1990',
                        icon:'ios-gift',
                        icontype:'ionicon'
                    }
                ],
                others:[
                    {
                        type:'Sign-In As',
                        value:'lyndell.dobluis',
                        icon:'user'
                    },
                    {
                        type:'Sign-In As',
                        value:'lyndell.dobluis',
                        icon:'user'
                    }
                ]
            },
            page:1,
            seed:1,
            error:null,
            refreshing: false,
            showForm:false,
        };

    }
    
    /*static navigationOptions ={
        title: 'CALLS',
        headerStyle:{
            backgroundColor: 'white'
        },
        headerTitleStyle:{
            alignSelf:'center',
            width: Platform.OS==='android'? '90%':'100%',
            textAlign:'center'
        }
    }*/


    componentDidMount(){
        console.log('profileLogs',this.props.profile.data.Users);
        if(this.props.profile.Data==null || this.props.profile.data==''){
            this._fetchData();
        }
        //this._fetchData();
    }

    _onProfilepress(){
        console.log("Works!")
    }
    _fetchData=()=>{

        const payload={
            username:'Jingfreeks',
            password:'Test',
            accesstoken:'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1NDM4MTgyMDYsImRldGFpbHMiOnsiVXNlcm5hbWUiOiJKaW5nZnJlZWtzIiwiSXNBbm9ueW1vdXMiOmZhbHNlLCJDYWxsc2lnbiI6IkppbmdmcmVla3MiLCJVc2VySUQiOjExOH19.3f5lwc3h5kC_2VXBpENDgf22_etcscH401Z_5OSIaKw',
            userid:118,
        }
        //this.props.profileActions.actions.fetchprofile(payload);

        //this.props.profileActions
        //this.props.actions.profile.fetchprofile(payload);
        //this.props.actions.profile.fetchprofile(payload);
        //this.props.actions.profile.fet
        
    }

    _keyExtractor = (item, index) => item.type;

    render(){
        //console.log('Profile Logs',this.props.profile);
        const profileData=this.props.profile.data.Users[0];
        //const profileData=this.state.data.profile;
        //const Data=this.props.profile.Data;

        //console.log('loggssss',this.props);
        console.log('profile name initials',this.props.nameinitialss)
        return(
            <ProfileContainers>
                <GeneralContents>
                    <View style={profileStyles.profileContainer}>
                        {this.props.nameinitialss[0]==2 ? 

                                <Spinner />
                            :
                                <Avatar
                                    xlarge
                                    rounded
                                    //title={`${profileData.Fullname ? profileData.Fullname.charAt(0):'P'}${profileData.Callsign.charAt(0)}`}
                                    title={this.props.nameinitialss}
                                    onPress={this._onProfilepress()}
                                    activeOpacity={0.7}
                                />                           
                        }
                    </View>
                    <View style={profileStyles.fullNameFormat}>
                        <Text style={profileStyles.profileNameTextFormat}>{profileData.Fullname}</Text>
                    </View>
                    <View style={profileStyles.profileNameAlignFormat}>
                        <View>
                            <Text style={{fontSize:20}}>Profile</Text>
                        </View>

                        <View style={{flexDirection:'row',marginLeft:20}}>
                            <Text>Call Sign:   </Text>
                            <Text>{profileData.Callsign}</Text>
                        </View>
                        <View style={{flexDirection:'row',marginLeft:20}}>
                            <Text>Email:   </Text>
                            <Text>{profileData.email}</Text>
                        </View>                 
                    </View>

                        {/*<View style={profileStyles.flatlist}>
                            <FlatList
                                data={profileData}
                                keyExtractor={this._keyExtractor}
                                renderItem={({ item }) => (
                                    <ListItem
                                        roundAvatar
                                        title={`${item.type}`}
                                        subtitle={item.date}
                                        leftIcon={
                                        
                                            <View style={{flexDirection:'row'}}>
                                                <View style={{paddingRight:10}}>
                                                    <IconButton>
                                                        <Icon name={item.icon} size={30}/>
                                                    </IconButton>

                                                </View>
                                            </View>                                          
                                        }
                                        onPress={()=>this._onPress()}
                                        rightTitle={`${item.value}`}                                          
                                    />
                                
                                )}
                            /> 
                        </View>*/}               
                </GeneralContents>
            </ProfileContainers>
        );
    }
}

function mapStateToProps(state){
    console.log('logstates12345677',state);
    return{
        login:state.security,
        profile:state.datareducer.profile,
        fullname:state.datareducer.fullname,
        nameinitialss:state.datareducer.profile.nameinitialss,
        //nameinitialss:state.datareducer.profile.nameinitialss,
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            //login:bindActionCreators(loginActions,dispatch),
            profile:bindActionCreators(profileActions,dispatch),
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile)

/*function mapStateToProps(state){
    console.log('statesss',state);
    return{
        profile:state.datareducer
    }
}

function mapDispatchToProps(dispatch){
    return{
        actions:{
            profile:bindActionCreators(profileActions,dispatch)
        }
    }
}

export default connect(
    mapDispatchToProps,
    mapStateToProps
)(Profile)*/