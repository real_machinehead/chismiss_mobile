
import { createBottomTabNavigator } from 'react-navigation';
import Call from './containers/calls';
import Chatlist from './containers/chatlist';
import Contacts from './containers/contacts';

const BottomTab =createBottomTabNavigator({
    Chatllist: Chatlist,
    Calls: Call,
    Contacts:Contacts,
});

export default BottomTab
    
  