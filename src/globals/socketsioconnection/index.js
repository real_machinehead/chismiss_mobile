
import io from 'socket.io-client';
import React, { Component, PureComponent} from 'react';

export default class SocketIoConnection extends Component{

        constructor(props){
            super(props);
        
        /*function SocketIoConnection(urls,transport) {
            console.log('connection12345556',urls,transport)
            this.connection = io.connect(urls,transport);
        }

        SocketIoConnection.prototype.on=function(ev,fn){
            this.connection.on(ev, fn);
        }

        SocketIoConnection.prototype.emit = function () {
            this.connection.emit.apply(this.connection, arguments);
        };
        
        SocketIoConnection.prototype.getSessionid = function () {
            return this.connection.id;
        };
        
        SocketIoConnection.prototype.disconnect = function () {
            return this.connection.disconnect();
        };*/
    }

    SocketIoConnection=(urls,transport)=>{
        console.log('connection12345556',urls,transport)
        this.connection = io.connect(urls,transport);
        this.connection.heartbeatTimeout=20000;
        return this.connection;
    }

    SocketIoOn=(ev,fn)=>{
        this.connection.on(ev,fn);
    }

    SocketIoEmit=()=>{
        this.connection.emit(this.connection);
    }

    GetSessionId=()=>{
        return this.connection.id;
    }

    SocketIoDisconnection=()=>{
        return this.connection.disconnect();
    }


}