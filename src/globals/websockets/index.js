import React, { Component, PureComponent} from 'react';
let instance=null;
let SERVER_URL='wss://chismischannel.live:9999/';

export default class websockets extends Component{
    constructor(props){
        super(props);
        if(!instance){
            instance=this;
        }
        this.ws= new WebSocket(SERVER_URL)

        return instance;
    }
}