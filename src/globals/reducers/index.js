import { combineReducers } from 'redux';
import {reducers as dataReducer } from '../../containers/datas/reducer';
import {reducers as setupReducer } from '../../containers/setups/datas/reducer';
import {reducers as securityReducer } from '../../containers/security/datas/reducer';

const rootReducer=combineReducers({
    datareducer:dataReducer,
    setups:setupReducer,
    security:securityReducer,
})

export default rootReducer;