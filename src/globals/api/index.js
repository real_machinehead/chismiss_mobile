import config from './config';

export const fetchApi=(endPoints,payload={},methodParams='POST',header={})=>{

    let bodyParams='';
    //console.log('config',config.url + endPoints);
    console.log('headeeerrr',header);
    if(methodParams.toUpperCase()==='POST' || methodParams.toUpperCase()==='PUT' ){
        console.log('config',config.url + endPoints);
        return fetch(config.url + endPoints,{
            method:methodParams,
            headers:header,
            body:payload,
        })
        .then((response)=>response.json())
        .then((responseJson)=>{
            console.log('test',responseJson);
            return responseJson;
        })
        .catch((error)=>{
            if(error.response && error.response.json()){
                error.response.json().then((json)=>{
                    if(json) throw json;
                    throw error;
                });
            }else{
                throw error;
            }
        });
    }else{
          return fetch(config.url+endPoints,{
            method:methodParams,
            headers:header 
        })
        .then((response)=>response.json())
        .then((responseJson)=>{
            console.log('response',responseJson);
            return responseJson;
        })
        .catch((error)=>{
            console.error(error);

            if(error.response && error.response.json){
                error.response.json().then((json)=>{
                    if(json) throw json;
                    throw error;
                });
            }else{
                throw error;
            }
        });      
    }
}