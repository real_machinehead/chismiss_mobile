import React from 'react';
import {View,Text,TextInput} from 'react-native';
import styles from './styles';

const GeneralContents=(props)=>{
    return(
        <View style={styles.contents}>
            {props.children}
        </View>
    );
}

export {GeneralContents}