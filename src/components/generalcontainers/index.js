import React from 'react';
import {View,Text,TextInput,SafeAreaView} from 'react-native';
import styles from './styles';

const GeneralContainers=(props,{loading})=>{
    return(
        <SafeAreaView style={{flex:1, backgroundColor:'#fff'}}>
            <View style={styles.container}>
                {props.children}
            </View>
        </SafeAreaView>
    );
}

export {GeneralContainers}