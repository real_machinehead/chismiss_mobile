export default{
    maincontainerStyle:{
        flex:1,
        borderBottomwidth:1,
        padding:5,
        backgroundColor:'#fff',
        justifyContent:'flex-start',
        flexDirection:'row',
    },
    roundAvatarContainerStyle:{
        width:'40%',
        marginTop:10,       
    },
    messageContainerStyle:{
        width:'60%',
        marginTop:10,
    },
    roundAvatarStyle:{
        width: 50,
        height: 50,
        borderRadius: 100/2,
        backgroundColor: '#7ec0ee',
        alignItems:'center',   
        justifyContent:'center'           
    },
    messageStyle:{
        width:'100%',
        height:'100%'
    }
};