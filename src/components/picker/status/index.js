import React,{ Component } from 'react';
import { View, Text, Picker,TouchableHighlight } from 'react-native';
import styles from './styles';

export default class Statuspicker extends Component {
    constructor(props){
        super(props);
        this.state={
            onlinestatus:'',
        };
    } 

    _onUpdatestatus=(status)=>{
        this.setState({onlinestatus:status})
    }
    _onStatusOnlineClick=()=>{
        let status={id:1,status:'online'};
        this.props.onChangeOnlineStatus(status);
    }
    _onStatusBusyClick=()=>{
        let status={id:2,status:'busy'};
        this.props.onChangeOnlineStatus(status);
    }
    _onStatusInvisibleClick=()=>{
        let status={id:0,status:'invisible'};

        this.props.onChangeOnlineStatus(status);
    }
    render(){
        console.log('status heres',this.props);
        return(
            <View style={styles.statusWrapper}>  
                <View style={styles.statusContainer}>
                    <View style={styles.statusonlineButtonStyle}>
                        <TouchableHighlight style={styles.statusonlineButtonStyle} onPress={()=>this._onStatusOnlineClick()} >
                            <Text>Online</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.statusbusyButtonStyle}>
                        <TouchableHighlight style={styles.statusbusyButtonStyle} onPress={()=>this._onStatusBusyClick()} >
                            <Text >Busy</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.statusinvisibleButtonStyle}>
                        <TouchableHighlight style={styles.statusinvisibleButtonStyle} onPress={()=>this._onStatusInvisibleClick()} >
                            <Text >Invisible</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View>
        );
    }
}
