import React from 'react';
import {View,TextInput,Text,TouchableOpacity} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
    

const Messagebox =({message,type,onClose,onPress,OnDelete})=>{

    switch(type){
        case 'success':
            return(
                <View style={styles.generalcontainerStyle}>
                    <View style={styles.mainContainerStyle}>
                        <View style={styles.header}>
                            <View  style={styles.headerIconStyle}>
                                <Text>Icon</Text>
                            </View>
                            <View styles={styles.headerButtonClosed}>
                                    <TouchableOpacity onPress={ onClose }>
                                        <Icon name='md-close-circle' size={30}/>
                                    </TouchableOpacity>
                            </View>
        
                        </View>
                            <View style={styles.content}>
                                <Text style={styles.contentTextStyle}>{message}</Text>
                            </View>
                            <View style={styles.footer}>
                                <View>
                                    <TouchableOpacity onPress={ onClose } style={styles.buttonStyle} >
                                        <Text style={styles.buttonTextStyle}> 
                                            Ok
                                        </Text>
                                    </TouchableOpacity>
                                </View>
        
                            </View>
                    </View>
        
                </View>
            );  
        break;
        case 'deletemsg':
            return(
                <View style={styles.deletmsggeneralcontainerStyle}>
                    <View style={styles.deletemsgmainContainerStyle}>
                        <View style={styles.header}>
                            <View  style={styles.headerIconStyle}>
                                <Text>Icon</Text>
                            </View>
                            <View styles={styles.headerButtonClosed}>
                                    <TouchableOpacity onPress={ onClose }>
                                        <Icon name='md-close-circle' size={30}/>
                                    </TouchableOpacity>
                            </View>
        
                        </View>
                            <View style={styles.deletecontent}>
                                <Text style={styles.contentTextStyle}>{message}</Text>
                            </View>
                            
                            <View style={styles.deletefooter}>
                                <View>
                                    <TouchableOpacity onPress={ onClose } style={styles.buttonStyle} >
                                        <Text style={styles.buttonTextStyle}> 
                                            No
                                        </Text>
                                    </TouchableOpacity>
                                </View>       
                                <View>
                                    <TouchableOpacity onPress={ OnDelete } style={styles.buttonStyle} >
                                        <Text style={styles.buttonTextStyle}> 
                                            Yes
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                    </View>
        
                </View>
            );  
        break;
        default:
            return(
                <View style={styles.generalcontainerStyle}>
                    <View style={styles.mainContainerStyle}>
                        <View style={styles.header}>
                            <View  style={styles.headerIconStyle}>
                                <Text>Icon</Text>
                            </View>
                            <View styles={styles.headerButtonClosed}>
                                    <TouchableOpacity onPress={ onClose }>
                                        <Icon name='md-close-circle' size={30}/>
                                    </TouchableOpacity>
                            </View>

                        </View>
                            <View style={styles.content}>
                                <Text style={styles.contentTextStyle}>{message}</Text>
                            </View>
                            <View style={styles.footer}>
                                <View>
                                    <TouchableOpacity onPress={ onClose } style={styles.buttonStyle} >
                                        <Text style={styles.buttonTextStyle}> 
                                            Ok
                                        </Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                    </View>

                </View>
            );
    }
           
}

export {Messagebox};