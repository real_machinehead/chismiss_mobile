import React from 'react';
import {View,Text,TextInput} from 'react-native';
import styles from './styles';

const GeneralHeaders=(props)=>{
    return(
        <View style={styles.header}>
            {props.children}
        </View>
    );
}

export {GeneralHeaders}