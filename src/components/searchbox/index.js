import React from 'react';
import {View,TextInput} from 'react-native';
import styles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';

const SearchBox =({value,onChangeText,placeholder,secureTextEntry})=>{
    return(
        <View style={styles.containerStyle}>
            <Icon style={styles.searchIcon} name="ios-search" size={20} color="#000"/>       
            <TextInput style={styles.inputStyle} secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                autoCorrect={false}
                value={value}
                onChangeText={onChangeText}
                underlineColorAndroid='transparent'
            />
        </View>
    );
}

export {SearchBox};
