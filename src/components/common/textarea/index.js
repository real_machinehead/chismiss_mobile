import React from 'react';
import {View,Text,TextInput,SafeAreaView} from 'react-native';
import styles from './styles';

const TextArea =({label,value,onChangeText,placeholder,secureTextEntry})=>{
    return(
        <SafeAreaView>
            <View style={styles.containerStyle}>
                <TextInput style={styles.inputStyle} secureTextEntry={secureTextEntry}
                    placeholder={placeholder}
                    multiline = {true}
                    autoCorrect={false}
                    value={value}
                    onChangeText={onChangeText}
                />
            </View>
        </SafeAreaView>          
    );
}

export {TextArea};
