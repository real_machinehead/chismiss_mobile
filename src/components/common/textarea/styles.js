export default{
    inputStyle:{
        color:'#000',
        paddingRight:5,
        paddingLeft:5,
        fontSize:18,
        lineHeight:23,
        height:80,
        flex:1,
    },
    labelStyle:{
        fontSize:18,
        paddingBottom:2,
        flex:1.5,
    },
    containerStyle:{
        height:80,
        flex:1,
        marginLeft:5,
        marginRight:5,
        flexDirection:'row',
        alignItems:'center',
        color:'white'
    },
}