import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import styles from './styles';

const IconButton = ({onPress,children})=>{
    return(
        <TouchableOpacity onPress={ onPress } >
            <Text> 
                {children}
            </Text>
        </TouchableOpacity>
    );
}

export {IconButton};