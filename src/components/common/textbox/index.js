import React from 'react';
import {View,Text,TextInput} from 'react-native';
import styles from './styles';

const Textbox =(
    {
        label,
        value,
        onChangeText,
        placeholder,
        secureTextEntry,
        reference,
        returnKeyType,
        onSubmitEditing
    })=>{
    return(
        <View style={styles.containerStyle}>
            <Text style={styles.labelStyle}>
                {label}
            </Text>
            <TextInput style={styles.inputStyle} secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                autoCorrect={false}
                value={value}
                onChangeText={onChangeText}
                ref={reference}
                returnKeyType = { returnKeyType }      
                onSubmitEditing={onSubmitEditing}         
            />
        </View>
    );
}

export {Textbox};
