import React,{ Component } from 'react';
import Navigator from './Navigator';

import {Provider} from 'react-redux';
import configureStore from './globals/store';

const store = configureStore();

export default class App extends Component{
  render(){
    return(
      <Provider store={store}>
        <Navigator />
      </Provider>
    );
  }
}

