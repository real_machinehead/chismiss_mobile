var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server,{pingTimeout: 30000});

server.listen(8888,'0.0.0.0');
// WARNING: app.listen(80) will NOT work here!

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

var roomList = {};
var users ={};

function socketIdsInRoom(name) {
  var socketIds = io.nsps['/'].adapter.rooms[name];
  if (socketIds) {
    var collection = [];
    for (var key in socketIds) {
      collection.push(key);
    }
    return collection;
  } else {
    return [];
  }
}

io.on('connection', function(socket){

  socket.on('usrregister',function(username){
    socket.username=username;
    users[socket.username]=socket;
    console.log('usersss',socket.username);
  });  

  socket.on('private_chat',function(data){
    var to=data.to,message=data.message,userId=data.userId.UserID;
    //console.log('dta',data.userId)
    if(users.hasOwnProperty(to)){
      console.log('priovaasdfasdf',data)
        users[to].emit('private_chat',{
          username:socket.username,
          message:message,
          userId:userId,
        })
    }
  });
  socket.on('disconnect', function(data){
    console.log('disconnect');
    delete users[socket.username];
  });

  socket.on('create',function(room){
    socket.join(room);
  });

  socket.on('joinroom',function(room,title,data){
    io.to(room).emit(title,data);
  })  

  socket.on('localdescription',function(data){

    socket.broadcast.emit('remotelocaldescription',data);

  })

  socket.on('messagesdpcandidate',function(data){
    console.log('messagesdpcandidate',data);
    socket.broadcast.emit('remotesdpcandidate',data);
  });

  socket.on('createanswer',function(data){
    console.log('createanswer',data);

    socket.broadcast.emit('remoteanswer',data);
  })
});

/*'use strict'

const express = require('express');
const fs = require('fs');
const https =require('https');
const path = require('path');

const app = express();
const directoryToServe='client'
const port =8888;

app.use('/',express.static(path.join(__dirname, '.', directoryToServe)));

const httpsOptions = {
  cert: fs.readFileSync(__dirname + '/ssl/cert.pem'),
  key: fs.readFileSync(__dirname +'/ssl/keys.key')
  //key: fs.readFileSync('./key.pem'),
  //cert: fs.readFileSync('./cert.pem')
}

const server=https.createServer(httpsOptions,app)
  .listen(port, function (){
    console.log(`Serving the $(directoryToServe)/ directory at https:localhost:${port}`)
  })
  var io = require('socket.io')(server,{log: false });
  io.on('connection', function (socket) {
    socket.emit('news', { hello: 'world' });
    socket.on('my other event', function (data) {
      console.log(data);
    });
    socket.emit('update',{response:'test'})
  });

    io.set('transports', [
      'websocket'
    , 'flashsocket'
    , 'htmlfile'
    , 'xhr-polling'
    , 'jsonp-polling'
    ]);

  

  //io.set('transports', ['websocket']);
  //io.set('transports', ['websocket','xhr-polling']);*/
  

  
