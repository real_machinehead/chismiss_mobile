var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(8888,'0.0.0.0');
// WARNING: app.listen(80) will NOT work here!

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

/*io.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', function (data) {
    console.log('connection',data);
  });
  socket.emit('update',{response:'test'})
});*/
var roomList = {};

function socketIdsInRoom(name) {
  var socketIds = io.nsps['/'].adapter.rooms[name];
  if (socketIds) {
    var collection = [];
    for (var key in socketIds) {
      collection.push(key);
    }
    return collection;
  } else {
    return [];
  }
}

io.on('connection', function(socket){

  console.log('connection');
  
  socket.on('disconnect', function(){
    console.log('disconnect');
    if (socket.room) {
      var room = socket.room;
      io.to(room).emit('leave', socket.id);
      socket.leave(room);
    }
  });

  socket.on('create',function(room){
    socket.join(room);
  });

  socket.on('join', function(name, callback){
    console.log('join', name);
    var socketIds = socketIdsInRoom(name);
    callback(socketIds);
    socket.join(name);
    socket.room = name;
  });


  socket.on('exchange', function(data){
    //console.log('exchange', socket.id);

    //io.to(socket.id).emit('exchange',data);
    data.from = socket.id;
    var to = io.sockets.connected[data.to];
    console.log('exchangesss', io.sockets.connected[data.to]);
    io.to(socket.id).emit('exchange',data);

    //io.to(socketid).emit('user-already-joined',data)
    //to.emit('exchange', data);
    //io.emit('exchange', data)
    /*console.log('exchange data',data);
    io.emit('exchange',data)*/
  });
  
  socket.on('localdescription',function(data){
    //console.log('createoffer',data)

    io.emit('remotedescription',data);
  });

  socket.on('callconnection',function(data){

    io.emit('remotecallconnection',data)
  })


  socket.on('onicecandidate',function(data){
    console.log('ice Candidate',data);

    io.emit('remoteonicecandidate',data);
  });



  socket.on('remoteurl',function(data){
    const streamurl=[];
    streamurl.from=data
    console.log(data);
    socket.emit('fetchremoteurl',data);
  });  

  socket.on('call',function(data){
    socket.emit('callinformation')
  });

  socket.on('streamurl',function(data){
    console.log('stream url',data);

    io.emit('remotestreamurl',data);

  });

  socket.on('peercredentials',function(data){

    console.log('peercredentials',data);

    io.emit('remotecredentials',data);
  })

});

/*'use strict'

const express = require('express');
const fs = require('fs');
const https =require('https');
const path = require('path');

const app = express();
const directoryToServe='client'
const port =8888;

app.use('/',express.static(path.join(__dirname, '.', directoryToServe)));

const httpsOptions = {
  cert: fs.readFileSync(__dirname + '/ssl/cert.pem'),
  key: fs.readFileSync(__dirname +'/ssl/keys.key')
  //key: fs.readFileSync('./key.pem'),
  //cert: fs.readFileSync('./cert.pem')
}

const server=https.createServer(httpsOptions,app)
  .listen(port, function (){
    console.log(`Serving the $(directoryToServe)/ directory at https:localhost:${port}`)
  })
  var io = require('socket.io')(server,{log: false });
  io.on('connection', function (socket) {
    socket.emit('news', { hello: 'world' });
    socket.on('my other event', function (data) {
      console.log(data);
    });
    socket.emit('update',{response:'test'})
  });

    io.set('transports', [
      'websocket'
    , 'flashsocket'
    , 'htmlfile'
    , 'xhr-polling'
    , 'jsonp-polling'
    ]);

  

  //io.set('transports', ['websocket']);
  //io.set('transports', ['websocket','xhr-polling']);*/
  

  
