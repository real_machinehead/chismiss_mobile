var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server,{pingTimeout: 30000});

server.listen(8888,'0.0.0.0');
// WARNING: app.listen(80) will NOT work here!

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

/*io.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', function (data) {
    console.log('connection',data);
  });
  socket.emit('update',{response:'test'})
});*/
var roomList = {};

function socketIdsInRoom(name) {
  var socketIds = io.nsps['/'].adapter.rooms[name];
  if (socketIds) {
    var collection = [];
    for (var key in socketIds) {
      collection.push(key);
    }
    return collection;
  } else {
    return [];
  }
}

io.on('connection', function(socket){

  console.log('connection');
  
  socket.on('disconnect', function(){
    console.log('disconnect');
    if (socket.room) {
      var room = socket.room;
      io.to(room).emit('leave', socket.id);
      socket.leave(room);
    }
  });

  socket.on('create',function(room){
    socket.join(room);
  });

  socket.on('joinroom',function(room,title,data){
    io.to(room).emit(title,data);
  })  

  socket.on('localdescription',function(data){

    socket.broadcast.emit('remotelocaldescription',data);

  })

  socket.on('messagesdpcandidate',function(data){
    console.log('messagesdpcandidate',data);
    socket.broadcast.emit('remotesdpcandidate',data);
  });

  socket.on('createanswer',function(data){
    console.log('createanswer',data);

    socket.broadcast.emit('remoteanswer',data);
  })
});

/*'use strict'

const express = require('express');
const fs = require('fs');
const https =require('https');
const path = require('path');

const app = express();
const directoryToServe='client'
const port =8888;

app.use('/',express.static(path.join(__dirname, '.', directoryToServe)));

const httpsOptions = {
  cert: fs.readFileSync(__dirname + '/ssl/cert.pem'),
  key: fs.readFileSync(__dirname +'/ssl/keys.key')
  //key: fs.readFileSync('./key.pem'),
  //cert: fs.readFileSync('./cert.pem')
}

const server=https.createServer(httpsOptions,app)
  .listen(port, function (){
    console.log(`Serving the $(directoryToServe)/ directory at https:localhost:${port}`)
  })
  var io = require('socket.io')(server,{log: false });
  io.on('connection', function (socket) {
    socket.emit('news', { hello: 'world' });
    socket.on('my other event', function (data) {
      console.log(data);
    });
    socket.emit('update',{response:'test'})
  });

    io.set('transports', [
      'websocket'
    , 'flashsocket'
    , 'htmlfile'
    , 'xhr-polling'
    , 'jsonp-polling'
    ]);

  

  //io.set('transports', ['websocket']);
  //io.set('transports', ['websocket','xhr-polling']);*/
  

  
